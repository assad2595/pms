
/*function to add datatable in tables.*/
$(document).ready(function() {
  $('.datatablejs').DataTable();
  $('#formvalidation').validationEngine();
  $("#validation").validationEngine();
});

/*Remaining Balance calculation for Property Transactions*/
$("#property_amount").keyup(function(){
  //alert('hello');
  let property_amount = $('#property_amount').val();
  let totalpropertyamount = $('#totalpropertyamount').val();
  let balance_amount= $('#balance_amount').val(); 
  
  let remaining_balance_from_total = +totalpropertyamount - +property_amount;
  let remaining_balance = +balance_amount - +property_amount;
  //alert(balance_amount);
  //alert(remaining_balance_from_total)
  if(+balance_amount == ""){
    //alert(balance_amount);
    if(+property_amount > +totalpropertyamount){
      alert("Amount cannot be greater than the actual amount.");
      $('#property_amount').val('');
      $('#remaining_balance').val('');
    }
    else{
      $('#remaining_balance').val(remaining_balance_from_total);
    }
  }
  else{
    if(+property_amount > +balance_amount){
      alert("Receiving amount cannot be greater than the balance amount.");
      $('#property_amount').val('');
      $('#remaining_balance').val('');
    }
    else{
      $('#remaining_balance').val(remaining_balance);
    }
  }

  if(+property_amount == ""){
    $('#remaining_balance').val('');
  }
});

/*Calculates Employee Total Salary at Registration*/
$("#salary").keyup(function(){
 let lunch_allowance  = $('#lunch_allowance').val();
 let allowance = $('#allowance').val();
 let salary = $('#salary').val();

   let total = +lunch_allowance + +allowance + +salary;
   $('#total_salary').val(total);
});

/*Save Loan Amount as Total Remaining Balance for first Time*/
$("#advance_payment").keyup(function(){
 let loan_amount = $('#advance_payment').val();
 $('#rem_balance').val(loan_amount);
});

/*Checking if employee leaving_date and leaving_reason have values on input*/
$("#leaving_reason").keyup(function(){
  let leaving_reason = $('#leaving_reason').val();
  let leaving_date   = $('#leaving_date').val();
  if(leaving_reason == ""){
    $('#check').val('Active');
  }
  else{
    $('#check').val('Closed');
  }
});
/* for printing datatable */
$(document).ready(function() {
    $('#print').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'print'
        ]
    } );
} );

/*on select, input debit & credit hide and show*/
$('#type_selection').change(function () {
    
    if ($(this).val() == 'Debit') {
        $('#debit').show();
    } else {
        $('#debit').hide();
    }
    if ($(this).val() == 'Credit') {
        $('#credit').show();
    } else {
        $('#credit').hide();
    }
});

/*on select, input cheque hide and show*/
$('#method_selection').change(function () {
    if ($(this).val() == 'Cheque') {
        $('#cheque').show();
    } else {
        $('#cheque').hide();
    }
});

/*on select get registration status value and calcultates total with holding tax on debit*/
$('#bank_selection').change(function () {
  let bank_val=$('#bank_selection').val();
  let arr = bank_val.split('-');  
  $('#bank_id').val(arr[0]);
  $('#taxpayer_status').val(arr[1]);
    //alert(arr[1]);
      $( "#amount").keyup(function(){
        let total_amount = $('#amount').val();
        let total_w_h_tax = $('#w_h_tax').val();
        //alert(total_amount);
      if ($('#taxpayer_status').val() == 'Filer' && total_amount >50000) { 
          total_w_h_tax = total_amount * 0.3 / 100;
          //alert(total_w_h_tax); 
          $('#w_h_tax').val(total_w_h_tax);
      } 
      else if ($('#taxpayer_status').val() == 'NonFiler' && total_amount >50000) {
          total_w_h_tax = total_amount * 0.6 / 100;
          //alert(total_w_h_tax);
          $('#w_h_tax').val(total_w_h_tax);
      }
      else{
           total_w_h_tax = 0;
           $('#w_h_tax').val(total_w_h_tax);
      }
    });
  });

/*job status from hide and show*/
$(document).ready(function(){
  $("#hide").click(function(){
    $("#jobStatusForm").hide();
  });
  $("#show").click(function(){
    $("#jobStatusForm").show();
  });
});

/*ViewInstallment Data Hide & Show*/
$(document).ready(function(){
  $("#hide").click(function(){
    $("#bank_balance").hide();
  });
  $("#show").click(function(){
    $("#bank_balance").show();
  });
});

/*Employee Salary Slips Table*/
$(document).ready(function(){
  $("#show").click(function(){
    $("#salaryTable").show();
  });
});

/*Calculate total marla from acr*/
$("#acr").keyup(function(){
  let acr = $('#acr').val();
  let t_marla = acr * 160;
  $('#total_marlas').val(t_marla);
  $('#kanal').val('0');
  $('#marla').val('0');
  $('#yard').val('0');
  $('#rate_per_acr').val('');
  $('#rate_per_marla').val('');
  let p_arc_entryVal = t_marla / 160;
  let p_arc_entry = Number.parseFloat(p_arc_entryVal).toFixed(2);
  $('#point_entry').val(p_arc_entry);
});

/*Calculate total marla from kanal*/
$("#kanal").keyup(function(){
  let acr = $('#acr').val();
  let kanal = $('#kanal').val();
  let kanal_t_marla = kanal * 20;
  let acr_t_marla = acr * 160;
  let t_marla = kanal_t_marla + acr_t_marla;
  $('#rate_per_acr').val('');
  $('#rate_per_marla').val('');
  
  if(acr == "" ){
    $('#total_marlas').val(kanal_t_marla);
    let p_k_entryVal = kanal_t_marla / 160;
    let p_k_entry = Number.parseFloat(p_k_entryVal).toFixed(2);
    $('#point_entry').val(p_k_entry);
  }
  else{
    $('#total_marlas').val(t_marla);
    let p_a_k_entryVal = t_marla / 160;
    let p_a_k_entry = Number.parseFloat(p_a_k_entryVal).toFixed(2);
    $('#point_entry').val(p_a_k_entry);
  }
});

/*Calculate total marla from marlas*/
$("#marla").keyup(function(){
  let acr = $('#acr').val();
  let kanal = $('#kanal').val();
  let marla = $('#marla').val();
  let kanal_t_marla = kanal * 20;
  let acr_t_marla = acr * 160;

  $('#rate_per_acr').val('');
  $('#rate_per_marla').val('');

  let t_kanal_marla = +kanal_t_marla + +marla;
  let t_marla = +kanal_t_marla + +acr_t_marla + +marla;
  
  $('#total_marlas').val('0');

  if(acr == "" && kanal == ""){
    $('#acr').val('0');
    $('#kanal').val('0');
    $('#total_marlas').val(marla);
    let p_m_entry = marla / 160;
    let pointEntryVal = Number.parseFloat(p_m_entry).toFixed(2);
    $('#point_entry').val(pointEntryVal);
    //alert(pointEntryVal);
  }
  else if(acr == "" && kanal != ""){
    $('#total_marlas').val(t_kanal_marla);

    let p_k_m_entry = t_kanal_marla / 160;
    let pointEntryVal = Number.parseFloat(p_k_m_entry).toFixed(2);
    $('#point_entry').val(pointEntryVal);
  }
  else{
    $('#total_marlas').val(t_marla);
    let p_a_k_m_entry = t_marla / 160;
    let pointEntryVal = Number.parseFloat(p_a_k_m_entry).toFixed(2);
    $('#point_entry').val(pointEntryVal);
  }
});

/*Calculate total marlas from yard*/
$("#yard").keyup(function(){
  let acr = $('#acr').val();
  let kanal = $('#kanal').val();
  let marla = $('#marla').val();
  let yard = $('#yard').val();
  //alert(yard);
  let acr_t_marla = acr * 160;
  let kanal_t_marla = kanal * 20;
  let yardToMarla = yard / 272;

  $('#rate_per_acr').val('');
  $('#rate_per_marla').val('');
  
  let yard_t_marla = Number.parseFloat(yardToMarla).toFixed(2);
  
  let t_arc_kanal_marla_yard = +acr_t_marla + +kanal_t_marla + +marla + +yard_t_marla;
  let t_kanal_marla_yard = +kanal_t_marla + +marla + +yard_t_marla;
  let t_marla_yard = +marla + +yard_t_marla;

  if(acr == "" && kanal == "" && marla == ""){
    $('#acr').val('0');
    $('#kanal').val('0');
    $('#marla').val('0');

    $('#total_marlas').val(yard_t_marla);
    let yard_entry = yard_t_marla / 160;
    let pointEntryValYard = Number.parseFloat(yard_entry).toFixed(2);
    $('#point_entry').val(pointEntryValYard);
  }
  else if(acr == "" && kanal != "" && marla != ""){
    $('#total_marlas').val(t_kanal_marla_yard);

    let kmy_entry = t_kanal_marla_yard / 160;
    let pointEntryValkmy = Number.parseFloat(kmy_entry).toFixed(2);
    $('#point_entry').val(pointEntryValkmy);
  }
  else if(acr == "" && kanal != "" && marla == ""){
    $('#total_marlas').val(t_marla_yard);

    let my_entry = t_marla_yard / 160;
    let pointEntryValmy = Number.parseFloat(my_entry).toFixed(2);
    $('#point_entry').val(pointEntryValmy);
  }
  else{
    $('#total_marlas').val(t_arc_kanal_marla_yard);

    let akmy_entry = t_arc_kanal_marla_yard / 160;
    let pointEntryValakmy = Number.parseFloat(akmy_entry).toFixed(2);
    $('#point_entry').val(pointEntryValakmy);
  }
});

/*Rate per marla from acr rate*/
$("#rate_per_acr").keyup(function(){
  let rate_per_acr = $('#rate_per_acr').val();
  let total_marlas = $('#total_marlas').val();

  let rate_per_marla = rate_per_acr / 160;
  //let rate_per_marla = Number.parseFloat(ratepermarla).toFixed(2);
  $('#rate_per_marla').val(rate_per_marla);
  
  let tamount = total_marlas * rate_per_marla;
  let t_amount = Number.parseFloat(tamount).toFixed(2);
  //let tcommission = t_amount / 100;
  //let commission = Number.parseFloat(tcommission).toFixed(2);


  if(total_marlas != ""){
    $("#total_amount").val(t_amount);
    //$('#proprety_commission').val(commission);
    
  }
});

/*Rate per acr from marla rate */
$("#rate_per_marla").keyup(function(){
  let rate_per_marla = $('#rate_per_marla').val();
  let total_marlas = $('#total_marlas').val();
  
  let rate_per_acr = rate_per_marla * 160;
  //let rate_per_acr = Number.parseFloat(rateperacr).toFixed(2);
  $('#rate_per_acr').val(rate_per_acr);

  let tamount = total_marlas * rate_per_marla;
  let t_amount = Number.parseFloat(tamount).toFixed(2);
  //let t_commission = t_amount / 100;
  //let commission = Number.parseFloat(t_commission).toFixed(2);

  if(total_marlas != ""){
    $("#total_amount").val(t_amount);
    //$('#proprety_commission').val(commission);
  }
});

/*No of files with the help of Excemption Rate*/
$("#exemption_rate").keyup(function(){
  let exemption_rate = $('#exemption_rate').val();
  let point_entry = $('#point_entry').val();

  let files= point_entry * exemption_rate;
  if(exemption_rate != ""){
    let filesVal = Number.parseFloat(files).toFixed(2);
    $('#no_of_files').val(filesVal);
  }
});

/*property commission div enable/disable */
$("input[name=commission_option]").change(function(){
  if($(this).val() == "yes"){
    
    $("#commission_div").show();
    $("#proprety_commission").prop('disabled', false);

    let total_amount = $('#total_amount').val();
    let t_commission = total_amount / 100;
    let commission = Number.parseFloat(t_commission).toFixed(2);
    $('#proprety_commission').val(commission);
    
  }
  else{
    $("#commission_div").hide();
    $("#proprety_commission").prop('disabled', true);
  }
});

//make calculation on file purchasse price and save remaining balance
$("#p_amount_received").keyup(function(){
  let amount_rcvd = $('#p_amount_received').val();
  let total_p_price = $('#total_p_price').val();
  let rem_bal = $('#r_bal').val();
  let cal_frm_t = total_p_price - amount_rcvd;
  let cal_frm_total = Number.parseFloat(cal_frm_t).toFixed(2);
  let cal_frm_rm = rem_bal - amount_rcvd;
  let cal_frm_rem = Number.parseFloat(cal_frm_rm).toFixed(2);

  if(+rem_bal == ""){
    if(+amount_rcvd > +total_p_price){
      alert("Receiving amount cannot be great than the purchasing price. ");
      $('#amt_bal').val('');
      $('#p_amount_received').val('');
    }
    else{
      $('#amt_bal').val(cal_frm_total);
    }
  }
  else{
    if(+amount_rcvd > +rem_bal){
      alert("Receiving amount cannot be great than the remaining purchasing price. ");
      $('#amt_bal').val('');
      $('#p_amount_received').val('');
    }
    else{
      $('#amt_bal').val(cal_frm_rem);
    }
  }

  if(+cal_frm_rem == 0){
    alert("remaining bal is 0, payment completed")
    $('#p_payment_status').val('Completed');
  }

  if(+amount_rcvd == ""){
    $('#amt_bal').val('');
  }
});

//make calculation on file sale price and save remaining balance
$("#s_amount_paid").keyup(function(){
  let amount_paid = $('#s_amount_paid').val();
  let total_s_price = $('#total_s_price').val();
  let rem_bal = $('#r_bal').val();

  let cal_frm_t = total_s_price - amount_paid;
  let cal_frm_total = Number.parseFloat(cal_frm_t).toFixed(2);
  let cal_frm_rm = rem_bal - amount_paid;
  let cal_frm_rem = Number.parseFloat(cal_frm_rm).toFixed(2);

  if(+rem_bal == ""){
    if(+amount_paid > +total_s_price){
      alert("Paid amount cannot be great than the selling price. ");
      $('#amt_bal').val('');
      $('#s_amount_paid').val('');
    }
    else{
      $('#amt_bal').val(cal_frm_total);
    }
  }
  else{
    if(+amount_paid <= +rem_bal){
      $('#amt_bal').val(cal_frm_rem);
    }
    else{
      $('#amt_bal').val('');
      $('#s_amount_paid').val('');
      alert("Paid amount cannot be great than the remaining selling price. ");
    }
  }

  if(+amount_paid == ""){
    $('#amt_bal').val('');
  }
});

//make calculation in officeexpenditure.create file(item amount - remaining balance)
$( "#item_amount").keyup(function(){
  let item_amount = $('#item_amount').val();
  let t_balance = $('#t_balance').val();
  let remaining_bal = +t_balance-item_amount;
  let remaining_balance = Number.parseFloat(remaining_bal).toFixed(2);
  $('#remaining_balance').val(remaining_balance);

  if(+item_amount == ""){
  $('#remaining_balance').val(remaining_balance);
  }
});

//make calculation when update amount in officeexpenditure.edit file
$("#new_amount").keyup(function(){
  let exist_amt = $('#existing_amount').val();
  let new_amt = $('#new_amount').val();
  let t_balance = $('#t_balance').val();
  let item_amt=exist_amt - new_amt;
  let remaining_bal = +t_balance + item_amt;
  let remaining_balance = Number.parseFloat(remaining_bal).toFixed(2);
  $('#remaining_balance').val(remaining_balance);

  if(+new_amount == ""){
  $('#remaining_balance').val(remaining_balance);
  }

});
// Property Sold files not to be greater then the actual files 
$('#sold_file').keyup(function(){
  let sold_file = $('#sold_file').val();
  let total_files = $('#no_of_files').val();
  let remaining_files = $('#remaining_files').val();
  

  if(remaining_files > '0.00'){
      //alert(remaining_files);
      if(sold_file > remaining_files){
        alert('No of Files value cannot be greater then the actual files');
        $('#sold_file').val('0');
      }
  }
  else{
    if(sold_file > total_files){
      alert('No of Files value cannot be greater then the actual files!');
      $('#sold_file').val('0');
    }
  }

  
});
//Property File price n profit calculation
$('#sale_price').keyup(function(){ 
  let sale_price = $('#sale_price').val();
  let no_of_files = $('#no_of_files').val();
  let file_price = $('#file_price').val();
  let total_no_of_files = $('#total_no_of_files').val();
  
  let total_sale_prices = sale_price * no_of_files;
  let total_sale_price = Number.parseFloat(total_sale_prices).toFixed(2);
  let remainingfiles = total_no_of_files - no_of_files;

  let remaining_files = Number.parseFloat(remainingfiles).toFixed(2);

  let remainingfile_baseprice = file_price * remainingfiles;

  $('#remaining_files').val(remaining_files);
  $('#total_sale_price').val(total_sale_price);

  let file_profits = total_sale_price - file_price;
  let file_profit = Number.parseFloat(file_profits).toFixed(2);

  let fileremaining_profits = total_sale_price - remainingfile_baseprice;
  let fileremaining_profit = Number.parseFloat(fileremaining_profits).toFixed(2);

  if(remaining_files < 1){
    $('#file_profit').val(fileremaining_profit);
  } 
  else{
    $('#file_profit').val(file_profit); 
  } 
});