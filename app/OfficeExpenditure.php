<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeExpenditure extends Model
{
     protected $fillable = ['remaining_balance'];
}
