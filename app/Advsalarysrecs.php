<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advsalarysrecs extends Model
{
	public $table = 'advsalarysrecs';
    //protected $guarded = [];

	public function employees()
	{
		return $this->belongsTo('App\Employee');
	}
	
    
}
