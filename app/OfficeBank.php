<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeBank extends Model
{
    public $table = 'officebanks';
}
