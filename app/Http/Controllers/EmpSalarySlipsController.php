<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Advsalarysrecs;
use App\Employee;
use App\EmpSalarySlips;
use Session;

class EmpSalarySlipsController extends Controller
{
     public function __construct()
     {
         $this->middleware('auth');
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = null;
        $advance_detail = null;
        $salary_month = null;
        $id = $request->input('id');
        $salary_month = $request->input('salary_month');
            if (empty($id) && empty($salary_month))
                {
                    $employees = Employee::all();
                    return view('payslips.index', compact('employees', 'advrecords'));
                }
            elseif (!empty($id) && isset($salary_month))
                {
                    $rem_bal = EmpSalarySlips::select('rem_loan_balance','installment_received', 'emp_id')->where('emp_id', $id)->get();
                    //dd($rem_bal);
                    $employee_detail = Employee::select('name', 'id', 'total_salary')->where('id', $id)->get();
                    $loan_detail = Advsalarysrecs::select('loan_amount', 'emp_id', 'monthly_installment', 'total_installments')->where('emp_id', $id)->get();
                    $employees = Employee::all();
                    return view('payslips.index', compact('employees','employee_detail', 'loan_detail', 'salary_month', 'rem_bal'));
                }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EmpSalarySlips $slips)
    {
        $slips->emp_id               = $request->emp_id;
        $slips->name                 = $request->name;
        $slips->salary_month         = $request->salary_month;
        $slips->installment_received = $request->monthly_installment;
        $slips->salary_dispatched    = $request->salary_dispatched;
        $slips->rem_loan_balance     = $request->rem_loan_balance;
        $slips->save();
        return redirect()->route('payslips.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmpSalarySlips $slips)
    {
        $slip = null;
        $month = $request->input('month');
        $slip = EmpSalarySlips::select('name','salary_month','installment_received','rem_loan_balance','salary_dispatched')->where('salary_month', $month)->get();
        //dd($slip);
        return view('payslips.slips', compact('slip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function print($id)
    {
        //Request $request, //EmpSalarySlips $slips
    }

}