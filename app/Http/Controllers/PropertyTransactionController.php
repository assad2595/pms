<?php

namespace App\Http\Controllers;
use App\Http\Controllers\PropertyController;
use Illuminate\Http\Request;
use App\Property;
use App\PropertyTransaction;
use App\Lender;
use Session;

class PropertyTransactionController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('property.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $property = Property::find(request('id'));
        $balance_amount = PropertyTransaction::where('property_id','=',$id)->get()->pluck('balance_payment')->last();
        $lenders = Lender::all();
        $data = [
            $property->id,
            $property->owner_name,
            $property->total_amount
        ];
        
        Session::flash('success', 'Property was successfully saved!');

        return view('property.property_transactions.create', compact('data', 'balance_amount', 'lenders'));
        //return redirect()->route('propertytransactions.create')->compact('data', 'balance_amount', 'lenders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PropertyTransaction $propertytransaction)
    {
        $propertytransaction->property_id        =$request->property_id;
        $propertytransaction->lender_id          =$request->lender_id;
        $propertytransaction->paymenter_name     =$request->paymenter_name;
        $propertytransaction->payment_description=$request->payment_description;
        $propertytransaction->payment_amount     =$request->payment_amount;
        $propertytransaction->balance_payment    =$request->balance_payment;
        $propertytransaction->mode_of_payment    =$request->mode_of_payment;
        $propertytransaction->cheque_no          =$request->cheque_no;
        $propertytransaction->bank_name          =$request->bank_name;
        $propertytransaction->pv_no              =$request->pv_no;
        $propertytransaction->payment_date       =$request->payment_date;

        $propertytransaction->save();

        Session::flash('success', 'Property transaction record was successfully saved!');
        
        return redirect()->route('property.show', $request->property_id );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyTransaction $propertytransaction)
    {
        $propertytransaction->payment_status = 'Paid';
        $propertytransaction->save();
        Session::flash('success', 'Lender payment has been received!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PropertyTransaction::destroy($id);

        return redirect()->route('property.index');
    }
}
