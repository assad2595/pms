<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Property;
use App\PropertyProject;
use App\PropertyTransaction;
use App\PropertyExpense;
use App\Lender;
use App\PropertyFile;
use Session;

class PropertyController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['propertys'] = Property::all(); 
        return view('property.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $propertyprojects = PropertyProject::all();
        return view('property.create', compact('propertyprojects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Property $property)
    {
        $property->propertyproject_id =$request->propertyproject_id;
        $property->owner_name         =$request->owner_name;
        $property->mouza              =$request->mouza;
        $property->acr                =$request->acr;
        $property->kanal              =$request->kanal;
        $property->marla              =$request->marla;
        $property->yard               =$request->yard;
        $property->total_marlas       =$request->total_marlas;
        $property->rate_per_acr       =$request->rate_per_acr;
        $property->rate_per_marla     =$request->rate_per_marla;
        $property->total_amount       =$request->total_amount;
        $property->point_entry        =$request->point_entry;
        $property->exemption_rate     =$request->exemption_rate;
        $property->no_of_files        =$request->no_of_files;
        $property->proprety_commission=$request->proprety_commission;
        $property->purchasing_date    =$request->purchasing_date;
        $property->save();

        Session::flash('success', 'Property record was successfully saved!');
        
        $data = [
            $property->id,
            $property->owner_name,
            $property->total_amount
        ];
        $balance_amount = null;
        $lenders = lender::all();
        return view('property.property_transactions.create', compact('data', 'lenders', 'balance_amount'))->with('message', 'Property record has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::find($id);
        $propertyprojects = PropertyProject::all();
        $propertytransactions = PropertyTransaction::where('property_id',$id)->get();
        $lenders = Lender::all();
        $propertyexpenses = PropertyExpense::where('property_id', $id)->sum('expenses_amount');

        $total_property_price = $property['total_amount'] + $propertyexpenses + $property['proprety_commission'];

        $average_file_price= round($total_property_price / $property['no_of_files'], 2);

        $propertyfiles = PropertyFile::orderBy('id','DESC')->where('property_id','=',$id)->get();
        
        $remaining_files = PropertyFile::orderBy('id','DESC')->where('property_id','=',$id)->get()->pluck('remaining_files')->first();
        $total_profit = PropertyFile::select('file_profit')->sum('file_profit');
        

        return view('property.show', compact('property','propertyprojects', 'propertytransactions', 'lenders', 'propertyexpenses', 'total_property_price', 'average_file_price', 'propertyfiles', 'remaining_files','total_profit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property)
    {
        $propertyprojects = PropertyProject::all();
        return view('property.edit',compact('property','propertyprojects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        $property->propertyproject_id=$request->propertyproject_id;
        $property->owner_name        =$request->owner_name;
        $property->mouza             =$request->mouza;
        $property->acr               =$request->acr;
        $property->kanal             =$request->kanal;
        $property->marla             =$request->marla;
        $property->yard              =$request->yard;
        $property->total_marlas      =$request->total_marlas;
        $property->rate_per_acr      =$request->rate_per_acr;
        $property->rate_per_marla    =$request->rate_per_marla;
        $property->total_amount      =$request->total_amount;
        $property->point_entry       =$request->point_entry;
        $property->exemption_rate    =$request->exemption_rate;
        $property->no_of_files       =$request->no_of_files;
        $property->proprety_commission=$request->proprety_commission;
        $property->purchasing_date    =$request->purchasing_date;
        $property->save();

        Session::flash('success', 'Property record was successfully updated!');

        return redirect()->route('property.show', $property->id );

        //return back()->withInfo('Property record has been updated successfully');
        //return redirect()->route('property', [$property]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Property::destroy($id);
       return redirect()->route('property.index');
    }

    public function delete_sold_file($id)
    {
       PropertyFile::destroy($id);
       return redirect()->back(); }


}
