<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Advsalarysrecs;
use App\Employee;
use App\EmpSalarySlips;
class AdvsalarysrecsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $id = null;
        $month = null;
        $id = $request->input('id');
        $month = $request->input('month');
        if (empty($id) && empty($month))
            {
                $loan_balance = null;
                $employees = Employee::all();
                $advrecords = Advsalarysrecs::all();
                return view('advsalarys.index', compact('employees','advrecords','loan_balance'));
            }
        elseif (!empty($month))
            {
                $loan_balance = null;
                $employees = Employee::all();
                $advrecords = Advsalarysrecs::all();
                $slips = EmpSalarySlips::select('rem_loan_balance','name','salary_month','emp_id')->where( 'rem_loan_balance', '!=', 'N/A')->where('salary_month', $month)->get();
                //dd($slips);
                return view('advsalarys.index', compact('employees','advrecords', 'slips', 'month','loan_balance'));
            }
        elseif (!empty($id))
            {
                $employees = Employee::all();
                $advrecords = Advsalarysrecs::all();
                $slips = EmpSalarySlips::select('rem_loan_balance','name','salary_month','installment_received', 'emp_id')->where('emp_id', $id)->get();
                $installments_submitted = EmpSalarySlips::select('installment_received')->where('emp_id', $id)->get();
                $actual_installments =  Advsalarysrecs::select('total_installments')->where('emp_id', $id)->sum('total_installments');
                $count_installments_submitted = $installments_submitted->count();
                $rem_installments = $actual_installments - $count_installments_submitted;
                $loan_balance = EmpSalarySlips::where('emp_id', $id)->pluck('rem_loan_balance')->last();
                //dd($loan_balance);
                return view('advsalarys.index', compact('employees','advrecords', 'slips', 'month','id','rem_installments','loan_balance'));
            }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
        $arr['emp'] = Employee::all();
        return view('advsalarys.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Advsalarysrecs $advsalarysrecs)
    {
        $advsalarysrecs->emp_id               = $request->emp_id;
        $advsalarysrecs->loan_amount          = $request->loan_amount;
        $advsalarysrecs->loan_month           = $request->loan_month;
        $advsalarysrecs->balance              = $request->balance;
        $advsalarysrecs->total_installments   = $request->total_installments;
        $advsalarysrecs->monthly_installment  = $request->monthly_installment;
        $advsalarysrecs->save();
        return redirect()->route('advsalarys.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Advsalarysrecs $advrecords)
    {
        $employees = Employee::all();
        $advrecords = Advsalarysrecs::all(); 
        return view('advsalarys.show',compact('advrecords','employees'));

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Advsalarysrecs $advsalarysrecs)
    {
        $advrecords = Advsalarysrecs::all();
        //echo $advrecords->loan_month;
        return view('advsalarys.edit', compact('advsalarysrecs', 'advrecords'));
       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advsalarysrecs $advsalarysrecs)
    {
        $advsalarysrecs->loan_amount          = $request->loan_amount;
        $advsalarysrecs->loan_month           = $request->loan_month;
        $advsalarysrecs->total_installments   = $request->total_installments;
        $advsalarysrecs->monthly_installment  = $request->monthly_installment;
        $advsalarysrecs->save();
        return redirect()->route('advsalarys.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Advsalarysrecs::destroy($id); 
        return redirect()->route('advsalarys.index');
    }
    
}
