<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OfficeBank;
use Session;

class OfficeBanksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $officebank = OfficeBank::all();
        return view('finance.banks.index', compact('officebank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('finance.banks.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, OfficeBank $officebank)
    {
        $officebank->account_title = $request->account_title;
        $officebank->account_no = $request->account_no;
        $officebank->taxpayer_status = $request->taxpayer_status;
        $officebank->starting_balance = $request->starting_balance;
        $officebank->bank_name = $request->bank_name;
        $officebank->branch_name = $request->branch_name;
        $officebank->branch_code = $request->branch_code;
        $officebank->branch_address = $request->branch_address;

        $officebank->save();
        return back()->with('success', 'Bank Account created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banks = OfficeBank::find($id);
        // dd($banks);
        return view('finance.banks.show',compact('banks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banks = OfficeBank::find($id);
        // dd($banks);
        return view('finance.banks.edit',compact('banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banks = OfficeBank::findOrFail($id);
        $banks->account_title = $request->account_title;
        $banks->account_no = $request->account_no;
        $banks->taxpayer_status = $request->taxpayer_status;
        $banks->starting_balance = $request->starting_balance;
        $banks->bank_name = $request->bank_name;
        $banks->branch_name = $request->branch_name;
        $banks->branch_code = $request->branch_code;
        $banks->branch_address = $request->branch_address;
        $banks->save();
        return redirect()->route('banks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
