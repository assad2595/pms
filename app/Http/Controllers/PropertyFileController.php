<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyFile;
use App\Property;
use App\PropertyProject;
use App\PropertyTransaction;
use App\PropertyExpense;
use App\Lender;
use Session;

class PropertyFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $property = Property::find($id);
        $project_id=$id;
        $owner_name=$property['owner_name'];
        $propertyproject_id= $property['propertyproject_id']; 
        $propertyprojects = PropertyProject::all();
        $no_of_files = $request->no_of_files;
        $sold_files = $request->sold_file;
        $file_price = $request->file_price;
        $remaining_files = PropertyFile::orderBy('id','DESC')->where('property_id','=',$project_id)->get()->pluck('remaining_files')->first();

        return view('property.property_files.create', compact('project_id','owner_name','no_of_files', 'file_price', 'sold_files', 'propertyprojects', 'propertyproject_id', 'remaining_files'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PropertyFile $propertyfile)
    {
        $propertyfile->property_id =$request->property_id;
        $propertyfile->purchaser_name =$request->purchaser_name;
        $propertyfile->no_of_files =$request->no_of_files;
        $propertyfile->file_price =$request->file_price;
        $propertyfile->sale_price =$request->sale_price;
        $propertyfile->total_sale_price =$request->total_sale_price;
        $propertyfile->remaining_files = $request->remaining_files;
        $propertyfile->file_profit =$request->file_profit;

        $propertyfile->save();

        Session::flash('success', 'Property File sale record was successfully saved!');

        return redirect()->route('property.show', $request->property_id );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $propertyfile =PropertyFile::find($id);
        return view('property.property_files.edit', compact('propertyfile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyFile $propertyfile)
    {
        $propertyfile->property_id =$request->property_id;
        $propertyfile->purchaser_name =$request->purchaser_name;
        $propertyfile->sale_price =$request->sale_price;
        $propertyfile->total_sale_price =$request->total_sale_price;
        $propertyfile->file_profit =$request->file_profit;
        $propertyfile->save();

        Session::flash('success', 'Property File sale record was successfully updated!');

        return redirect()->route('property.show', $request->property_id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PropertyFile::destroy($id);
       return redirect()->back();
    }
}
