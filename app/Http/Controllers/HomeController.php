<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use App\OfficeDeposit;
use App\OfficeExpenditure;
use App\PropertyProject;
use App\Property;
use App\Employee;
use App\OfficeBank;
use App\BankTransaction;
use App\Lender;
use App\PropertyTransaction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $countavailablefiles = File::where('file_status','Available')->count(); 
        $countsoldfiles = File::where('file_status','Sold')->count(); 

        $startdate = date('Y-m-01');
        $enddate = date('Y-m-31');
        $ip = $request->ip();
        //$datetime = Carbon\Carbon::now();
        //echo $mytime->toDateTimeString();
        //dd($datetime);
        $totalexpense = OfficeExpenditure::select('amount')->whereBetween('date', [$startdate, $enddate])->sum('amount');

         $remainingbalance= OfficeExpenditure::orderBy('id','DESC')->pluck('remaining_balance')->first();
        // dd($remainingbalance);

        $remainingbalance= OfficeExpenditure::orderBy('id','DESC')->pluck('remaining_balance')->first();
        $totalpropertyProjects = PropertyProject::all()->count();
        $totalproperties = Property::all()->count();
        $totallenders = lender::all()->count();
        $totalemp = Employee::all()->count();
        $totalbanks = OfficeBank::all()->count();

        $totalbanktrans = BankTransaction::select('trans_amount')->whereBetween('trans_date', [$startdate, $enddate])->sum('trans_amount');
        $totalcredit = BankTransaction::select('trans_amount')->whereBetween('trans_date', [$startdate, $enddate])->where('trans_type','Credit')->sum('trans_amount');
        $totaldebit = BankTransaction::select('trans_amount')->whereBetween('trans_date', [$startdate, $enddate])->where('trans_type','Debit')->sum('trans_amount');
        $totalwhtax = BankTransaction::select('w_h_tax')->whereBetween('trans_date', [$startdate, $enddate])->where('trans_type','Debit')->sum('w_h_tax');

        $lendersamount = PropertyTransaction::select('payment_amount')->whereBetween('created_at', [$startdate, $enddate])->where('payment_status','unPaid')->sum('payment_amount');

        return view('dashboard',compact('countavailablefiles','countsoldfiles','totalexpense','remainingbalance','totalpropertyProjects','totalproperties','totallenders','totalemp','totalbanks','ip','totalbanktrans','totalcredit','totaldebit','totalwhtax','lendersamount'));

    }
    
}
