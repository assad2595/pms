<?php



namespace App\Http\Controllers;

use App\OfficeExpenditure;

use App\OfficeDeposit;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Session;



class OfficeExpenditureController extends Controller

{

    public function __construct(){

        $this->middleware('auth');

    }





    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

       $arr['officeexpenditure'] = OfficeExpenditure::orderBy('id','DESC')->get();

       return view('officeexpenditure.index')->with($arr);

   }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    { 

        $last_remaining_balance= OfficeExpenditure::orderBy('id','DESC')->pluck('remaining_balance')->first();

        if($last_remaining_balance===null){

            $last_remaining_balance=0;

        }

        // $deposit = OfficeDeposit::select('deposited')->sum('deposited');

        return view('officeexpenditure.create', compact('deposit','last_remaining_balance'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request,OfficeExpenditure $officeexpenditure)

    {

        $officeexpenditure->item_name           = $request->item_name;

        $officeexpenditure->description         = $request->description;

        $officeexpenditure->date                = $request->date;

        $officeexpenditure->amount              = $request->amount;

        $officeexpenditure->remaining_balance   = $request->remaining_balance;

        $officeexpenditure->save();

        Session::flash('success', 'Expense record has been successfully save!');

        return redirect()->route('officeexpenditure.index');

    }





    public function show($id)

    {

    }



    

    public function edit($id)

    {

       $last_remaining_balance= OfficeExpenditure::orderBy('id','DESC')->pluck('remaining_balance')->first();

       if($last_remaining_balance===null){

        $last_remaining_balance=0;

    }

        // dd($last_remaining_balance);

    $officeexpenditure=OfficeExpenditure::find($id);

    return view('officeexpenditure.edit',compact('officeexpenditure','last_remaining_balance'));

}



public function update(Request $request,OfficeExpenditure $officeexpenditure)

{

    $officeexpenditure->item_name           = $request->item_name;

    $officeexpenditure->description         = $request->description;

    $officeexpenditure->date                = $request->date;

    $officeexpenditure->amount              = $request->amount;

    $officeexpenditure->remaining_balance   = $request->remaining_balance;

    $officeexpenditure->save();

    $officeexpenditure = new OfficeExpenditure();

    $officeexpenditure->remaining_balance   =$request->input('remaining_balance');

    $officeexpenditure->save();

    Session::flash('success', 'Expense record has been successfully updated!');

    return redirect()->route('officeexpenditure.index');

}



public function destroy($id)

{

 OfficeExpenditure::destroy($id);

 Session::flash('error', 'Deleted record successfully!');

 return redirect()->route('officeexpenditure.index');

}



public function deposit()

{

    $officedeposits = OfficeDeposit::orderBy('id','DESC')->get();


    return view('officeexpenditure.deposit', compact('officedeposits'));

}



public function addofficedeposit(Request $request){

    $officedeposit = new OfficeDeposit();

    $officedeposit->deposited   =$request->input('deposited');

    $officedeposit->date        =$request->input('date');   

    $officedeposit->save();

    $last_remaining_balance= OfficeExpenditure::orderBy('id','DESC')->pluck('remaining_balance')->first();

    $officeexpenditure = new OfficeExpenditure();

    $officeexpenditure->remaining_balance =$request->input('deposited')+$last_remaining_balance;

    $officeexpenditure->save(); 

    $officedeposits = OfficeDeposit::orderBy('id','DESC')->get();

    Session::flash('success', 'Deposit record has been successfully save!');


    return back()->with('officedeposits');
}

public function monthlyreport(Request $request){

    $monthlydeposit=null;

    $monthlyexpenses=null;

    return view('officeexpenditure.monthlyreport', compact('monthlydeposit','monthlyexpenses'));

}







public function deleteofficedeposit($id){

    $deposit = OfficeDeposit::find($id);
    $last_remaining_balance= OfficeExpenditure::orderBy('id','DESC')->pluck('remaining_balance')->first();
    $rem_amount=$last_remaining_balance - $deposit->deposited;
    $insertdata=OfficeExpenditure::orderBy('id','DESC')->first()
    ->update([

        'remaining_balance'             => $rem_amount,

    ]);
    OfficeDeposit::where('id',$id)->delete();

    return $this->deposit();

} 



public function checkmonthlyexpenses(Request $request){

    $month = $request->input('expenses_month');

    $time = strtotime($month);

    $startdate = date('Y-m-01',$time);

    $enddate = date('Y-m-31',$time);



    $monthlyexpenses=OfficeExpenditure::orderBy('id','DESC')->whereBetween('date', [$startdate, $enddate])->get();

    $total_deposit = OfficeDeposit::select('deposited')->whereBetween('date', [$startdate, $enddate])->sum('deposited');

        // dd($deposit);

        // dd($monthlyexpenses);

    $monthlydeposit=null;

    return view('officeexpenditure.monthlyreport', compact('monthlyexpenses','monthlydeposit','total_deposit'));

}

public function checkmonthlydeposit(Request $request){

    $month = $request->input('deposit_date');

    $time = strtotime($month);

    $startdate = date('Y-m-01',$time);

    $enddate = date('Y-m-31',$time);



    $monthlydeposit=OfficeDeposit::orderBy('id','DESC')->whereBetween('date', [$startdate, $enddate])->get();



    $monthlyexpenses=null;

        // dd($monthlydeposit);

    return view('officeexpenditure.monthlyreport', compact('monthlydeposit','monthlyexpenses'));

}

}

