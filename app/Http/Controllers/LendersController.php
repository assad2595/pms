<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lender;
use App\PropertyProject;
use App\Property;
use App\PropertyTransaction;
use Session;

class LendersController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lender = lender::all();
        return view('lenders.lenders', compact('lender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lenders.lenders');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, lender $lender)
    {
        $lender->lender_name = $request->lender_name;
        $lender->lender_business_name = $request->lender_business_name;
        $lender->lender_contactno = $request->lender_contactno;
        $lender->lender_address = $request->lender_address;
        $lender->save();
        Session::flash('success', 'Lender was successfully saved!');
        return back()->with('success', 'Lender created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $editlender = Lender::find($id);
        $properties = Property::all();
        $propertyprojects = PropertyProject::all();
        $propertytransactions = PropertyTransaction::where('lender_id',$id)->get();
        return view('lenders.show', compact('editlender', 'propertytransactions', 'propertyprojects', 'properties'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editlender = Lender::find($id);
        $lender = lender::all();
        return view('lenders.edit', compact('lender','editlender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,lender $lender)
    {
        $lender->lender_name            = $request->lender_name;
        $lender->lender_business_name   = $request->lender_business_name;
        $lender->lender_contactno       = $request->lender_contactno;
        $lender->lender_address         = $request->lender_address;
        $lender->save();
        Session::flash('success', 'Lender information was successfully updated!');
        $lender = lender::all();
        return view('lenders.lenders', compact('lender'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
