<?php

namespace App\Http\Controllers;
use App\Http\Controllers\FilesController;
use Illuminate\Http\Request;
use App\FileTransaction;
use App\File;
use Session;
use Illuminate\Support\Facades\App;

 
class FilePurchaseTransactionController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public function index()
  {
    $arr['filetransactions'] = FileTransaction::all();
    return view('file.filetransaction.index')->with($arr);
  }

  public function sold()
  {
    $arr['filetransactions'] = FileTransaction::all();
    return view('file.filetransaction.sold')->with($arr);
  }

  public function create($id)
  {
     $file = File::find(request('id'));
     $last_amount = FileTransaction::orderBy('id','ASC')->where('file_id','=',$id)->get()->pluck('p_amount_balance')->last();
       $data = [
            $file->id,
            $file->p_name,
            $file->p_price,
        ];

    return view('file.filetransaction.create', compact('data','last_amount'));
  }

  public function store(Request $request, FileTransaction $filetransaction)
  {
    $filetransaction->file_id                 = $request->file_id;
    $filetransaction->p_amount_received       = $request->p_amount_received;
    $filetransaction->p_amount_balance        = $request->p_amount_balance;
    $filetransaction->p_mode_of_receipt       = $request->p_mode_of_receipt;
    $filetransaction->p_rvno                  = $request->p_rvno;
    $filetransaction->p_rvdate                = $request->p_rvdate;
    $filetransaction->p_bank                  = $request->p_bank;
    $filetransaction->cheque_no               = $request->cheque_no;
    $filetransaction->save();

    $file = File::find($request->file_id);
    $filetransaction = FileTransaction::orderBy('id','ASC')->where('file_id',$request->file_id)->get(); 
    Session::flash('success', 'File purchase transacton has been successfully save!');
    return redirect()->route('file.show',compact(['file','filetransaction']));
  }

  public function show(FileTransaction $filetransaction)
  {
    $arr['filetransaction']=$filetransaction;
    return view('file.filetransaction.show')->with($arr);
  }

  public function edit(FileTransaction $filetransaction)
  {
   $arr['filetransaction']=$filetransaction;
   return view('file.filetransaction.edit')->with($arr);
 }
 
 public function update(Request $request,FileTransaction $filetransaction)
 {

  $filetransaction->p_amount_received       = $request->p_amount_received;
  $filetransaction->p_amount_balance        = $request->p_amount_balance;
  $filetransaction->p_mode_of_receipt       = $request->p_mode_of_receipt;
  $filetransaction->p_rvno                  = $request->p_rvno;
  $filetransaction->p_rvdate                = $request->p_rvdate;
  $filetransaction->p_bank                  = $request->p_bank;
  $filetransaction->cheque_no               = $request->cheque_no;
  $filetransaction->save();
  return redirect()->route('file.filetransaction.index');
}


public function destroy($id)
{
  FileTransaction::destroy($id);
  return redirect()->route('file.filetransaction.index');
}
}
