
<?php



namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\File;

use App\FileTransaction;

use App\FileSaleTransaction;

use App\Http\Controllers\FilePurchaseTransactionController;

use App\Http\Controllers\FileSaleTransactionController;

use PDF;

use Illuminate\Support\Facades\Redirect;

use Session;





class FilesController extends Controller

{

  public function __construct(){

    $this->middleware('auth');

  }

  

  public function index()

  {

    $arr['files'] = File::orderBy('id','DESC')->get();

    return view('file.index')->with($arr);

  }



  public function create()

  {

    return view('file.create');

  }



  public function store(Request $request, File $file)

  {

    $file->p_name                  = $request->p_name;

    $file->p_co                    = $request->p_co;

    $file->p_mobilenumber          = $request->p_mobilenumber;

    $file->landpur_no              = $request->landpur_no;

    $file->p_price                 = $request->p_price;

    $file->p_date                  = $request->p_date;

    $file->p_contents              = $request->p_contents;

    $file->p_payment_status        = $request->p_payment_status;

    $file->cat_of_file             = $request->cat_of_file;

    $file->type_of_file            = $request->type_of_file;

    $file->file_status             = $request->file_status;

    $file->save();



    Session::flash('success', 'File Purchase record has been successfully save!');

    return redirect()->route('filetransaction.create',$file->id);

  }



  public function show($id)

  {

   $file = File::find($id);

   $filetransaction = FileTransaction::orderBy('id','DESC')->where('file_id',$id)->get(); 

   return view('file.show',compact(['file','filetransaction']));

 }



 public function edit(File $file)

 {

   $arr['file']=$file;

   return view('file.edit')->with($arr);

 }

 

 public function update(Request $request,File $file)

 {

  $file->p_name                  = $request->p_name;

  $file->p_co                    = $request->p_co;

  $file->p_mobilenumber          = $request->p_mobilenumber;

  $file->landpur_no              = $request->landpur_no;

  $file->p_price                 = $request->p_price;

  $file->p_date                  = $request->p_date;

  $file->p_contents              = $request->p_contents;

  $file->p_payment_status        = $request->p_payment_status;

  $file->cat_of_file             = $request->cat_of_file;

  $file->type_of_file            = $request->type_of_file;

  $file->file_status             = $request->file_status;

  $file->save();

  Session::flash('success', 'File Purchase record has been successfully updated!');

  return redirect()->route('file.index');

}





public function destroy($id)

{

  File::destroy($id);

  Session::flash('error', 'File deleted successfully!');

  return redirect()->route('file.index');

}

//show all sold files

public function sale()

{

  $arr['files'] = File::orderBy('id','DESC')->get();

  return view('file.indexfilesale')->with($arr);

}

//go to add new sale

public function createfilesale(Request $request,$id)

{

  $file = File::find($id);

  return view('file.createfilesale',['file'=>$file]);

}

//save created file sale record

public function filesale(Request $request, $id)

{

  $file = File::find($id);

  $insertdata=File::where('id',$id)

  ->update([

    'file_status'             => $request->input('file_status'),

    's_name'                  => $request->input('s_name'),

    's_co'                    => $request->input('s_co'),

    's_mobilenumber'          => $request->input('s_mobilenumber'),

    's_price'                 => $request->input('s_price'),

    's_date'                  => $request->input('s_date'),

    's_contents'              => $request->input('s_contents'),

    'profit'                  => $request->input('s_price')-$file->p_price,

  ]);

  Session::flash('success', 'File Sale record has been successfully save!');

  return redirect()->route('filesaletransaction.create',$file->id);

      // return redirect()->route('file.index');

}



  //only view record

public function showfilesale($id)

{

  $file = File::find($id);

  $filesaletransaction = FileSaleTransaction::orderBy('id','DESC')->where('file_id',$id)->get(); 

  return view('file.showfilesale' ,compact(['file','filesaletransaction']));

} 



public function editfilesale(Request $request, $id)

{

  $file = File::find($id);

  return view('file.editfilesale' , [ 'file' => $file]);

}



public function updatefilesale(Request $request, $id)

{

  $insertdata=File::where('id',$id)

  ->update([

    's_name'                  => $request->input('s_name'),

    's_co'                    => $request->input('s_co'),

    's_mobilenumber'          => $request->input('s_mobilenumber'),

    's_price'                 => $request->input('s_price'),

    's_date'                  => $request->input('s_date'),

    's_contents'              => $request->input('s_contents'),

  ]);

  $arr['files'] = File::all();

  Session::flash('success', 'File Sale record has been successfully updated!');

  return view('file.indexfilesale')->with($arr);

}



public function viewpurchasetransaction(File $file){

  $arr['filetransactions'] = FileTransaction::all();

  return view('file.show')->with($arr);

  return view('file.filetransaction.index')->with($arr);

}



public function showfilehistory(Request $request, $id)

{

  $file = File::find($id);

  $filetransaction = FileTransaction::orderBy('id','DESC')->where('file_id',$id)->get(); 

  $filesaletransaction = FileSaleTransaction::orderBy('id','DESC')->where('file_id',$id)->get(); 

  return view('file.showfilehistory' , compact(['file', 'filetransaction', 'filesaletransaction']));

} 

public function printshowfilepurchasehistory(Request $request, $id)

{

  // $pdf = PDF::loadView('file.printshowfilepurchasehistory'); //

  return view('file.printshowfilepurchasehistory');

}



public function print()

{

   

  // return  PDF::loadView('file.printfilehistory');

  // return view('file.printfilehistory');

}  

public function profit(){

  $arr['file'] = File::orderBy('id','DESC')->get();

  return view('file.profit')->with($arr);

}





}

