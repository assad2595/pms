<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Employee;
use Session;

class EmployeesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['employees'] = Employee::all();
        return view('employees.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $employee->name             = $request->name;
        $employee->father_name      = $request->father_name;
        $employee->cnic             = $request->cnic;
        $employee->dob              = $request->dob;
        $employee->gender           = $request->gender;
        $employee->email            = $request->email;
        $employee->phone_no         = $request->phone_no;
        $employee->address          = $request->address;
        $employee->designation      = $request->designation;
        $employee->joining_date     = $request->joining_date;
        $employee->salary           = $request->salary;
        $employee->lunch_allowance  = $request->lunch_allowance;
        $employee->allowance        = $request->allowance;
        $employee->total_salary     = $request->total_salary;
        $employee->save();
        return redirect()->route('employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $arr['employee'] = $employee;
        return view('employees.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $arr['employee'] = $employee;
        return view('employees.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->name             = $request->name;
        $employee->father_name      = $request->father_name;
        $employee->cnic             = $request->cnic;
        $employee->dob              = $request->dob;
        $employee->gender           = $request->gender;
        $employee->email            = $request->email;
        $employee->phone_no         = $request->phone_no;
        $employee->address          = $request->address;
        $employee->designation      = $request->designation;
        $employee->joining_date     = $request->joining_date;
        $employee->salary           = $request->salary;
        $employee->lunch_allowance  = $request->lunch_allowance;
        $employee->allowance        = $request->allowance;
        $employee->total_salary     = $request->total_salary;
        $employee->leaving_reason   = $request->leaving_reason;
        $employee->leaving_date     = $request->leaving_date;
        $employee->job_status       = $request->job_status;
        $employee->save();
        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::destroy($id);
        return redirect()->route('employees.index');
    }
    

}
