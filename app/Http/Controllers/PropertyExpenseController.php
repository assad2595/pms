<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyExpense;
use App\Property;
use Session;

class PropertyExpenseController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // $propertyexpenses = PropertyExpense::all();
        // return view('property.property_project.propertyprojects', compact('propertyProjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $property_id = $id;
        $propertyexpenses = PropertyExpense::orderBy('id','ASC')->where('property_id','=', $id)->get(); 
        return view('property.property_expenses.propertyexpenses', compact('property_id', 'propertyexpenses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PropertyExpense $propertyexpense)
    {
        $propertyexpense->property_id=$request->property_id;
        $propertyexpense->expenses_name=$request->expenses_name;
        $propertyexpense->expenses_amount=$request->expenses_amount;
        $propertyexpense->mode_of_payment=$request->mode_of_payment;
        $propertyexpense->cheque_no=$request->cheque_no;
        $propertyexpense->bank_name=$request->bank_name;
        $propertyexpense->payment_date=$request->payment_date;
        $propertyexpense->save();
        
        Session::flash('success', 'Property Expenses has been added successfully.');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
