<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\FileTransaction;

use App\FileSaleTransaction;

use App\File;

use Session;



class FileSaleTransactionController extends Controller

{

  public function __construct(){

    $this->middleware('auth');

  }

  

  public function index()

  {

    $arr['filesaletransactions'] = FileSaletransaction::all();

    return view('file.filesaletransaction.index')->with($arr);

  }



  public function sold()

  {

    $arr['filesaletransactions'] = FileSaletransaction::all();

    return view('file.filesaletransaction.sold')->with($arr);

  }



  public function create($id)

  {

     $file = File::find(request('id'));

     $last_amount = FileSaletransaction::orderBy('id','DESC')->where('file_id','=',$id)->get()->pluck('s_amount_balance')->first();

       $data = [

            $file->id,

            $file->s_name,

            $file->s_price,

        ];



    return view('file.filesaletransaction.create', compact('data','last_amount'));

  }



  public function store(Request $request, FileSaletransaction $filesaletransaction)

  {

    $filesaletransaction->file_id                 = $request->file_id;

    $filesaletransaction->s_amount_paid           = $request->s_amount_paid;

    $filesaletransaction->s_amount_balance        = $request->s_amount_balance;

    $filesaletransaction->s_mode_of_receipt       = $request->s_mode_of_receipt;

    $filesaletransaction->s_rvno                  = $request->s_rvno;

    $filesaletransaction->s_rvdate                = $request->s_rvdate;

    $filesaletransaction->s_bank                  = $request->s_bank;

    $filesaletransaction->cheque_no               = $request->cheque_no;

    $filesaletransaction->save();

    $file = File::find($request->file_id);

    $filesaletransaction = FileSaleTransaction::orderBy('id','ASC')->where('file_id',$request->file_id)->get();

    Session::flash('success', 'File sale transaction has been successfully save!');



    //return view('file.showfilesale' ,compact(['file','filesaletransaction']));



    return redirect()->route('file.showfilesale', $request->file_id )->with(compact('file', 'filesaletransaction'));

  }



  public function show(FileSaletransaction $filesaletransaction)

  {

    $arr['filesaletransaction']=$filesaletransaction;

    // $arr2['file']=$file;



    // return view('pathTo.someView', compact('groupOne', 'groupOne'));

    return view('file.filesaletransaction.show')->with($arr);

  }



  public function edit(FileSaletransaction $filesaletransaction)

  {

   $arr['filesaletransaction']=$filesaletransaction;

   return view('file.filesaletransaction.edit')->with($arr);

 }

 

 public function update(Request $request,FileSaletransaction $filesaletransaction)

 {



  $filesaletransaction->s_amount_paid           = $request->s_amount_paid;

  $filesaletransaction->s_amount_balance        = $request->s_amount_balance;

  $filesaletransaction->s_mode_of_receipt       = $request->s_mode_of_receipt;

  $filesaletransaction->s_rvno                  = $request->s_rvno;

  $filesaletransaction->s_rvdate                = $request->s_rvdate;

  $filetransaction->cheque_no                   = $request->cheque_no;

  $filesaletransaction->s_bank                  = $request->s_bank;

  $filesaletransaction->save();

  return redirect()->route('file.filesaletransaction.index');

}





public function destroy($id)

{

  FileSaletransaction::destroy($id);

  return redirect()->route('file.filesaletransaction.index');

}

}

