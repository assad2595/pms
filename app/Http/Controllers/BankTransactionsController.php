<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BankTransaction;
use App\OfficeBank;
use Sessions;


class BankTransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $total_credit = null;
        $total_debit = null;
        $starting_balance = null;
        $balance = null;
        $from = null;
        $to = null;
        $bank_id = $request->input('id');
        $from = $request->input('from');
        $to = $request->input('to');
        if (empty($from) && empty($to))
        {
            $transactions = BankTransaction::where('bank_account_id', $bank_id)->get();
            $total_credit = BankTransaction::select('trans_type', 'trans_amount')->where('trans_type','=', 'Credit')->where('bank_account_id', $bank_id)->sum('trans_amount');
            $total_debit = BankTransaction::select('trans_type', 'trans_amount')->where('trans_type','=', 'Debit')->where('bank_account_id', $bank_id)->sum('trans_amount');
            $starting_balance = Officebank::select('starting_balance')->where('id', $bank_id)->sum('starting_balance');
            $account_details = Officebank::select('bank_name', 'account_title')->where('id', $bank_id)->get();
            $balance =$starting_balance+$total_credit-$total_debit;
            $w_h_tax = BankTransaction::where('bank_account_id', $bank_id)->sum('w_h_tax');
            //dd($total_credit,$total_debit,$starting_balance,$balance);
            //dd($transactions);
            //dd($w_h_tax);
            $bank_accounts = OfficeBank::all();
            return view('finance.bank_transactions.index', compact('transactions', 'bank_accounts', 'starting_balance', 'total_credit','total_debit', 'balance', 'account_details', 'from', 'to', 'w_h_tax'));
        }
        elseif (!empty($from) && !empty($to))
        {
            $transactions = BankTransaction::where('bank_account_id', $bank_id)->whereBetween('trans_date', [$from, $to])->get();
            $total_credit = BankTransaction::select('trans_type', 'trans_amount')->where('trans_type','=', 'Credit')->where('bank_account_id', $bank_id)->whereBetween('trans_date', [$from, $to])->sum('trans_amount');
            $total_debit = BankTransaction::select('trans_type', 'trans_amount')->where('trans_type','=', 'Debit')->where('bank_account_id', $bank_id)->whereBetween('trans_date', [$from, $to])->sum('trans_amount');
            $starting_balance = Officebank::select('starting_balance')->where('id', $bank_id)->sum('starting_balance');
            $account_details = Officebank::select('bank_name', 'account_title')->where('id', $bank_id)->get();
            $w_h_tax = BankTransaction::where('bank_account_id', $bank_id)->whereBetween('trans_date', [$from, $to])->sum('w_h_tax');
            $balance =$starting_balance+$total_credit-$total_debit;
            //dd($from_date,$to_date,$total_credit,$total_debit, $balance);
            //dd($total_credit,$total_debit);
            //dd($total_credit,$total_debit,$starting_balance,$balance);
            //dd($transactions);
            //dd($account_details);
            //dd($w_h_tax);
            $bank_accounts = OfficeBank::all();
            return view('finance.bank_transactions.index', compact('transactions', 'bank_accounts', 'starting_balance', 'total_credit','total_debit', 'balance', 'account_details', 'from', 'to', 'w_h_tax'));
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $officebank = OfficeBank::all();
        //dd($officebank);
        return view('finance.bank_transactions.create', compact('officebank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BankTransaction $banktransactions)
    {   
        $banktransactions->bank_account_id = $request->bank_account_id;;
        $banktransactions->trans_amount = $request->trans_amount;
        $banktransactions->trans_date = $request->trans_date;
        $banktransactions->trans_type = $request->trans_type;
        $banktransactions->payment_method = $request->payment_method;
        $banktransactions->trans_descp = $request->trans_descp;
        $banktransactions->w_h_tax = $request->w_h_tax;
        $banktransactions->bank_charges = $request->bank_charges;
        $banktransactions->cheque_no = $request->cheque_no;

        $banktransactions->save();
        return redirect()->route('bank_transaction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $transaction = BankTransaction::where('id',$id)->get();
        $officebank = OfficeBank::all();
        return view('finance.bank_transactions.show', compact('transaction', 'officebank'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $transaction = BankTransaction::find($id);
        $account_id = BankTransaction::select('bank_account_id')->where('id', $id)->get();
        //dd($account_id);
        $banks = OfficeBank::all();
        //dd($transaction,$banks);
        
        return view('finance.bank_transactions.edit', compact('transaction', 'banks', 'account_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $banktransactions = BankTransaction::findOrFail($id);
        $banktransactions->bank_account_id = $request->bank_account_id;
        $banktransactions->trans_amount = $request->trans_amount;
        $banktransactions->trans_date = $request->trans_date;
        $banktransactions->trans_type = $request->trans_type;
        $banktransactions->payment_method = $request->payment_method;
        $banktransactions->trans_descp = $request->trans_descp;
        $banktransactions->w_h_tax = $request->w_h_tax;
        $banktransactions->bank_charges = $request->bank_charges;
        $banktransactions->cheque_no = $request->cheque_no;
        $banktransactions->save();
        return redirect()->route('bank_transaction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BankTransaction::destroy($id);
        return redirect()->route('bank_transaction.index');
    }
}
