<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyExpense extends Model
{
    public $table = 'propertyexpenses';
}
