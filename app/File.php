<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	public function filetransaction() {
    return $this->hasMany('App\FileTransaction');
	}
	public function filesaletransaction() {
    return $this->hasMany('App\FileSaleTransaction');
	}
}
