<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTransaction extends Model
{
    public $table = "propertytransactions";

    public function lender(){
    	return $this->belongsTo('App\Lender');
    }
}
