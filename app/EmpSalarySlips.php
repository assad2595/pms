<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpSalarySlips extends Model
{
     	protected $table = 'emp_salary_slips';
     	//protected $primaryKey = null;
     	protected $fillable = ['emp_id','name','installment_received','salary_dispatched','rem_loan_balance','salary_month','created_at','updated_at'];

}
