<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
	public $table = 'propertys';
    protected $guarded = [];
    
    public function propertyproject(){
    	return $this->belongsTo('App\PropertyProject');
    }

    public function propertytransaction(){
    	return $this->belongsTo('App\PropertyTransaction');
    }

    public function propertyexpense(){
    	return $this->belongsTo('App\PropertyExpense');
    }

    public function lender(){
        return $this->belongsTo('App\Lender');
    }

    public function PropertyFile(){
        return $this->belongsTo('App\PropertyFile');    
    }
}
