<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	public function advsalarysrecs()
	{
     	return $this->hasMany('App\Advsalarysrecs');
	}
   
}
