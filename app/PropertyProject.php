<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyProject extends Model
{
    public $table = 'propertyprojects';
}
