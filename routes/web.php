<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//---------------------------------------------------------------Property Routes
Route::resource('property', 'PropertyController');
Route::resource('propertyproject','PropertyProjectController');
Route::resource('propertytransactions','PropertyTransactionController');
Route::resource('propertyfiles', 'PropertyFileController');

Route::get('/propertytransactions/create/{id}', 'PropertyTransactionController@create')->name('propertytransactions.create');
Route::resource('propertyexpenses','PropertyExpenseController');
Route::get('/propertyexpenses/create/{id}', 'PropertyExpenseController@create')->name('propertyexpenses.create');
Route::post('/propertyfiles/sale/{id}', 'PropertyFileController@create')->name('propertyfiles.sale');
//Lenders Routes
Route::resource('lenders','LendersController');
//Office Banks and Bank Transaction Routes
Route::resource('banks','OfficeBanksController');
Route::resource('bank_transaction','BankTransactionsController');
//Route::get('/transactions','BankTransactionsController@bankTransactionsDatewise')->name('transactionsdatewise');
//Route::view('/welcome', 'pdashboard');
//----------------------------------------------------------------Emplooyees Routes
Route::resource('payslips', 'EmpSalarySlipsController');	
Route::resource('employees', 'EmployeesController');
Route::resource('advsalarys', 'AdvsalarysrecsController');

//Files Route
Route::get('/file/profit', 'FilesController@profit');
Route::get('/file/sale', 'FilesController@sale');
Route::get('/file/createfilesale/{id}', 'FilesController@createfilesale');
Route::get('/file/showfilesale/{id}', 'FilesController@showfilesale')->name('file.showfilesale');
Route::get('/file/editfilesale/{id}', 'FilesController@editfilesale');
Route::post('/file/updatefilesale/{id}', 'FilesController@updatefilesale');
Route::post('/file/filesale/{id}', 'FilesController@filesale');
//related to file purchase
Route::resource('file', 'FilesController');
//related to file transaction
Route::resource('filetransaction','FilePurchaseTransactionController');
//passing parameter to file transaction create method in controller
Route::get('filetransaction/create/{id}', 'FilePurchaseTransactionController@create')->name('filetransaction.create');
//file sale trnasaction
Route::resource('filesaletransaction','FileSaleTransactionController');
//passing parameter to file transaction create method in controller
Route::get('filesaletransaction/create/{id}', 'FileSaleTransactionController@create')->name('filesaletransaction.create');
//file purchase history when click on 'view purchase history' button
Route::get('/file/showfilehistory/{id}', 'FilesController@showfilehistory');
//for print
Route::get('/file/printfilehistory/{id}', 'FilesController@print');

//for office expenditure
Route::resource('officeexpenditure', 'OfficeExpenditureController');
Route::get('/deposit', 'OfficeExpenditureController@deposit')->name('officeexpenditure');
Route::post('/addofficedeposit', 'OfficeExpenditureController@addofficedeposit');
Route::post('/deleteofficedeposit/{id}', 'OfficeExpenditureController@deleteofficedeposit');
Route::get('/monthlyreport', 'OfficeExpenditureController@monthlyreport');
Route::post('/checkmonthlyexpenses', 'OfficeExpenditureController@checkmonthlyexpenses');
Route::post('/checkmonthlydeposit', 'OfficeExpenditureController@checkmonthlydeposit');

