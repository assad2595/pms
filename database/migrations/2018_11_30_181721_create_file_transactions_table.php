<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_id');
            $table->string('p_amount_received');
            $table->string('p_amount_balance');
            $table->string('p_mode_of_receipt');
            $table->string('p_rvno')->nullable();
            $table->string('p_rvdate')->nullable();
            $table->string('cheque_no')->nullable();
            $table->string('p_bank')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_transactions');
    }
}
