<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyexpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertyexpenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_id');
            $table->string('expenses_name');
            $table->string('expenses_amount');
            $table->string('mode_of_payment');
            $table->string('cheque_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('pv_no')->nullable();
            $table->string('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propertyexpenses');
    }
}
