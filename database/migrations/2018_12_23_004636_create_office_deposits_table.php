<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeDepositsTable extends Migration
{
    public function up()
    {
        Schema::create('office_deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deposited');
            $table->string('date');
            $table->timestamps();
        });
    }

     
    public function down()
    {
        Schema::dropIfExists('office_deposits');
    }
}
