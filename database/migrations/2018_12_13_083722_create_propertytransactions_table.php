<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertytransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertytransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_id');
            $table->string('lender_id')->nullable();
            $table->string('paymenter_name')->nullable();
            $table->string('payment_description')->nullable();
            $table->string('payment_amount');
            $table->string('balance_payment');
            $table->string('mode_of_payment');
            $table->string('cheque_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('pv_no')->nullable();
            $table->string('payment_status')->default('unPaid');
            $table->string('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propertytransactions');
    }
}
