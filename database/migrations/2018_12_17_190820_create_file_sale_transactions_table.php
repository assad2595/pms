<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileSaleTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_sale_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_id');
            $table->string('s_amount_paid');
            $table->string('s_amount_balance');
            $table->string('s_mode_of_receipt');
            $table->string('s_rvno')->nullable();
            $table->string('s_rvdate')->nullable();
            $table->string('s_bank')->nullable();
            $table->string('cheque_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_sale_transactions');
    }
}
