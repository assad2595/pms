<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertyfiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_id');
            $table->string('purchaser_name');
            $table->string('no_of_files');
            $table->string('file_price');
            $table->string('sale_price');
            $table->string('total_sale_price');
            $table->string('file_profit');
            $table->string('remaining_files');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propertyfiles');
    }
}
