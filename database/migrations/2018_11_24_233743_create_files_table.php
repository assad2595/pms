<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('p_name')->nullable();
            $table->string('p_co')->nullable();
            $table->string('p_mobilenumber')->nullable();
            $table->string('p_price')->nullable();
            $table->string('p_date')->nullable();
            $table->string('p_contents')->nullable();
            $table->string('p_payment_status')->nullable();
            $table->string('s_name')->nullable();
            $table->string('s_co')->nullable();
            $table->string('s_mobilenumber')->nullable();
            $table->string('s_price')->nullable();
            $table->string('s_date')->nullable();
            $table->string('s_contents')->nullable();
            $table->string('s_payment_status')->nullable();
            $table->string('file_status')->nullable();
            $table->string('cat_of_file')->nullable();
            $table->string('type_of_file')->nullable();
            $table->string('profit')->nullable();
            $table->string('landpur_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
