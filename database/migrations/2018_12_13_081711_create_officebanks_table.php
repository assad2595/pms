<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficebanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officebanks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_title');
            $table->string('account_no');
            $table->string('taxpayer_status');
            $table->string('starting_balance');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('branch_code');
            $table->string('branch_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officebanks');
    }
}
