<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('father_name');
            $table->string('cnic');
            $table->string('dob');
            $table->string('gender');
            $table->string('designation');
            $table->string('joining_date');
            $table->string('address');
            $table->string('phone_no');
            $table->string('email')->nullable();
            $table->string('lunch_allowance')->nullable();
            $table->string('allowance')->nullable();
            $table->string('salary');
            $table->string('total_salary');
            $table->string('leaving_date')->nullable();
            $table->string('leaving_reason')->nullable();
            $table->string('job_status')->default('Active');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
