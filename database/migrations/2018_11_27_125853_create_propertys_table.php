<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('propertyproject_id')->nullable();
            $table->string('owner_name');
            $table->string('mouza')->nullable();
            $table->string('acr')->nullable();
            $table->string('kanal')->nullable();
            $table->string('marla')->nullable();
            $table->string('yard')->nullable();
            $table->string('total_marlas');
            $table->string('rate_per_acr')->nullable();
            $table->string('rate_per_marla');
            $table->string('total_amount');
            $table->string('point_entry')->nullable();
            $table->string('exemption_rate')->nullable();
            $table->string('no_of_files')->nullable();
            $table->string('proprety_commission')->nullable();
            $table->string('purchasing_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propertys');
    }
}
