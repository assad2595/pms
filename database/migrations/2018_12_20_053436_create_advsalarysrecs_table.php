<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvsalarysrecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advsalarysrecs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->string('loan_amount');
            $table->string('total_installments');
            $table->string('monthly_installment');
            $table->string('balance');
            $table->string('loan_month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advsalarysrecs');
    }
}
