<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpSalarySlipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_salary_slips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->string('salary_month');
            $table->string('rem_balance');
            $table->string('name');
            $table->string('installment_received');
            $table->string('salary_dispatched');
            $table->string('rem_loan_balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_salary_slips');
    }
}
