-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 09, 2019 at 12:58 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `propertyv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `advsalarysrecs`
--

DROP TABLE IF EXISTS `advsalarysrecs`;
CREATE TABLE IF NOT EXISTS `advsalarysrecs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `loan_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_installments` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monthly_installment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advsalarysrecs`
--

INSERT INTO `advsalarysrecs` (`id`, `emp_id`, `loan_amount`, `total_installments`, `monthly_installment`, `balance`, `loan_month`, `created_at`, `updated_at`) VALUES
(1, 1, '25000', '5', '5000', '25000', '2019-01-01', '2019-01-07 12:41:27', '2019-01-07 12:41:27'),
(2, 4, '20000', '5', '4000', '20000', '2019-01-02', '2019-01-07 12:42:06', '2019-01-07 12:42:06');

-- --------------------------------------------------------

--
-- Table structure for table `bank_transactions`
--

DROP TABLE IF EXISTS `bank_transactions`;
CREATE TABLE IF NOT EXISTS `bank_transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bank_account_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_descp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `w_h_tax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_charges` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank_transactions`
--

INSERT INTO `bank_transactions` (`id`, `bank_account_id`, `trans_amount`, `trans_date`, `trans_type`, `payment_method`, `trans_descp`, `w_h_tax`, `bank_charges`, `cheque_no`, `created_at`, `updated_at`) VALUES
(27, '1', '4500000', '2019-01-05', 'Credit', 'Cash', 'sale of land done', NULL, '4600', NULL, '2019-01-05 01:07:02', '2019-01-05 01:07:02'),
(28, '2', '80000', '2019-01-03', 'Debit', 'Cash', 'land purchase', '2000', NULL, NULL, '2019-01-05 02:43:40', '2019-01-05 02:43:40'),
(31, '1', '4500000', '2019-01-05', 'Debit', 'Cash', 'sale of land done', NULL, '4600', NULL, '2019-01-05 06:58:58', '2019-01-05 06:58:58'),
(35, '4', '400000', '2019-01-12', 'Credit', 'Cash', 'added amount', NULL, '4600', NULL, '2019-01-07 00:27:03', '2019-01-07 00:27:03'),
(34, '3', '800000', '2019-01-10', 'Debit', 'Cash', 'debit earinings', '2000', NULL, NULL, '2019-01-07 00:19:47', '2019-01-07 00:19:47'),
(36, '2', '80000', '2019-01-03', 'Debit', 'Cash', 'debit the amount', '7500', NULL, NULL, '2019-01-07 00:27:50', '2019-01-07 00:27:50'),
(37, '3', '900000', '2019-01-04', 'Debit', 'Cash', 'Bank requirement', '9500', NULL, NULL, '2019-01-07 00:28:34', '2019-01-07 00:28:34'),
(38, '4', '2500000', '2019-01-02', 'Debit', 'Cash', 'for purchasing plots', '10500', NULL, NULL, '2019-01-07 00:29:16', '2019-01-07 00:29:16'),
(41, '1', '400000', '2016-01-06', 'Credit', 'Cash', 'this is for the purchasing', NULL, '2000', NULL, '2019-01-07 10:50:37', '2019-01-07 10:50:37'),
(40, '3', '200000', '2018-12-14', 'Credit', 'Cash', 'this is', NULL, '4500', NULL, '2019-01-07 00:30:28', '2019-01-07 00:30:28'),
(42, '1', '100000', '2018-06-04', 'Credit', 'Cash', 'this is', NULL, '2000', NULL, '2019-01-07 10:51:20', '2019-01-07 10:51:20'),
(43, '3', '800000', '2018-12-13', 'Debit', 'Cash', 'yhi dis', '7500', NULL, NULL, '2019-01-07 10:52:10', '2019-01-07 10:52:10'),
(44, '2', '540000', '2018-12-05', 'Credit', 'Cash', 'this is', NULL, '4500', NULL, '2019-01-07 10:53:16', '2019-01-07 10:53:16'),
(45, '4', '90000', '2019-01-04', 'Debit', 'Cash', 'yhidis', '0', NULL, NULL, '2019-01-07 10:54:06', '2019-01-07 10:54:06'),
(46, '1', '670000', '2018-11-13', 'Debit', 'Cash', 'debited', '3500', NULL, NULL, '2019-01-07 10:55:01', '2019-01-07 10:55:01'),
(47, '3', '240000', '2018-12-23', 'Debit', 'Cash', 'this is', '1500', NULL, NULL, '2019-01-07 10:55:37', '2019-01-07 10:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `joining_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lunch_allowance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allowance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_salary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leaving_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leaving_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `father_name`, `cnic`, `dob`, `gender`, `designation`, `joining_date`, `address`, `phone_no`, `email`, `lunch_allowance`, `allowance`, `salary`, `total_salary`, `leaving_date`, `leaving_reason`, `job_status`, `created_at`, `updated_at`) VALUES
(1, 'Ahmad', 'Haroon-ur-Rashids', '35201-2221535-9', '2018-12-14', 'Male', 'Dev-2', '2018-11-29', 'ad', '0332-9987986', 'asimmalik@notitia.tech', '2000', '3000', '35000', '40000', NULL, NULL, 'Active', '2018-12-28 00:55:36', '2018-12-28 00:55:36'),
(3, 'Attaullah', 'Tariq Mehmood', '23669-5656569-9', '2018-12-13', 'Male', 'Dev-2', '2018-12-08', 'abc', '0333333333', 'mairabruner@gmail.com', '5000', '1000', '7000', '13000', NULL, NULL, 'Active', '2018-12-29 03:28:02', '2019-01-01 03:10:42'),
(4, 'Assad Yaqoob', 'New One', '44866-8454986-8', '2019-01-12', 'Male', 'SMD', '2019-01-28', 'this is', '0302-7819136', 'assad2595@gmail.com', '2000', '1000', '11000', '14000', NULL, NULL, 'Active', '2019-01-01 03:09:55', '2019-01-01 03:09:55'),
(5, 'Asim', 'Mukhtar', '23669-5656569-9', '2019-01-18', 'Male', 'SMD', '2019-01-12', 'this', '0333-6658622', 'asimmalik@notitia.tech', '2500', '4000', '35500', '42000', NULL, NULL, 'Active', '2019-01-01 06:24:22', '2019-01-01 06:24:22'),
(6, 'Arsalan', 'Akhbar', '3740502600623', '2019-01-02', 'Male', 'DO', '2019-01-09', 'Amil Coloniy', '03139133135', 'moazateeq@gmail.com', '6000', '0', '10000', '16000', NULL, NULL, 'Active', '2019-01-09 02:50:59', '2019-01-09 02:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `emp_salary_slips`
--

DROP TABLE IF EXISTS `emp_salary_slips`;
CREATE TABLE IF NOT EXISTS `emp_salary_slips` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary_month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `installment_received` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary_dispatched` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_loan_balance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emp_salary_slips`
--

INSERT INTO `emp_salary_slips` (`id`, `emp_id`, `name`, `salary_month`, `installment_received`, `salary_dispatched`, `rem_loan_balance`, `created_at`, `updated_at`) VALUES
(161, 1, 'Ahmad', '2019-01', '5000', '35000', '20000', '2019-01-08 06:36:24', '2019-01-08 06:36:24'),
(162, 4, 'Assad Yaqoob', '2019-01', '4000', '10000', '16000', '2019-01-08 06:36:35', '2019-01-08 06:36:35'),
(163, 1, 'Ahmad', '2019-01', '5000', '35000', '15000', '2019-01-08 07:06:05', '2019-01-08 07:06:05'),
(164, 4, 'Assad Yaqoob', '2018-12', '4000', '10000', '12000', '2019-01-08 13:22:00', '2019-01-08 13:22:00'),
(165, 3, 'Attaullah', '2019-01', 'N/A', '13000', 'N/A', '2019-01-08 13:46:53', '2019-01-08 13:46:53'),
(169, 4, 'Assad Yaqoob', '2019-01', '4000', '10000', '8000', '2019-01-08 14:20:15', '2019-01-08 14:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `p_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_co` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_mobilenumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_contents` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_payment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_co` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_mobilenumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_contents` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_payment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_of_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landpur_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `p_name`, `p_co`, `p_mobilenumber`, `p_price`, `p_date`, `p_contents`, `p_payment_status`, `s_name`, `s_co`, `s_mobilenumber`, `s_price`, `s_date`, `s_contents`, `s_payment_status`, `file_status`, `cat_of_file`, `type_of_file`, `profit`, `landpur_no`, `created_at`, `updated_at`) VALUES
(5, 'Asim', 'Moaz', '03121464', '200000', '2018-12-25', 'Files', NULL, 'yasir khan', 'Mahd', '0321456', '100000', '2018-12-25', 'pdfs', NULL, 'Sold', 'Land', 'Affidavit', '-100000', '21456', '2018-12-26 06:57:42', '2018-12-26 09:02:18'),
(4, 'Asim Malik', 'Ali', '033212156', '600000', '2018-12-18', 'Files', NULL, 'Awais Akhter', 'Waleed', '031245646', '500000', '2018-12-07', 'Files', NULL, 'Sold', 'Coy Fin', 'Affidavit', '-100000', '321264', '2018-12-26 06:37:51', '2018-12-26 06:40:38'),
(3, 'Assad Yaqoob', 'Asim', '03124568798', '100000', '2018-12-18', 'files for dha islamabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 'Land', 'Affidavit', NULL, '123456', '2018-12-26 05:11:51', '2018-12-26 05:11:51'),
(6, 'Arasalan', 'Naz', '0312456', '10000', '2018-12-18', 'erqar', NULL, 'Ali', 'Arsalan', '03133539174', '15000', '2018-12-31', 'sfgstr', NULL, 'Sold', 'Coy Fin', 'Affidavit', '5000', '1123232', '2018-12-26 06:59:35', '2019-01-03 02:33:13'),
(7, 'Naz', 'Ali', '03121234567', '25000', '2018-12-18', 'files', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 'Coy Fin', 'Aloc', NULL, '22332213', '2018-12-26 07:02:35', '2019-01-05 00:34:10'),
(15, 'Assad Yaqoob', 'Mahd', '03331234561', '10000', '2018-12-10', 'fil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 'Land', 'Aloc', NULL, '2233', '2018-12-26 12:12:07', '2019-01-03 02:31:17'),
(16, 'Moaz Ateeq', 'khan sb', '03139133135', '450000', '2018-12-28', 'File purchased for cornor plot', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 'Land', 'Affidavit', NULL, '1245', '2018-12-29 06:48:32', '2018-12-29 06:48:32'),
(17, 'gfgd', 'frtg', 'tret', '12540', '2018-12-19', 'gfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 'Land', 'Affidavit', NULL, 'tre', '2018-12-31 04:28:47', '2018-12-31 04:28:47'),
(19, 'fsdf', 'fsdf', '03139133135', '450.65', '2019-01-03', 'fsdfgds  sfswf fsw fwrnwl fwef wfrw e', NULL, 'Moaz', NULL, '03139133135', '500', '2019-01-09', '//', NULL, 'Sold', 'Land', 'Affidavit', '49.35000000000002', '235', '2019-01-03 08:06:56', '2019-01-09 03:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `file_sale_transactions`
--

DROP TABLE IF EXISTS `file_sale_transactions`;
CREATE TABLE IF NOT EXISTS `file_sale_transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_amount_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_amount_balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_mode_of_receipt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_rvno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_rvdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `file_sale_transactions`
--

INSERT INTO `file_sale_transactions` (`id`, `file_id`, `s_amount_paid`, `s_amount_balance`, `s_mode_of_receipt`, `s_rvno`, `s_rvdate`, `cheque_no`, `s_bank`, `created_at`, `updated_at`) VALUES
(1, '4', '500000', '0', 'Cash', '225566', '2018-12-25', '', NULL, '2018-12-26 06:41:10', '2018-12-26 06:41:10'),
(2, '5', '100000', '0', 'Bank', '225566', '2018-12-25', '', 'Summit Bank', '2018-12-26 08:23:08', '2018-12-26 08:23:08'),
(3, '5', '100000', '0', 'Bank', '225566', NULL, '', 'Summit Bank', '2018-12-26 08:24:12', '2018-12-26 08:24:12'),
(4, '6', '10000', '5000.00', 'Cash', '123', '2018-10-02', NULL, 'HBL', '2019-01-05 00:57:06', '2019-01-05 00:57:06'),
(5, '6', '10000', '5000.00', 'Cash', '123', '2018-10-02', NULL, 'HBL', '2019-01-05 00:57:36', '2019-01-05 00:57:36'),
(6, '6', '10000', '5000.00', 'Cash', '123', '2018-10-02', NULL, 'HBL', '2019-01-05 00:57:42', '2019-01-05 00:57:42'),
(7, '6', '10000', '5000.00', 'Cash', '123', '2018-10-02', NULL, 'HBL', '2019-01-05 00:58:23', '2019-01-05 00:58:23'),
(8, '6', '10000', '5000.00', 'Cash', '123', '2018-10-02', NULL, 'HBL', '2019-01-05 00:58:30', '2019-01-05 00:58:30'),
(9, '6', '2000', '3000.00', 'Cash', NULL, '2018-01-05', NULL, 'ABL', '2019-01-05 00:59:55', '2019-01-05 00:59:55'),
(10, '6', '2000', '1000.00', 'Cash', NULL, '2019-01-07', NULL, NULL, '2019-01-07 06:02:10', '2019-01-07 06:02:10'),
(11, '19', '500', '0.00', 'Cash', NULL, '2019-01-09', NULL, NULL, '2019-01-09 03:07:50', '2019-01-09 03:07:50');

-- --------------------------------------------------------

--
-- Table structure for table `file_transactions`
--

DROP TABLE IF EXISTS `file_transactions`;
CREATE TABLE IF NOT EXISTS `file_transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_amount_received` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_amount_balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_mode_of_receipt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_rvno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_rvdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `file_transactions`
--

INSERT INTO `file_transactions` (`id`, `file_id`, `p_amount_received`, `p_amount_balance`, `p_mode_of_receipt`, `p_rvno`, `p_rvdate`, `cheque_no`, `p_bank`, `created_at`, `updated_at`) VALUES
(11, '5', '10000', '30000', 'Bank', '7414258', '2018-12-25', NULL, 'Summit Bank', '2018-12-26 08:25:04', '2018-12-26 08:25:04'),
(2, '4', '100000', '500000', 'Cash', '123232', '2018-12-18', NULL, NULL, '2018-12-26 06:38:32', '2018-12-26 06:38:32'),
(3, '4', '500000', '0', 'Bank', '7414258', '2018-12-25', NULL, 'Hbl', '2018-12-26 06:39:03', '2018-12-26 06:39:03'),
(4, '3', '50000', '50000', 'Cash', '7414258', '2018-12-25', NULL, NULL, '2018-12-26 06:55:40', '2018-12-26 06:55:40'),
(5, '3', '50000', '0', 'Bank', '321', '2018-12-25', NULL, NULL, '2018-12-26 06:56:06', '2018-12-26 06:56:06'),
(6, '6', '5000', '5000', 'Bank', '321', '2018-12-26', NULL, 'Hbl', '2018-12-26 07:00:44', '2018-12-26 07:00:44'),
(7, '6', '5000', '0', 'Bank', '321', '2018-12-25', NULL, 'Summit Bank', '2018-12-26 07:01:32', '2018-12-26 07:01:32'),
(8, '5', '100000', '100000', 'Cash', '321', '2018-12-25', NULL, NULL, '2018-12-26 07:24:30', '2018-12-26 07:24:30'),
(9, '5', '50000', '50000', 'Bank', '7414258', '2018-12-25', NULL, 'Hbl', '2018-12-26 07:24:49', '2018-12-26 07:24:49'),
(10, '5', '10000', '40000', 'Cash', '7414258', '2018-12-25', NULL, 'Summit Bank', '2018-12-26 07:46:37', '2018-12-26 07:46:37'),
(12, '15', '9000', '1000', 'Bank', '7414258', '2018-12-25', NULL, 'Hbl', '2018-12-26 12:13:27', '2018-12-26 12:13:27'),
(13, '19', '350', '100.64999999999998', 'Cash', '456', '2019-01-03', 'ac588698', 'hbl', '2019-01-03 08:07:40', '2019-01-03 08:07:40'),
(14, '15', '1000', '0.00', 'Cash', '526', '2019-01-16', NULL, NULL, '2019-01-05 00:55:54', '2019-01-05 00:55:54');

-- --------------------------------------------------------

--
-- Table structure for table `lenders`
--

DROP TABLE IF EXISTS `lenders`;
CREATE TABLE IF NOT EXISTS `lenders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lender_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lender_business_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lender_contactno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lender_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lenders`
--

INSERT INTO `lenders` (`id`, `lender_name`, `lender_business_name`, `lender_contactno`, `lender_address`, `created_at`, `updated_at`) VALUES
(4, 'Rahmah', 'Al-Rahmah', '03131234567', 'Multan Road', '2019-01-04 02:49:56', '2019-01-04 06:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(21, '2018_12_17_190820_create_file_sale_transactions_table', 3),
(26, '2018_11_27_125853_create_propertys_table', 7),
(34, '2019_01_04_052217_create_bank_transactions_table', 11),
(7, '2018_11_29_060657_create_propertyprojects_table', 1),
(20, '2018_11_30_181721_create_file_transactions_table', 3),
(33, '2018_12_20_053436_create_advsalarysrecs_table', 11),
(37, '2018_12_13_081711_create_officebanks_table', 14),
(35, '2018_12_13_083722_create_propertytransactions_table', 12),
(19, '2018_11_24_233743_create_files_table', 3),
(23, '2018_12_22_180902_create_office_expenditures_table', 5),
(22, '2018_12_23_004636_create_office_deposits_table', 4),
(32, '2018_12_13_043409_create_emp_salary_slips_table', 11),
(27, '2018_12_26_103145_create_lenders_table', 8),
(29, '2019_01_02_114458_create_propertyexpenses_table', 10),
(31, '2018_11_23_070058_create_employees_table', 11),
(38, '2019_01_08_131209_create_propertyfiles_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `officebanks`
--

DROP TABLE IF EXISTS `officebanks`;
CREATE TABLE IF NOT EXISTS `officebanks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `starting_balance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `officebanks`
--

INSERT INTO `officebanks` (`id`, `account_title`, `account_no`, `starting_balance`, `bank_name`, `branch_name`, `branch_code`, `branch_address`, `created_at`, `updated_at`) VALUES
(1, 'Asim', '1566466566564', '1200000', 'HBL', 'Bank Road', '0132', 'Saddar', '2019-01-03 00:54:00', '2019-01-03 00:54:00'),
(2, 'Assad', '4565005666562', '300000', 'ABL', 'Saddar', '4121', 'Rawalpindi', '2019-01-03 07:18:02', '2019-01-03 07:18:02'),
(3, 'Test', '45259866555656', '5600000', 'Allied Bank', 'Phase-7', '5892', 'Rawalpindi', '2019-01-07 00:13:50', '2019-01-07 00:13:50'),
(4, 'TestTwo', '565122230003', '3000000', 'Askari Bank', 'Phase-8', '0213', 'Rawalpindi', '2019-01-07 00:26:01', '2019-01-07 00:26:01'),
(5, 'Notitia Tech', '2564789922000', '2000000', 'MCB', 'Phase-7', '0256', 'Rawalpindi', '2019-01-08 03:30:08', '2019-01-08 03:30:08');

-- --------------------------------------------------------

--
-- Table structure for table `office_deposits`
--

DROP TABLE IF EXISTS `office_deposits`;
CREATE TABLE IF NOT EXISTS `office_deposits` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deposited` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `office_deposits`
--

INSERT INTO `office_deposits` (`id`, `deposited`, `date`, `created_at`, `updated_at`) VALUES
(1, '2000', '2018-12-25', '2018-12-26 10:40:32', '2018-12-26 10:40:32'),
(2, '2000', '2018-12-25', '2018-12-26 10:41:04', '2018-12-26 10:41:04'),
(3, '2000', '2018-12-25', '2018-12-26 10:41:41', '2018-12-26 10:41:41'),
(4, '2000', '2018-12-25', '2018-12-26 10:42:47', '2018-12-26 10:42:47'),
(9, '1000', '2018-12-26', '2018-12-27 02:38:08', '2018-12-27 02:38:08'),
(6, '500', '2018-12-25', '2018-12-26 11:10:39', '2018-12-26 11:10:39'),
(7, '1000', '2018-12-25', '2018-12-26 11:46:06', '2018-12-26 11:46:06'),
(8, '2000', '2018-12-25', '2018-12-26 12:33:47', '2018-12-26 12:33:47'),
(10, '5000', '2018-12-31', '2018-12-31 05:37:51', '2018-12-31 05:37:51');

-- --------------------------------------------------------

--
-- Table structure for table `office_expenditures`
--

DROP TABLE IF EXISTS `office_expenditures`;
CREATE TABLE IF NOT EXISTS `office_expenditures` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remaining_balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `office_expenditures`
--

INSERT INTO `office_expenditures` (`id`, `item_name`, `description`, `amount`, `date`, `remaining_balance`, `created_at`, `updated_at`) VALUES
(4, NULL, NULL, NULL, NULL, '1000', '2018-12-26 11:46:07', '2018-12-26 11:46:07'),
(3, NULL, NULL, NULL, NULL, '500', '2018-12-26 11:10:39', '2018-12-26 11:10:39'),
(6, 'TeaBill', 'files', '200', '2018-12-25', '300', '2018-12-26 12:32:48', '2018-12-26 12:32:48'),
(7, NULL, NULL, NULL, NULL, '2000', '2018-12-26 12:33:47', '2018-12-26 12:33:47'),
(8, 'Photos', 'files', '900', '2018-12-25', '1100', '2018-12-26 12:34:51', '2018-12-26 12:34:51'),
(9, 'Photos', 'aslam', '2000', '2018-12-26', '5000', '2018-12-27 02:11:22', '2019-01-03 08:05:16'),
(10, NULL, NULL, NULL, NULL, '1000', '2018-12-27 02:38:08', '2018-12-27 02:38:08'),
(11, NULL, NULL, NULL, NULL, '6000', '2018-12-31 05:37:51', '2018-12-31 05:37:51'),
(12, NULL, NULL, NULL, NULL, '5000', '2019-01-03 08:05:16', '2019-01-03 08:05:16'),
(13, 'Tea', 'office tea', '2000', '2019-01-07', '3000.00', '2019-01-07 04:38:54', '2019-01-07 04:38:54'),
(14, 'tea', 'tea', '1500', '2019-01-09', '1500.00', '2019-01-09 03:11:50', '2019-01-09 03:11:50');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `propertyexpenses`
--

DROP TABLE IF EXISTS `propertyexpenses`;
CREATE TABLE IF NOT EXISTS `propertyexpenses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `property_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expenses_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expenses_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mode_of_payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `propertyexpenses`
--

INSERT INTO `propertyexpenses` (`id`, `property_id`, `expenses_name`, `expenses_amount`, `mode_of_payment`, `cheque_no`, `bank_name`, `pv_no`, `payment_date`, `created_at`, `updated_at`) VALUES
(5, '38', 'Comparsion', '25', 'Cheque', NULL, 'BoP', NULL, '2018-10-05', '2019-01-08 06:38:16', '2019-01-08 06:38:16'),
(2, '38', '3% DC Rate Stamp paper', '121679', 'Cheque', NULL, 'BoP', NULL, '2018-10-05', '2019-01-04 06:09:11', '2019-01-04 06:09:11'),
(3, '38', 'Agreement', '5256', 'Cheque', NULL, 'BoP', NULL, '2018-10-05', '2019-01-05 00:52:03', '2019-01-05 00:52:03'),
(4, '38', 'Registration', '1000', 'Cheque', NULL, 'BoP', NULL, '2018-10-05', '2019-01-05 00:53:19', '2019-01-05 00:53:19'),
(6, '38', 'Payment of commission', '57293', 'Cheque', NULL, 'Al-Habib', NULL, '2018-10-16', '2019-01-08 06:40:24', '2019-01-08 06:40:24'),
(7, '38', 'Advance Tax 2%', '81113', 'Cash', NULL, 'AKBL', NULL, '2018-10-16', '2019-01-08 06:43:44', '2019-01-08 06:43:44'),
(8, '38', 'Challan of advance tax', '50', 'Cash', NULL, 'AKBL', NULL, '2018-10-16', '2019-01-08 07:06:47', '2019-01-08 07:06:47'),
(9, '38', '1% TMA (DCF)', '40557', 'Cash', NULL, 'AKBL', NULL, '2018-10-16', '2019-01-08 07:07:39', '2019-01-08 07:07:39'),
(10, '38', 'TMA Clerk', '1000', 'Cash', NULL, 'AKBL', NULL, '2018-10-16', '2019-01-08 07:08:21', '2019-01-08 07:08:21'),
(11, '38', 'Ahl-e-Commission Clerk', '3000', 'Cash', NULL, 'AKBL', NULL, '2018-10-16', '2019-01-08 07:09:24', '2019-01-08 07:09:24'),
(12, '54', '3% DC Rate Stamp paper', '45000', 'Cash', NULL, NULL, NULL, '2019-01-09', '2019-01-09 03:06:01', '2019-01-09 03:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `propertyfiles`
--

DROP TABLE IF EXISTS `propertyfiles`;
CREATE TABLE IF NOT EXISTS `propertyfiles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `property_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaser_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_files` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_sale_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_profit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remaining_files` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `propertyfiles`
--

INSERT INTO `propertyfiles` (`id`, `property_id`, `purchaser_name`, `no_of_files`, `file_price`, `sale_price`, `total_sale_price`, `file_profit`, `remaining_files`, `created_at`, `updated_at`) VALUES
(5, '38', 'moaz', '1', '4194686.11', '4204686', '4204686', '9999.89', '0.44', '2019-01-09 07:36:04', '2019-01-09 07:36:04');

-- --------------------------------------------------------

--
-- Table structure for table `propertyprojects`
--

DROP TABLE IF EXISTS `propertyprojects`;
CREATE TABLE IF NOT EXISTS `propertyprojects` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `propertyprojects`
--

INSERT INTO `propertyprojects` (`id`, `project_name`, `project_city`, `created_at`, `updated_at`) VALUES
(1, 'DHA', 'Multan', '2018-12-25 09:47:21', '2018-12-25 09:47:21'),
(2, 'DHA', 'Bahawalpur', '2019-01-01 04:17:06', '2019-01-01 04:17:06');

-- --------------------------------------------------------

--
-- Table structure for table `propertys`
--

DROP TABLE IF EXISTS `propertys`;
CREATE TABLE IF NOT EXISTS `propertys` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `propertyproject_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mouza` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kanal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marla` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yard` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_marlas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate_per_acr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate_per_marla` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `point_entry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exemption_rate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_files` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proprety_commission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchasing_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `propertys`
--

INSERT INTO `propertys` (`id`, `propertyproject_id`, `owner_name`, `mouza`, `acr`, `kanal`, `marla`, `yard`, `total_marlas`, `rate_per_acr`, `rate_per_marla`, `total_amount`, `point_entry`, `exemption_rate`, `no_of_files`, `proprety_commission`, `purchasing_date`, `created_at`, `updated_at`) VALUES
(38, '1', 'Nasim Mai', 'Basti Nau Dhand', NULL, '5', '3', NULL, '103', '8900000', '55625', '5729375', '0.64', '2.25', '1.44', NULL, '2019-01-03', '2019-01-04 02:52:41', '2019-01-04 05:53:31');

-- --------------------------------------------------------

--
-- Table structure for table `propertytransactions`
--

DROP TABLE IF EXISTS `propertytransactions`;
CREATE TABLE IF NOT EXISTS `propertytransactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `property_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lender_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymenter_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance_payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mode_of_payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `propertytransactions`
--

INSERT INTO `propertytransactions` (`id`, `property_id`, `lender_id`, `paymenter_name`, `payment_description`, `payment_amount`, `balance_payment`, `mode_of_payment`, `cheque_no`, `bank_name`, `pv_no`, `payment_status`, `payment_date`, `created_at`, `updated_at`) VALUES
(1, '38', '4', NULL, 'Token money paid by Al-Rahmah', '500000', '5229375', 'Cash', NULL, 'AKBL', 'P-18', 'Paid', '2018-08-17', '2019-01-07 05:48:49', '2019-01-09 05:13:01'),
(2, '38', NULL, 'Imtiaz Khan', 'Token Money paid by Imtiaz Khan', '500000', '4729375', 'Cheque', NULL, 'AKBL', NULL, NULL, '2018-08-09', '2019-01-07 05:50:29', '2019-01-07 05:50:29'),
(3, '38', NULL, 'Imtiaz Khan', '//', '74000', '4655375', 'Cash', NULL, NULL, NULL, NULL, '2018-08-09', '2019-01-07 06:21:03', '2019-01-07 06:21:03'),
(5, '38', NULL, 'Imtiaz Khan', '//', '500000', '4155375', 'Cash', NULL, NULL, NULL, NULL, '2018-08-09', '2019-01-07 07:40:19', '2019-01-07 07:40:19'),
(6, '38', '4', NULL, 'Paid by Al-Rahmah to LO', '4040000', '115375', 'Cash', NULL, 'Al-Habib', NULL, NULL, '2018-08-09', '2019-01-07 07:46:23', '2019-01-07 07:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'aaaa', 'assad2595@gmail.com', NULL, '$2y$10$vSqT4a9/GTCYnIUg0Ou6x.pDTlJs1wh1nYTk8GgeW51uu38oeC/8e', 'vMSf1bNAMb9hr7lpmp8EukkAxA0kOCX2IwqnOOm2KjPeEfbtuYU3avJPZMag', '2018-12-23 03:35:28', '2018-12-23 03:35:28'),
(2, 'Moaz Ateeq', 'moazateeq@gmail.com', NULL, '$2y$10$wyXFlvmvoZ.EujDbQEjYHeuIk5qQhwq/cyjEo/Hnwgpb1iKaUGXW6', 'TMSupmpLg8lNANWYq1t6gjiih4OEOknDCCbNa719Rg2GZvaDbhreCagD9Fcg', '2018-12-26 03:17:17', '2018-12-26 03:17:17');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
