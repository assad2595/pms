<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<h4 class="page-title">List of Total Employees</h4>
					<div class="card">
						<div class="card-header">
							<a href="{{ route('employees.create') }}" class="btn btn-primary  text-left mt-3 mb-3"><i class="la la-plus"></i> Add Employee</a>
							</div>
							<div class="card-body">
								<table class="table table-striped mt-3 datatablejs">
									<thead>
										<tr>
											<th scope="col">S.No</th>
											<th scope="col">Name</th>
											<th scope="col">Designation</th>
											<th scope="col">Total Salary</th>
											<th scope="col">Job Status</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($employees as $e)
										<tr>
											<td>{{$loop->iteration}}</td>
											<td>{{ $e->name }}</td>
											<td>{{ $e->designation }}</td>
											<td>{{ $e->total_salary }}</td>
											<td>
												@if($e->job_status == "Active")
												<span class="badge badge-primary">{{ $e->job_status }}</span>
												@else
												<span class="badge badge-warning">{{ $e->job_status }}</span>
												@endif
											</td>
											<td>
											<a href="{{ route('employees.show',$e->id) }}" class="btn btn-success btn-xs">View</a>	
											<a href="{{ route('employees.edit',$e->id) }}" class="btn btn-info btn-xs">Edit</a>	
											<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
											<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('employees.destroy',$e->id) }}" method="post">
											@method('DELETE')
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											</form>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>					
							</div>
						</div>
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>