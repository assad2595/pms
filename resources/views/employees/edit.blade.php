<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Update Employee Information</div></div>
							<div class="card-body">
								<form id="formvalidation" method="post" action="{{ route('employees.update',$employee->id) }}">
									@method('PUT')
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row">
										<div class="form-group col-md-4">	
											<label for="name">Name</label>							
											<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="name" placeholder="Name"  value="{{ $employee->name }}">		
										</div>
										<div class="form-group col-md-4">
											<label for="father name">Father Name</label>
											<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="father_name" placeholder="Father Name"  value="{{ $employee->father_name }}">
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="cnic"> CNIC</label>								
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[13],maxSize[13]] form-control" name="cnic" placeholder=" 11111-1234567-8"value="{{ $employee->cnic }}">
										</div>
										<div class="form-group col-md-4 form-check">
											<label for="dob"> DOB</label>
											<input type="date" class="validate[required] form-control" name="dob"  value="{{ $employee->dob }}">
										</div>
										<div class="form-group col-md-4">
											<label for="gender"> Gender</label><br/>		
											<label class="form-radio-label">
												<input class="form-radio-input" type="radio" name="gender" value="Male" {{ $employee->gender == 'Male' ? 'checked' : '' }} >
												<span class="form-radio-sign">Male</span>
											</label>
											<label class="form-radio-label ml-3">
												<input class="form-radio-input" type="radio" name="gender" value="FeMale" {{ $employee->gender == 'Female' ? 'checked' : '' }}  >
												<span class="form-radio-sign">Female</span>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="address"> Address</label>
											<textarea class="validate[required] form-control" name="address" rows="2"  placeholder="Enter Address Details" style="resize: none;" value="address">{{ $employee->address }}</textarea>	
										</div>
										<div class="form-group col-md-3">
											<label for="email"> Email</label>							
											<input type="email" class="validate[custom[email]] form-control" name="email" placeholder="Email" value="{{ $employee->email }}">		
										</div>
										<div class="form-group col-md-3">
											<label for="phone_no"> Mobile No.</label>
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="phone_no" placeholder="Mobile No" value="{{ $employee->phone_no }}">
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">	
											<label for="designation"> Designation</label>	
											<input type="text" class="validate[required] form-control" name="designation" placeholder="Designation"  value="{{ $employee->designation }}">		
										</div>	
										<div class="form-group col-md-4">
											<label for="joining_date"> Joining Date</label>
											<input type="date" class="validate[required] form-control" name="joining_date"  value="{{ $employee->joining_date }}">
										</div>			
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="lunch_allowance">Lunch Allowance</label>
											<input type="text" class="validate[required, custom[number],minSize[1]] form-control" id="lunch_allowance" name="lunch_allowance" placeholder="Lunch Allowance"  value="{{ $employee->lunch_allowance }}" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="allowance">Allowance</label>
											<input type="text" class="validate[required, custom[number],minSize[1]] form-control" id="allowance" name="allowance" placeholder="Allowance" value="{{ $employee->allowance }}" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="salary"> Salary</label>
											<input type="text" class="validate[required, custom[number],minSize[1]] form-control" id="salary" name="salary" placeholder="Salary" value="{{ $employee->salary }}" autocomplete="off">
										</div>								
									</div>
									<input type="hidden" id="total_salary" name="total_salary" value="{{ $employee->total_salary }}"> 
									<!-- Setting employee job status at update -->
									<input type="hidden" name="job_status" id="check" value="{{ $employee->job_status }}">
									<div class="card-action row justify-content-md-center" >
										<button type="submit" class="btn btn-primary" value="Save">Update</button>
										<button type="button" id="show" class="btn btn-success" style="margin-left: 10px;">Update Job Status</button>
									</div>
									<!--Job leaving entry Start --->
									<div id="jobStatusForm" style="display: none;" class="card">
										<div class="card-hearder">
											<div class="card-title" style="margin-top: 10px;">Update Employee Job Status Information</div>
											<div class="card-body">
												<div class="row">
													<div class="form-group col-md-6">	
														<label for="Leaving Reason"> Leaving Reason</label>	
														<textarea type="text" class="validate[required] form-control" rows="2" name="leaving_reason" id="leaving_reason" style="resize: none;" placeholder="Leaving Reason" value="{{ $employee->leaving_reason }}">{{ $employee->leaving_reason }}</textarea>	
													</div>
													<div class="form-group col-md-4">
														<label for="leaving_date"> Leaving Date</label>
														<input type="date" class="validate[required] form-control" id="leaving_date" name="leaving_date" value="{{ $employee->leaving_date }}">
													</div>
												</div>
												<div class="card-action row justify-content-md-center">
													<button type="button" id="hide" class="btn btn-warning" >Not Now!</button>
												</div>
											</div>
										</div>
									</div>
									<!--Job leaving entry End --->	
								</form>
							</div>		
						</div>
					</div>
					@include('partials.footer')
				</div>
				@include ('partials.js-libraries')
			</div>
		</body>
		</html>