<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Employee Registration Form</div></div>
							<div class="card-body">
								<form id="formvalidation" method="post" action="{{ route('employees.store') }}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row">
										<div class="form-group col-md-4">	
											<label for="name">Name</label>							
											<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="name" placeholder="Name" >		
										</div>
										<div class="form-group col-md-4">
											<label for="father name">Father Name</label>
											<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="father_name" placeholder="Father Name">
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="cnic"> CNIC</label>								
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[13],maxSize[13]] form-control" name="cnic" placeholder="1234567891011">
										</div>
										<div class="form-group col-md-4 form-check">
											<label for="dob"> DOB</label>
											<input type="date" class="validate[required] form-control" name="dob"  autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="gender"> Select Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>		
											<label class="form-radio-label">
												<input class="form-radio-input" type="radio" name="gender" value="Male" checked="">
												<span class="form-radio-sign">Male</span>
											</label>
											<label class="form-radio-label">
												<input class="form-radio-input" type="radio" name="gender" value="Female">
												<span class="form-radio-sign">Female</span>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="address"> Address</label>
											<textarea class="validate[required] form-control" name="address" rows="2"  style="resize: none;" placeholder="Enter Address Details"></textarea>	
										</div>
										<div class="form-group col-md-3">
											<label for="email"> Email</label>							
											<input type="email" class="validate[custom[email]] form-control" name="email" placeholder="Email">		
										</div>
										<div class="form-group col-md-3">
											<label for="phone_no"> Mobile No.</label>
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="phone_no" placeholder="e.g 03001234567">
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">	
											<label for="designation"> Designation</label>	
											<input type="text" class="validate[required]  form-control" name="designation" placeholder="Designation" >		
										</div>	
										<div class="form-group col-md-4">
											<label for="joining_date"> Joining Date</label>
											<input type="date" class="validate[required] form-control" name="joining_date" >
										</div>					
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="lunch_allowance">Lunch Allowance</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="lunch_allowance" name="lunch_allowance" placeholder="Lunch Allowance" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="allowance">Allowance</label>
											<input type="text" id="allowance" class="validate[required, custom[number] form-control" name="allowance" placeholder="Allowance" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="salary"> Salary</label>
											<input type="text" id="salary" class="validate[required, custom[number] form-control" name="salary" placeholder="Salary" autocomplete="off">
										</div>								
									</div>
									<input type="hidden" id="total_salary" name="total_salary">							
									<div class="card-action row justify-content-md-center">
										<div class="col-md-auto">
										<button class="btn btn-primary" value="Save">Submit</button>
										<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
										</div>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>