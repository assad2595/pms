<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Employee Information</div></div>
							<div class="card-body">
								<form method="post" action="{{ route('employees.show',$employee->id) }}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row">
										<div class="form-group col-md-4">	
											<label for="name">Name</label>		
											<input type="text" class="form-control" name="name" placeholder="Name" required="" value="{{ $employee->name }}" disabled>		
										</div>
										<div class="form-group col-md-4">
											<label for="father name">Father Name</label>
											<input type="text" class="form-control" name="father_name" placeholder="Father Name" required="" value="{{ $employee->father_name }}" disabled>
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="cnic"> CNIC</label>								
											<input type="text" class="form-control" name="cnic" placeholder=" 11111-1234567-8" required="" value="{{ $employee->cnic }}" disabled>
										</div>
										<div class="form-group col-md-4 form-check">
											<label for="dob"> DOB</label>
											<input type="text" class="form-control" name="dob" placeholder="Date of Birth" required="" value="{{ $employee->dob }}" disabled>
										</div>
										<div class="form-group col-md-4">
											<label for="gender"> Gender</label><br/>		
											<label class="form-radio-label">
												<input class="form-radio-input" type="radio" name="gender" {{ $employee->gender == 'Male' ? 'checked' : '' }} disabled>
												<span class="form-radio-sign">Male</span>
											</label>
											<label class="form-radio-label ml-3">
												<input class="form-radio-input" type="radio" name="gender" {{ $employee->gender == 'Female' ? 'checked' : '' }}  disabled>
												<span class="form-radio-sign">Female</span>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="address"> Address</label>
											<textarea class="form-control" name="address" rows="2" required="" placeholder="Enter Address Details" style="resize: none;" disabled>{{ $employee->address }}</textarea>	
										</div>
										<div class="form-group col-md-3">
											<label for="email"> Email</label>							
											<input type="email" class="form-control" name="email" placeholder="Email"required="" value="{{ $employee->email }}" disabled>		
										</div>
										<div class="form-group col-md-3">
											<label for="phone_no"> Mobile No.</label>
											<input type="text" class="form-control" name="phone_no" placeholder="Mobile No"required="" value="{{ $employee->phone_no }}" disabled>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-4">	
											<label for="designation"> Designation</label>	
											<input type="text" class="form-control" name="designation" placeholder="Designation" required="" value="{{ $employee->designation }}" disabled>		
										</div>	
										<div class="form-group col-md-4">
											<label for="joining_date"> Joining Date</label>
											<input type="text" class="form-control" name="joining_date" placeholder="Joining Date"required="" value="{{ $employee->joining_date }}" disabled>
										</div>				
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="lunch_allowance">Lunch Allowance</label>
											<input type="text" class="form-control" name="lunch_allowance" placeholder="Lunch Allowance"required="" value="{{ $employee->lunch_allowance }}" disabled>
										</div>
										<div class="form-group col-md-4">
											<label for="allowance">Allowance</label>
											<input type="text" class="form-control" name="allowance" placeholder="Allowance"required="" value="{{ $employee->allowance }}" disabled>
										</div>
										<div class="form-group col-md-4">
											<label for="salary"> Salary</label>
											<input type="text" class="form-control" name="salary" placeholder="Salary"required="" value="{{ $employee->salary }}" disabled>
										</div>								
									</div>						
									<div class="card-action row justify-content-md-center">
										<a href="{{ route('employees.edit',$employee->id) }}" class="btn btn-default">Edit Details</a>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>