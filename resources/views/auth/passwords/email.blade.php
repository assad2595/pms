<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Kashaf-Enterprises</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    @include('partials.head')
</head>
<body>
@include('partials.navbar')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="login100-form-title" style="background-image: url(../assets/img/bg-03.jpeg);">
                <span class="login100-form-title-1">
                    Reset Password
                </span>
            </div>
            <form class="login100-form validate-form" method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="wrap-input100 validate-input m-b-26">
                    <span class="label-input100">Email</span>
                    <input id="email" type="email" class="input100{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-sb-m w-full p-b-30">
                    
                </div>
                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        Send Password Reset Link
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('partials.footer')
@include ('partials.js-libraries')
</body>
</html>
