<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Kashaf-Enterprises</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    @include('partials.head')
</head>
<body>
    @include('partials.navbar')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form-title" style="background-image: url(assets/img/bg-02.jpeg);">
                    <span class="login100-form-title-1">
                        Register Yourself
                    </span>
                </div>

                <form class="login100-form validate-form" method="POST" action="{{ route('register') }}">
                        @csrf
                    <div class="wrap-input100 validate-input m-b-26" data-validate="Valid email is required: ex@abc.xyz">
                        <span class="label-input100">Name</span>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Enter Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-26" data-validate="Valid email is required: ex@abc.xyz">
                        <span class="label-input100">E-mail Address </span>
                       <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter Email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                        <span class="label-input100">Password</span>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter Password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                        <span class="label-input100">Confirm Password</span>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="flex-sb-m w-full p-b-30"></div>
                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

        @include('partials.footer')
        @include ('partials.js-libraries')
</body>
</html>

