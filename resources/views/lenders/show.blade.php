<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Lender Details</div>
						</div>
						<div class="card-body">
							<form id="formvalidation" method="post" action="{{ route('lenders.update', $editlender->id) }}">
								@method('PUT')
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="lender Name">Full Name</label>
										<input type="textbox" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="lender_name" placeholder="Enter lender Name" value="{{ $editlender->lender_name }}">
									</div>
									<div class="form-group col-md-5">
										<label for="Business Name">Business Name</label>
										<input type="textbox" class="validate[required] form-control" name="lender_business_name" placeholder="Enter Business Name" value="{{ $editlender->lender_business_name }}">
									</div>
									<div class="form-group col-md-3">
										<label for="Contact No">Contact No</label>
										<input type="textbox" class="validate[required, custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="lender_contactno" placeholder="Enter Contact No" value="{{ $editlender->lender_contactno }}">
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label for="Business Address">Address</label>
										<textarea class="validate[required] form-control" name="lender_address" cols="10" rows="2">{{ $editlender->lender_address }}</textarea>
									</div>
								</div>
								<div class="card-action row justify-content-md-center">
										<button class="btn btn-primary" value="Save">Update Record</button>
										
										<input type="reset" class="btn btn-danger" value="Cancel">
								</div>
								
							</form>
						</div>
						<!--Table View-->
						<div class="card-header">
							<div class="card-title">Lender Payment Details</div>
						</div>
						<div class="card-body">
							<table class="table table-striped mt-3 datatablejs">
								<thead>
									<tr>
										<th scope="col">S.No</th>
										<th scope="col">Property Project & Owner Name</th>
										<th scope="col">Amount</th>
										<th scope="col">Status</th>
										<th scope="col">Date</th>
										<th scope="col" class="text-center">Actions</th>
									</tr>
								</thead>
								<tbody>
									<!-- propertyproject_id  -->
									@foreach($propertytransactions as $ptransactions)
										<tr>
											<td>{{$loop->iteration}}</td>
											<td>
												@foreach($properties as $property)
													@if( $ptransactions->property_id == $property->id)
														@foreach($propertyprojects as $pp)
															@if($property->propertyproject_id == $pp->id)
																{{$pp->project_name}} {{$pp->project_city}} -
															@endif
														@endforeach
														{{ $property->owner_name }}
													@endif
												@endforeach
												
											</td>
											<td>{{$ptransactions->payment_amount}}</td>
											@if($ptransactions->payment_status)
												<td>{{$ptransactions->payment_status}}</td>
											@else
												<td>UnPaid</td>
											@endif

											<td>{{$ptransactions->payment_date }}</td>

											@if($ptransactions->payment_status == 'Paid')
												<td class="text-center"><input class="btn btn-success btn-xs" value="Payment Clear" readonly=""></td>
											@else	
											<td class="text-center">
												<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-primary btn-xs" >Paid</a>
												<form onsubmit="return confirm('Are You Sure?');" method="post" action="{{ route('propertytransactions.update',$ptransactions->id) }}" >
												@method('PUT')
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												</form>
											</td>
											@endif

										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>