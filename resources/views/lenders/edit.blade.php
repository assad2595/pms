<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Lenders</h4>
						<div class="row">
							
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Lenders List</div>
									</div>
									<div class="card-body">
										<table class="table table-striped mt-3 datatablejs">
											<thead>
												<tr>
													<th scope="col">S.No</th>
													<th scope="col">Lender Name</th>
													<th scope="col">Business Name</th>
													<th scope="col">Contact No</th>
													<th scope="col">Actions</th>
												</tr>
											</thead>
											<tbody>
												@foreach($lender as $lenders)
												<tr>
													<td>{{$loop->iteration}}</td>
													<td>{{ $lenders->lender_name}}</td>
													<td>{{ $lenders->lender_business_name}}</td>
													<td>{{ $lenders->lender_contactno}}</td>
													<td>
														<a href="{{ route('lenders.edit',$lenders->id) }}" class="btn btn-info btn-xs">Edit</a>
														
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Update Lender</div>
									</div>
									<div class="card-body">
										<form id="formvalidation" method="post" action="{{ route('lenders.update', $editlender->id) }}">
											@method('PUT')
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="row">
												<div class="form-group col-md-12">
													<label for="lender Name">Full Name</label>
													<input type="textbox" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="lender_name" placeholder="Enter lender Name" value="{{ $editlender->lender_name }}">
												</div>
												<div class="form-group col-md-12">
													<label for="Business Name">Business Name</label>
													<input type="textbox" class="validate[required] form-control" name="lender_business_name" placeholder="Enter Business Name" value="{{ $editlender->lender_business_name }}">
												</div>
												<div class="form-group col-md-12">
													<label for="Contact No">Contact No</label>
													<input type="textbox" class="validate[required, custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="lender_contactno" placeholder="Enter Contact No" value="{{ $editlender->lender_contactno }}">
												</div>
												<div class="form-group col-md-12">
													<label for="Business Address">Address</label>
													<textarea class="validate[required] form-control" name="lender_address" cols="10" rows="2">{{ $editlender->lender_address }}</textarea>
												</div>
											</div>
											<div class="row">
												<div class="card-action">
													<button class="btn btn-primary" value="Save">Update Record</button>
													
													<input type="reset" class="btn btn-danger" value="Cancel">
												</div>
											</div>
										</form>
									</div>
									
								</div>
							</div>
						</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>