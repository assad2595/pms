<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<!-- <h4 class="page-title">Property File Records</h4> -->
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Add New Expense</div>
								</div>
								<div class="card-body">
								@include('partials.flash-message')
										<form id="formvalidation" method="post" action="{{ route('officeexpenditure.store') }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-3">	
												<label>Total Balance</label>	
												<input type="text" class="form-control" name="" value="{{$last_remaining_balance}}" readonly="" id="t_balance">		
											</div>
											<div class="form-group col-md-3">	
												<label>Remaining Balance</label>	
												<input type="text" class="form-control" name="remaining_balance" readonly="" id="remaining_balance">		
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3">	
												<label for="purchaser_name">Item Name</label>	
												<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="item_name" placeholder="Enter Item Name" >		
											</div>
											<div class="form-group col-md-6">
												<label for="contents">Description</label>
												<textarea type="text" class="validate[required] form-control" name="description" placeholder="Enter Description" ></textarea> 
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3">
												<label for="c_o">Amount</label>
												<input type="text" class="validate[required, custom[number],minSize[1]] form-control" name="amount" placeholder="Enter Item Amount"  id="item_amount">
											</div>
											<div class="form-group col-md-3">
												<label for="date">Date</label>
												<input type="date" class="validate[required] form-control" name="date" placeholder="Enter Date"  autocomplete="off">	
											</div>
										</div>
										
										<div class="row justify-content-md-center">
											<div class="col-md-auto">
												<button class="btn btn-primary" value="Save">Save</button>
												<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</body>
</html>