<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf Enterprices</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Office Monthly Report</h4>
					<div class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Monthly Expenses</div>
								</div>
								<div class="card-body">
									<form id="formvalidation" method="POST" action="checkmonthlyexpenses">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-3"></div>
											<div class="form-group col-md-6">
												<label">Select Month</label>
												<input type="month" class="validate[required] form-control" name="expenses_month"  autocomplete="off">
											</div>
										</div>
										<div class="row">
											<div class="col-md-3"></div>
											<div class="card-action col-md-6">
												<input type="Submit" name="" class="btn btn-success" value="Submit">
												<input type="reset" class="btn btn-danger" value="Cancel">
											</div>
										</div>
									</form>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Monthly Deposit</div>
								</div>
								<div class="card-body">
									<form id="validation" method="POST" action="checkmonthlydeposit">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-3"></div>
											<div class="form-group col-md-6">
												<label for="rv_date">Select Month</label>
												<input type="month" class="validate[required] form-control" name="deposit_date" placeholder="Enter From Deposit Date"  autocomplete="off">
											</div>
										</div>
										<div class="row">
											<div class="col-md-3"></div>
											<div class="card-action">
												<input type="Submit" name="" class="btn btn-success" value="Submit">
												<input type="reset" class="btn btn-danger" value="Cancel">
											</div>
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>
					
					<div class="row justify-content-center">
						@if($monthlyexpenses==null)
						
						@else
						<!-- <div class="col-md-0.5"></div> -->
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Monthly Expenses List</div>
								</div>
								<div class="card-body">
									<div class="row">
										@foreach($monthlyexpenses as $mon_exp)
											@if ($loop->last)
												@php
													$month = date("M-Y",strtotime($mon_exp->date));
												@endphp
												<div class="form-group col-md-0.5"></div>
												<div class="form-group col-md-2.5">
													<input type="text" class="form-control" value=" Month {{ $month }}" readonly="">
												</div>
											@endif
										@endforeach

										@php $total_expense=0;	
										@endphp

										@foreach($monthlyexpenses as $mon_exp)
											<input type="hidden" value="{{ $total_expense += $mon_exp->amount}}">
										@endforeach
										<div class="form-group col-md-3">
											<input type="text" class="form-control" value="Total Expenses={{ $total_expense }}" readonly="">
										</div>
										<div class="form-group col-md-3">
											<input type="text" class="form-control" value="Total Deposit={{ $total_deposit }}" readonly="">
										</div>
										<div class="form-group col-md-2.5">
											<input type="text" class="form-control" value="Remaining Bal={{ $total_deposit - $total_expense}}" readonly="">
										</div>
										<div class="form-group col-md-0.5"></div>
									</div>
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Item Name</th>
												<th>Description</th>
												<th>Date</th>
												<th>Amount</th>
											</tr>
										</thead>
										<tbody>
											<?php $count=1?>
											@foreach($monthlyexpenses as $mon_exp)
											<tr>	
												<td><?php echo $count;?></td>
												<td>{{ $mon_exp->item_name }}</td>
												<td>{{ $mon_exp->description }}</td>
												<td>{{ $mon_exp->date }}</td>
												<td>{{ $mon_exp->amount }}</td>
												<?php $count++?>
											</tr>
											@endforeach
											
										</tbody>
									</table>
								</div>
								@endif
							</div>
						</div>

						@if($monthlydeposit==null)
						@else
						<div class="row justify-content-center">
							<div class="col-md-9">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Monthly Depsoit List</div>
									</div>
									<div class="row">
										<div class="col-md-2"></div>
										@foreach($monthlydeposit as $mon_dep)
										@if ($loop->last)
										@php
										$month = date("M-Y",strtotime($mon_dep->date));
										@endphp
										<div class="col-md-4">
											<input type="text" class="form-control" value="Month {{ $month }}" readonly="">
										</div>
										@endif
										@endforeach

										@php	
										$total_deposit=0;
										@endphp
										@foreach($monthlydeposit as $mon_dep)
										<input type="hidden" value="{{ $total_deposit += $mon_dep->deposited}}">
										@endforeach
										<div class="col-md-4">
											<input type="text" class="form-control" value="Total Deposits = {{ $total_deposit }}" readonly="">
										</div>
									</div>
									<div class="card-body">
										<table class="table table-striped mt-3 datatablejs">
											<thead>
												<tr>
													<th scope="col">S.No</th>
													<th scope="col">Amount</th>
													<th scope="col">Date</th>
												</tr>
											</thead>
											<tbody>
												@foreach($monthlydeposit as $officedeposit)
												<tr>
													<td>{{$loop->iteration}}</td>
													<td>{{$officedeposit->deposited}} 
														<td>{{$officedeposit->date}}</td>
													</tr>
													@endforeach
													@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@include('partials.footer')
				</div>
				@include ('partials.js-libraries')
			</div>
		</body>
		</html>