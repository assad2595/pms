<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="row">
										<div class="card-title col-md-8">Expenses List</div>
										@foreach($officeexpenditure as $oe)	
										@if ($loop->first)
										<div class="col-md-1.5">
											<a class="btn btn-default fload-right">Balance</a>
										</div>
										<div class="col-md-2.5">
											<input type="text" class="form-control" value="{{ $oe->remaining_balance }}" disabled>
										</div>
										@endif
										@endforeach
									</div>
								</div>
								<div class="card-body">
									@include('partials.flash-message')
									<table class="table table-striped datatablejs">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Item Name</th>
												<th>Description</th>
												<th>Date</th>
												<th>Amount</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<?php $count=1?>
											@foreach($officeexpenditure as $oe)	
											@if($oe->item_name)
											<td><?php echo $count;?></td>
											<td>{{ $oe->item_name }}</td>
											<td>{{ $oe->description }}</td>
											<td>{{ $oe->date }}</td>
											<td>{{ $oe->amount }}</td>
											<td>
												<!-- 	<a href="{{ route('officeexpenditure.show',$oe->id) }}" class="btn btn-success btn-xs">View</a>	 -->
												<a href="{{ route('officeexpenditure.edit',$oe->id) }}" class="btn btn-info btn-xs">Update</a>	
											</td>
										</tr>
										<?php $count++?>
										@endif
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('partials.footer')
</div>
@include ('partials.js-libraries')
</body>
</html>