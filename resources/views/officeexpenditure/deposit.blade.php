<!DOCTYPE html>

<html>

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Kashaf Enterprices</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')

</head>

<body>

	<div class="wrapper">

		@include('partials.header')

		@include('partials.sidebar-menu')

		<div class="main-panel">

			<div class="content">

				<div class="container-fluid">		   						

					<!-- <h4 class="page-title">Office Expenses</h4> -->

						<div class="row">

							<div class="col-md-2"></div>

							<div class="col-md-8">

								<div class="card">

									<div class="card-header">

										<div class="card-title">Add Deposit</div>

									</div>

									<div class="card-body">

										@include('partials.flash-message')

										<form id="formvalidation" method="POST" action="addofficedeposit">

											<input type="hidden" name="_token" value="{{ csrf_token() }}">

											<div class="row">

												<div class="form-group col-md-2"></div>

												<div class="form-group col-md-4">

													<label for="deposit">Deposit Amount</label>

													<input type="textbox" class="validate[required, custom[number],minSize[1]] form-control" name="deposited" placeholder="Enter Deposit Amount" >

												</div>

												<div class="form-group col-md-4">

													<label for="rv_date">Date</label>

													<input type="date" class="validate[required] form-control" name="date" placeholder="Enter Deposit Date"  autocomplete="off">

												</div>

											</div>

											<div class="row">

												<div class="form-group col-md-2"></div>

												<div class="form-group col-md-4">

													<input type="Submit" name="" class="btn btn-success form-control" value="Submit">

												</div>

												<div class="form-group col-md-4">	

													<input type="reset" class="btn btn-danger form-control" value="Cancel">

												</div>

											</div>

										</form>

									</div>

									

								</div>

							</div>

						</div>



						<div class="row">

							<div class="col-md-2"></div>

							<div class="col-md-8">

								<div class="card">

									<div class="card-header">

										<div class="card-title">Deposit List</div>

									</div>

									<div class="card-body">

										<table class="table table-striped datatablejs">

											<thead>

												<tr>

													<th scope="col">S.No</th>

													<th scope="col">Amount</th>

													<th scope="col">Date</th>

													<th scope="col">Action</th>

												</tr>

											</thead>

											<tbody>

												@foreach($officedeposits as $officedeposit)

												<tr>

													<td>{{$loop->iteration}}</td>

													<td>{{$officedeposit->deposited}} 

													<td>{{$officedeposit->date}}</td>

													<td>

														
														<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>

														<form onsubmit="return confirm('Do you really want to delete?');" action="/deleteofficedeposit/{{$officedeposit->id}}" method="post">

														<input type="hidden" name="_token" value="{{ csrf_token() }}">

														</form>

													</td>

												</tr>

												@endforeach

											</tbody>

										</table>

									</div>

								</div>

							</div>

						</div>

				</div>

			</div>

			@include('partials.footer')

		</div>

		@include ('partials.js-libraries')

	</div>

</body>

</html>