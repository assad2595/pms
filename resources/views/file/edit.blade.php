<!DOCTYPE html>

<html>

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />



	@include('partials.head')

</head>

<body>

	<div class="wrapper">

		@include('partials.header')

		@include('partials.sidebar-menu')

		<div class="main-panel">

			<div class="content">

				<div class="container-fluid">

					<!-- <h4 class="page-title">File Purchase</h4> -->

					<div class=row>

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<div class="card-title">Update File Purchase</div>

								</div>

								<div class="card-body">

									<form id="formvalidation" method="post" action="{{ route('file.update',$file->id) }}">

										@method('PUT')

										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										@if($file->file_status=='Available')

											<input type="hidden" name="file_status" value="Available">

										@else

											<input type="hidden" name="file_status" value="Sold">

										@endif

										<div class="row">

											<div class="form-group col-md-4">	

												<label for="purchaser_name">Purchaser Name</label>	

												<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="p_name" placeholder="Enter Purchaser Name"  value="{{ $file->p_name}}">		

											</div>

											<div class="form-group col-md-4">

												<label for="c_o">C.O</label>

												<input type="text" class="validate[custom[onlyLetterSp],minSize[3]] form-control" name="p_co" placeholder="Enter C.O"  value="{{ $file->p_co }}"  >

											</div>

											<div class="form-group col-md-4">

												<label for="mobile_no">Mobile Number</label>

												<input type="text" class="validate[required, custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="p_mobilenumber" placeholder="e.g 0333-1234567"  value="{{ $file->p_mobilenumber }}" >

											</div>

										</div>



										<div class="row">

											<div class="form-group col-md-3">

												<label for="landpur_no">Land Pur (L.P) No</label>

												<input type="text" class="validate[custom[number]] form-control" value="{{ 	$file->landpur_no }}" name="landpur_no" placeholder="Enter Land Purchase No">	

											</div>

											<div class="form-group col-md-3">

												<label for="cat_of_file">Category Of File</label>

												<select class="validate[required] form-control" name="cat_of_file">

													<option value="{{$file->cat_of_file}}">{{$file->cat_of_file}}</option>

													<option value="Land">Land</option>

													<option value="Coy Fin">Coy Fin</option>

												</select>

											</div>

											<div class="form-group col-md-3">

												<label for="type_of_file">Type Of File</label>

												<select class="validate[required] form-control" name="type_of_file">

													<option value="{{$file->type_of_file}}">{{$file->type_of_file}}</option>

													<option value="Affidavit">Affidavit</option>

													<option value="Aloc">Aloc</option>

												</select>	

											</div>

											<div class="form-group col-md-3">

												<label for="date">Date</label>

												<input type="Date" class="validate[required] form-control" name="p_date" placeholder="Enter Date"  value="{{ $file->p_date }}" >

											</div>

										</div>

										<div class="row">

											<div class="form-group col-md-6">

												<label for="contents">Contents</label>

												<textarea type="text" class="validate[required] form-control" style="resize: none;" name="p_contents" placeholder="Enter Contents">{{ $file->p_contents }}</textarea>

											</div>

											<div class="form-group col-md-3"></div>

											<div class="form-group col-md-3">

												<label for="p_price">Purchase Price</label>

												<input type="text" class="validate[required, custom[number],minSize[1]] form-control" name="p_price" placeholder="Enter Purchase Price"  value="{{ $file->p_price }}" readonly >

											</div>

										</div>



										<div class="row justify-content-md-center">

											<div class="col-md-auto">

												<button class="btn btn-primary" value="Save">Save Record</button>

											</div>

										</div>



									</form>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		@include('partials.footer')

	</div>

	@include ('partials.js-libraries')

</body>

</html>