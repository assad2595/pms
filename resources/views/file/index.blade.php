<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="row">
										<div class="col-md-8 card-title">File Purchase List</div>
										<div class="col-md-4">
											<a href="{{ route('file.create') }}" class="btn btn-primary float-right">Create New Purchase</a>
										</div>
									</div>
								</div>
								
								<div class="card-body">
								@include('partials.flash-message')
									<table class="table table-striped datatablejs">
										<thead>
											<tr>
												<th>S No.</th>
												<th>Purchaser Name</th>
												<th>C.O</th>
												<th>Mobile Number</th>
												<th>Purchase Price</th>
												<th>Date</th>
												<th>Actions</th> 
											</tr>
										</thead>
										<tbody>
											<?php $count=1?>
											@foreach($files as $p)
											@if($p->file_status=='Available')
											<tr>
												<td><?php echo $count;?></td>
												<td>{{ $p->p_name }}</td>
												<td>{{ $p->p_co }}</td>
												<td>{{ $p->p_mobilenumber }}</td>
												<td>{{ $p->p_price }}</td>
												<td>{{ $p->p_date }}</td>
												<td>
													<a href="{{ route('file.show',$p->id) }}" class="btn btn-success btn-xs">View</a>	
													<a href='{{ url("/file/createfilesale/{$p->id}") }}' class="btn btn-info btn-xs">Sale</a>	
													<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
													<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('file.destroy',$p->id) }}" method="post">
														@method('DELETE')
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													</form>
												</td>
											</tr>
											<?php $count++?>
											@endif
											@endforeach
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</body>
</html>