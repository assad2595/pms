<!DOCTYPE html>

<html>

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />



	@include('partials.head')

</head>

<body>

	<div class="wrapper">

		@include('partials.header')

		@include('partials.sidebar-menu')

		<div class="main-panel">

			<div class="content">

				<div class="container-fluid">

					<h4>File History</h4>

					<!-- When click on view purchase history -->

					<div class=row>

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<div class="row">

										<div class="col-md-8 card-title">File Purchase</div>

										<div class="col-md-4">

											@if(count($filetransaction) > 0)

												@foreach($filetransaction as $ft)

													@if ($loop->first)

														@if($ft->p_amount_balance == 0)

															<button class="btn btn-success float-right">Payment Completed</button>

														@else

														<a href="{{ route('filetransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

														@endif

													@endif

												@endforeach

											@else

											<a href="{{ route('filetransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

											@endif

										</div>

									</div>

								</div>

								<div class="card-body">

									<form method="get" action="">

										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="row">

											<div class="form-group col-md-4">	

												<label for="purchaser_name">Purchaser Name</label>	

												<input type="text" class="form-control" value="{{ $file->p_name}}" disabled>	

											</div>

											<div class="form-group col-md-4">

												<label for="c_o">C.O</label>

												<input type="text" class="form-control" value="{{ $file->p_co }}" disabled >

											</div>

											<div class="form-group col-md-4">

												<label for="mobile_no">Mobile Number</label>

												<input type="text" class="form-control" value="{{ $file->p_mobilenumber }}" disabled>

											</div>

										</div>

										<div class="row">

											<div class="form-group col-md-3">

												<label for="landpur_no">Land Pur (L.P) No</label>

												<input type="text" class="form-control" value="{{ 	$file->landpur_no }}" disabled>	

											</div>

											<div class="form-group col-md-3">

												<label for="cat_of_file">Category Of File</label>

												<input type="text" class="form-control" value="{{ $file->cat_of_file}}" disabled>	

											</div>

											<div class="form-group col-md-3">

												<label for="type_of_file">Type Of File</label>

												<input class="form-control" value="{{ $file->type_of_file}}" disabled>

											</div>

											<div class="form-group col-md-3">

												<label for="date">Date</label>

												<input type="text" class="form-control" value="{{ $file->p_date }}" disabled>	

											</div>

										</div>

										<div class="row">

											<div class="form-group col-md-6">

												<label for="contents">Contents</label>

												<textarea type="text" class="form-control" disabled style="resize: none;">{{ $file->p_contents }}</textarea>				

											</div>

											<div class="form-group col-md-3"></div>

											<div class="form-group col-md-3">

												<label for="p_price">Purchase Price</label>			

												<input type="text" class="form-control" value="{{ $file->p_price }}" disabled>

											</div>

										</div>

										<!-- Display Data related to transactions -->

										<div class="row justify-content-md-center">

											<div class="col-md-auto">

												<a href="{{ route('file.edit',$file->id) }}"" class="btn btn-primary">Edit Purchase Record</a></td>

											</div>

										</div>

									</form>

								</div>

								<hr>

								<!-- start file purchase transactions -->

								<div class="card-header">

									<div class="row">

										<div class="col-md-8 card-title">File Purchase Transaction List</div>

										<div class="col-md-4">

											@if(count($filetransaction) > 0)

												@foreach($filetransaction as $ft)

													@if ($loop->first)

														@if($ft->p_amount_balance == 0)

															<button class="btn btn-success float-right">Payment Completed</button>

														@else

														<a href="{{ route('filetransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

														@endif

													@endif

												@endforeach

											@else

											<a href="{{ route('filetransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

											@endif

										</div>

									</div>

								</div>

								<div class="card-body">

									<table class="table table-striped datatablejs">

										<thead>

											<tr>

												<th>S.No</th>

												<th>Amount Received</th>

												<th>Amount Balance</th>

												<th>Mode of Payment</th>

												<th>Date</th>

											</tr>

										</thead>

										<tbody>



											@foreach($filetransaction as $ft)

											<tr>	

												<td>{{ $loop->iteration }}</td>

												<td>{{ $ft->p_amount_received }}</td>

												<td>{{ $ft->p_amount_balance }}</td>

												<td>
													{{ $ft->p_mode_of_receipt }}
												@if($ft->p_bank)
													: {{ $ft->p_bank }}
												@endif

												@if($ft->cheque_no)
													- Cheque No: {{ $ft->cheque_no }}
												@endif

												@if($ft->p_rvno)
													- RV No: {{ $ft->p_rvno }}
												@endif
												</td>
												<td>{{ $ft->p_rvdate }}</td>

												<td>

													<input type="hidden" name="_token" value="{{ csrf_token() }}">

												</form>

											</td>

										</tr>

										@endforeach

									</tbody>

								</table>

							</div>

						</div>

						<!-- end file purchase transaction -->

					</div>

				</div>					

				<!-- view purchase history here -->

				<div class=row>

					<div class="col-md-12">

						<div class="card">

							<div class="card-header">

								<div class="row">

									<div class=" col-md-8 card-title">File Sale </div>

									<div class="col-md-4">

										@if(count($filesaletransaction) > 0)

											@foreach($filesaletransaction as $filesale)

												@if ($loop->first)

													@if($filesale->s_amount_balance == 0)

														<button class="btn btn-success float-right">Payment Completed</button>

													@else

														<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

													@endif

												@endif

											@endforeach

										@else

											<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

										@endif

									</div>

								</div>

							</div>

							<div class="card-body">

								<form method="get" action="">

									<input type="hidden" name="_token" value="{{ csrf_token() }}">

									<!-- When click on view purchase history -->

									<!-- view purchase history here -->

									<div class="row">

										<div class="form-group col-md-3">	

											<label for="seller_name">Seller Name</label>	

											<input type="text" class="form-control" name="s_name" placeholder="Enter Seller Name" required="" value="{{ $file->s_name}}" disabled>		

										</div>

										<div class="form-group col-md-3">

											<label for="c_o">C.O</label>

											<input type="text" class="form-control" name="s_co" placeholder="Enter C.O" required="" value="{{ $file->s_co }}" disabled >

										</div>

										<div class="form-group col-md-3">

											<label for="mobile_no">Mobile Number</label>

											<input type="text" class="form-control" name="s_mobilenumber" placeholder="e.g 0333-1234567" required="" value="{{ $file->s_mobilenumber }}" disabled>

										</div>

										<div class="form-group col-md-3">

											<label for="s_price">Sale Price</label>				

											<input type="text" class="form-control" name="s_price" placeholder="Enter Sale Price" required="" value="{{ $file->s_price }}" disabled>

										</div>

									</div>



									<div class="row">

										<div class="form-group col-md-3">

											<label for="date">Date</label>

											<input type="text" class="form-control" name="s_date" placeholder="Enter Date" required="" value="{{ $file->s_date }}" disabled>	

										</div>

										<div class="form-group col-md-6">

											<label for="contents">Contents</label>

											<textarea type="text" class="form-control" disabled style="resize: none;">{{ $file->s_contents }}</textarea>	

										</div>

									</div>

									<!-- Display Data related to transactions -->



									<div class="row justify-content-md-center">

										<div class="col-md-auto">

											<a href='{{ url("/file/editfilesale/{$file->id}") }}' class="btn btn-primary ">Edit Sale Record</a>

										</div>

									</div>

								</form>

							</div>

							<!-- start file sale transaction -->

							<hr>

							<div class="card-header">

								<div class="row">

									<div class=" col-md-8 card-title">File Sale Transaction List</div>

									<div class="col-md-4">

										@if(count($filesaletransaction) > 0)

											@foreach($filesaletransaction as $filesale)

												@if ($loop->first)

													@if($filesale->s_amount_balance == 0)

														<button class="btn btn-success float-right">Payment Completed</button>

													@else

														<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

													@endif

												@endif

											@endforeach

										@else

											<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

										@endif

									</div>

								</div>

							</div>

							<div class="card-body">

								<table class="table table-striped datatablejs">

									<thead>

										<tr>

											<th>S.No</th>

											<th>Amount Paid</th>

											<th>Amount Balance</th>

											<th>Mode of Payment</th>

											<th>Date</th>

										</tr>

									</thead>

									<tbody>

										@foreach($filesaletransaction as $filesale)

										<tr>	

											<td>{{$loop->iteration}}</td>

											<td>{{ $filesale->s_amount_paid }}</td>

											<td>{{ $filesale->s_amount_balance }}</td>

											<td>
													{{ $filesale->s_mode_of_receipt }}
												@if($filesale->s_bank)
													: {{ $filesale->s_bank }}
												@endif

												@if($filesale->cheque_no)
													- Cheque No: {{ $filesale->cheque_no }}
												@endif

												@if($filesale->p_rvno)
													- RV No: {{ $filesale->s_rvno }}
												@endif
											</td>

											<td>{{ $filesale->s_rvdate }}</td>

											<td>

												<input type="hidden" name="_token" value="{{ csrf_token() }}">

											</form>

										</td>

									</tr>

									@endforeach

								</tbody>

							</table>

						</div>

						<!-- end file sale transaction -->

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

</div>

</div>

@include('partials.footer')

</div>

@include ('partials.js-libraries')

<script type="text/javascript">

	$(document).ready(function(){

		$('.btnPrint').printPage();

	});

</script>

</body>

</html>