<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Profit/Loss</div>
								</div>
								<div class="card-body">
									<table class="table table-striped datatablejs">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Purchaser Name</th>
												<th>Seller Name</th>
												<th>Purchasing Date</th>
												<th>Selling Date</th>
												<th>Purchase Price</th>
												<th>Sale Price</th>
												<th>Profit/Loss</th>
											</tr>
										</thead>
										<tbody>
											@php ($sumprofit = 0)							
											<?php $count=1?>
											@foreach($file as $f)	
											@if($f->s_price)
											<tr>
											<td><?php echo $count;?></td>
											<td>{{ $f->p_name }}</td>
											<td>{{ $f->s_name }}</td>
											<td>{{ $f->p_date }}</td>
											<td>{{ $f->s_date }}</td>
											<td>{{ $f->p_price }}</td>
											<td>{{ $f->s_price }}</td>
											@if($f->s_price > $f->p_price)
											<td class="table-success">{{ $f->profit }}</td>
											@else
											<td class="table-danger">{{ $f->profit }}</td>
											@endif
											<!-- <td hidden>{{ $sumprofit +=  $f->profit }}</td> -->
											</tr>
											
											<?php $count++?>
											@endif
											@endforeach
										</tbody>
									</table>
									<div class="text-right">
									<!-- 	<br>
										<button class="btn btn-success">Total Profit = {{ $sumprofit }}</button> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
@include ('partials.js-libraries')
</body>
</html>