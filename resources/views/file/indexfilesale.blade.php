<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<h4 class="page-title">File Sale</h4>
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">File Sale List</div>
								</div>
								<div class="card-body">
								@include('partials.flash-message')
									<table class="table table-striped datatablejs">
										<thead>
											<tr>
												<th>S No.</th>
												<th>Seller Name</th>
												<th>C.O</th>
												<th>Mobile Number</th>
												<th>Sale Price</th>
												<th>Date</th>
												<th>Actions</th> 
											</tr>
										</thead>
										<tbody>
											<?php $count=1?>
											@foreach($files as $s)
											@if($s->file_status=='Sold')
											<tr>
												<td><?php echo $count;?></td>
												<td>{{ $s->s_name }}</td>
												<td>{{ $s->s_co }}</td>
												<td>{{ $s->s_mobilenumber }}</td>
												<td>{{ $s->s_price }}</td>
												<td>{{ $s->s_date }}</td>
												<td>
													<a href='{{ url("/file/showfilesale/{$s->id}") }}' class="btn btn-success btn-xs">View</a>	
													<a href='{{ url("/file/showfilehistory/{$s->id}") }}' class="btn btn-primary btn-xs">View File History</a>	
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
												</form>
											</td>
										</tr>
										<?php $count++?>
										@endif
										@endforeach
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('partials.footer')
</div>
@include ('partials.js-libraries')
</body>
</html>