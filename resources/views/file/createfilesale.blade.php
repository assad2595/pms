<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<!-- <h4 class="page-title">File Purchase Record</h4> -->
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">New File Sale</div>
								</div>
								<div class="card-body">
									<form id="formvalidation" method="POST" action="/file/filesale/{{$file->id}}">
										
										<input type="hidden" name="file_status" value="Sold">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" class="form-control" value="{{ $file->id }}">	
										<div class="row">
											<div class="form-group col-md-4">	
												<label>Purchaser Name</label>
												<input type="text" class="form-control"  value="{{ $file->p_name }}" disabled="">		
											</div>
											<div class="form-group col-md-4">
												<label>Purchase Price</label>
												<input type="text" class="form-control"  value="{{ $file->p_price }}" disabled="" >
											</div>
										</div>

										<div class="row">
											<div class="form-group col-md-3">	
												<label for="seller_name">Seller Name</label>	
												<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="s_name" placeholder="Enter Seller Name" >		
											</div>
											<div class="form-group col-md-3">
												<label for="c_o">C.O</label>
												<input type="text" class="validate[custom[onlyLetterSp],minSize[3]] form-control" name="s_co" placeholder="Enter C.O" >
											</div>
											<div class="form-group col-md-3">
												<label for="mobile_no">Mobile Number</label>
												<input type="text" class="validate[required,custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="s_mobilenumber" placeholder="e.g 0333-1234567" >
											</div>
												<div class="form-group col-md-3">
												<label for="s_price">Sale Price</label>				
												<input type="text" class="validate[required,custom[number],minSize[1]] form-control" name="s_price" placeholder="Enter Sale Price" >
										</div>
										</div>
									
										<div class="row">
											<div class="form-group col-md-3">
												<label for="date">Date</label>
												<input type="date" class="validate[required] form-control" name="s_date" placeholder="Enter Date"  autocomplete="off">	
											</div>
											
											
											<div class="form-group col-md-6">
												<label for="contents">Contents</label>
												<textarea type="text" class="validate[required] form-control" name="s_contents" placeholder="Enter Contents"></textarea>
											</div>
										</div>				
										<div class="row justify-content-md-center">
											<div class="col-md-auto">
												<button class="btn btn-primary" type="Submit">Save & Continue To payment</button>
												<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
											</div>
										</div>

									</form>
								</div>
								
								<!-- sold records -->
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include('partials.footer')
</div>
@include ('partials.js-libraries')
</body>
</html>