<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Update File Sale</div>
								</div>
								<div class="card-body">
									<form id="formvalidation" method="POST" action="/file/updatefilesale/{{$file->id}}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="file_status" value="Sold">
										<div class="row">
											<div class="form-group col-md-3">	
												<label for="seller_name">Seller Name</label>	
												<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="s_name" placeholder="Enter Seller Name"  value="{{ $file->s_name}}" >		
											</div>
											<div class="form-group col-md-3">
												<label for="c_o">C.O</label>
												<input type="text" class="validate[custom[onlyLetterSp],minSize[3]] form-control" name="s_co" placeholder="Enter C.O"  value="{{ $file->s_co }}"  >
											</div>
											<div class="form-group col-md-3">
												<label for="mobile_no">Mobile Number</label>
												<input type="text" class="validate[required,custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="s_mobilenumber" placeholder="e.g 0333-1234567"  value="{{ $file->s_mobilenumber }}" >
											</div>
											<div class="form-group col-md-3">
												<label for="s_price">Sale Price</label>
												<input type="text" class="validate[required, custom[number],minSize[1]] form-control" name="s_price" placeholder="Enter Sale Price"  value="{{ $file->s_price }}" readonly="" >
											</div>
										</div>

										<div class="row">
											<div class="form-group col-md-3">
												<label for="date">Date</label>
												<input type="date" class="validate[required] form-control" name="s_date" placeholder="Enter Date"  value="{{ $file->s_date }}" >	
											</div>
											<div class="form-group col-md-6">
												<label for="contents">Contents</label>
												<textarea type="text" class="validate[required] form-control" name="s_contents" placeholder="Enter Contents"  style="resize: none;">{{ $file->s_contents }}</textarea>	
											</div> 
										</div>

										<div class="row justify-content-md-center">
											<div class="col-md-auto">
												<button type="Submit" class="btn btn-primary">Save Record</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</body>
</html>