<!DOCTYPE html>

<html>

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />



	@include('partials.head')

</head>

<body>

	<div class="wrapper">

		@include('partials.header')

		@include('partials.sidebar-menu')

		<div class="main-panel">

			<div class="content">

				<div class="container-fluid">

					<!-- <h4 class="page-title">Property File Records</h4> -->

					<div class=row>

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<div class="card-title">New File Purchase </div>

								</div>

								<div class="card-body">

									<form id="formvalidation" method="post" action="{{ route('file.store') }}">

										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<input type="hidden" name="file_status" value="Available">

										<div class="row">

											<div class="form-group col-md-4">	

												<label for="purchaser_name">Purchaser Name</label>	

												<input type="text" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="p_name" placeholder="Enter Purchaser Name" >	

											</div>

											<div class="form-group col-md-4">

												<label for="c_o">C.O</label>

												<input type="text" class="validate[custom[onlyLetterSp],minSize[3]] form-control" name="p_co" placeholder="Enter C.O" >

											</div>

											<div class="form-group col-md-4">

												<label for="mobile_no">Mobile Number</label>

												<input type="text" class="validate[required, custom[onlyNumberSp],minSize[11],maxSize[11]] form-control" name="p_mobilenumber" placeholder="e.g 03001234567" >

											</div>

										</div>

										<div class="row">	

											<div class="form-group col-md-3">

												<label for="landpur_no">Land Pur (L.P) No</label>

												<input type="text" class="validate[custom[onlyNumberSp]] form-control" name="landpur_no" placeholder="Enter L.P No" >	

											</div>

											<div class="form-group col-md-3">

												<label for="cat_of_file">Category Of File</label>

												<select class="validate[required] form-control" name="cat_of_file">

													<option value="">Choose File Category</option>

													<option>Land</option>

													<option>Coy Fin</option>

												</select>

											</div>

											<div class="form-group col-md-3">

												<label for="type_of_file">Type Of File</label>

												<select class="validate[required] form-control" name="type_of_file">

													<option value="">Choose Type Of File</option>

													<option>Affidavit</option>

													<option>Aloc</option>

												</select>	

											</div>

											<div class="form-group col-md-3">

												<label for="date">Date</label>

												<input type="date" class="validate[required] form-control" name="p_date" placeholder="Enter Date"  autocomplete="off">	

											</div>

										</div>

										<div class="row">

											<div class="form-group col-md-6">

												<label for="contents">Purchase Contents</label>

												<textarea type="text" class="validate[required] form-control" name="p_contents" placeholder="Enter Contents" ></textarea> 

											</div>

											<div class="form-group col-md-3">

												<label for="p_price">Purchase Price</label>				

												<input type="text" class="validate[required, custom[number],minSize[1],min[0]] form-control" name="p_price" placeholder="Enter Purchase Price" >

											</div>

										</div>



										<div class="row">

										</div>					

										<div class="row justify-content-md-center">

											<div class="col-md-auto">

												<button class="btn btn-primary" value="Save">Save & Continue To payment</button>

												<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>

											</div>

										</div>

									</form>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		@include('partials.footer')

	</div>

	@include ('partials.js-libraries')

</body>

</html>