<!DOCTYPE html>

<html>

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />



	@include('partials.head')

</head>

<body>

	<div class="wrapper">

		@include('partials.header')

		@include('partials.sidebar-menu')

		<div class="main-panel">

			<div class="content">

				<div class="container-fluid">

					<div class=row>

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<div class="row">

										<div class=" col-md-8 card-title">File Sale</div>

										<div class="col-md-4">

											@if(count($filesaletransaction) > 0)

												@foreach($filesaletransaction as $filesale)

													@if ($loop->first)

														@if($filesale->s_amount_balance == 0)

															<button class="btn btn-success float-right">Payment Completed</button>

														@else

															<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

														@endif

													@endif

												@endforeach

											@else

												<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

											@endif

										</div>

									</div>

								</div>

								<div class="card-body">

								@include('partials.flash-message')

									<form method="get" action="">

										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<!-- When click on view purchase history -->

										<!-- view purchase history here -->

										<div class="row">

											<div class="form-group col-md-3">	

												<label for="seller_name">Seller Name</label>	

												<input type="text" class="form-control" name="s_name" placeholder="Enter Seller Name" required="" value="{{ $file->s_name}}" disabled>		

											</div>

											<div class="form-group col-md-3">

												<label for="c_o">C.O</label>

												<input type="text" class="form-control" name="s_co" placeholder="Enter C.O" required="" value="{{ $file->s_co }}" disabled >

											</div>

											<div class="form-group col-md-3">

												<label for="mobile_no">Mobile Number</label>

												<input type="text" class="form-control" name="s_mobilenumber" placeholder="e.g 0333-1234567" required="" value="{{ $file->s_mobilenumber }}" disabled>

											</div>

											<div class="form-group col-md-3">

												<label for="s_price">Sale Price</label>				

												<input type="text" class="form-control" name="s_price" placeholder="Enter Sale Price" required="" value="{{ $file->s_price }}" disabled>

											</div>

										</div>



										<div class="row">

											<div class="form-group col-md-3">

												<label for="date">Date</label>

												<input type="text" class="form-control" name="s_date" placeholder="Enter Date" required="" value="{{ $file->s_date }}" disabled>	

											</div>

											<div class="form-group col-md-6">

												<label for="contents">Contents</label>

												<textarea type="text" class="form-control" disabled style="resize: none;">{{ $file->s_contents }}</textarea>	

											</div>

										</div>

										<!-- Display Data related to transactions -->

										

										<div class="row justify-content-md-center">

											<div class="col-md-auto">

												<a href='{{ url("/file/editfilesale/{$file->id}") }}' class="btn btn-primary ">Edit Sale Record</a>

											</div>

										</div>

									</form>

								</div>

							</div>

						</div>

					</div>

					<!-- Transaction Start from here -->

					<div class=row>

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<div class="row">

										<div class=" col-md-8 card-title">File Transaction List</div>

										<div class="col-md-4">

											@if(count($filesaletransaction) > 0)

												@foreach($filesaletransaction as $filesale)

													@if ($loop->first)

														@if($filesale->s_amount_balance == 0)

															<button class="btn btn-success float-right">Payment Completed</button>

														@else

															<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

														@endif

													@endif

												@endforeach

											@else

												<a href="{{ route('filesaletransaction.create',[$file->id]) }}" class="btn btn-primary float-right">Create New Transaction</a>

											@endif

										</div>

									</div>

								</div>

								<div class="card-body">

								@include('partials.flash-message')

									<table class="table table-striped datatablejs">

										<thead>

											<tr>

												<th>S.No</th>

												<th>Amount Paid</th>

												<th>Amount Balance</th>

												<th>Mode of Payment</th>

												<th>Date</th>

											</tr>

										</thead>

										<tbody>

											@foreach($filesaletransaction as $filesale)

											<tr>	

												<td>{{$loop->iteration}}</td>

												<td>{{ $filesale->s_amount_paid }}</td>

												<td>{{ $filesale->s_amount_balance }}</td>

												<td>
													{{ $filesale->s_mode_of_receipt }}
												@if($filesale->s_bank)
													: {{ $filesale->s_bank }}
												@endif

												@if($filesale->cheque_no)
													- Cheque No: {{ $filesale->cheque_no }}
												@endif

												@if($filesale->p_rvno)
													- RV No: {{ $filesale->s_rvno }}
												@endif
												</td>

												<td>{{ $filesale->s_rvdate }}</td>

												<td>

													<input type="hidden" name="_token" value="{{ csrf_token() }}">

												</form>

											</td>

										</tr>

										@endforeach

									</tbody>

								</table>

							</div>

						</div>

					</div>

					<!-- Transaction end -->

				</div>

			</div>

		</div>

	</div>

</div>

</div>

@include('partials.footer')

</div>

@include ('partials.js-libraries')

</body>

</html>