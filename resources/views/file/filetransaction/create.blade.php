<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">New File Transaction</div>
								</div>
								<div class="card-body">
								@include('partials.flash-message')
									<form id="formvalidation" method="post" action="{{ route('filetransaction.store') }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										
												<input type="hidden" class="form-control" name="file_id" value="{{ $data[0] }}" readonly="">
										<div class="row">
											<div class="form-group col-md-4">
												<label>Purchaser Name</label>
												<input type="text" class="form-control" name="p_name" value="{{ $data[1] }}" disabled="">
											</div>
											<div class="form-group col-md-4">
												<label>Total Purchase Price</label>
												<input type="text" class="form-control" name="total_p_price" value="{{ $data[2] }}" id="total_p_price" disabled="">
											</div>
											<div class="form-group col-md-4">
												@if($last_amount==null)
												<input type="hidden" class="form-control" name="r_bal" value="{{$last_amount}}" id="r_bal" disabled="">
												@else
												<label>Remaining Balance</label>
												<input type="text" class="form-control" name="r_bal" value="{{$last_amount}}" id="r_bal" disabled="">
												@endif
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">							
												<label for="amount_received">Amount Received</label>
												<input type="text" class="validate[required, custom[number],minSize[1]] form-control" name="p_amount_received" id="p_amount_received" placeholder="Enter Amount Received" >	
											</div>
											<div class="form-group col-md-4">
												<label>Amount balance</label>
												<input class="form-control" type="text" name="p_amount_balance" id="amt_bal" readonly> 
											</div>
											<div class="form-group col-md-4">
												<label for="mode_of_receipt">Mode of Payment</label>
												<select type="text" class="validate[required] form-control" name="p_mode_of_receipt" placeholder="Enter Mode of Receipt" >	
													<option value="">Select Payment Method</option>
													<option value="Cash">Cash</option>
													<option value="Bank">Bank</option>
												</select>		
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3">							<label for="bank">Bank</label>
												<input type="text" class="validate[custom[onlyLetterSp]] form-control" name="p_bank" placeholder="Enter Bank" >			
											</div>
											<div class="form-group col-md-3">							<label for="bank">Cheque Number</label>
												<input type="text" class="validate[custom[onlyLetterNumber]] form-control" name="cheque_no" placeholder="Enter Cheque No" >			
											</div>
											<div class="form-group col-md-3">							
												<label for="rv_no">R.V No</label>
												<input type="text" class="validate[custom[onlyLetterNumber]] form-control" name="p_rvno" placeholder="Enter R.V No" >			
											</div>
											<div class="form-group col-md-3">							
												<label for="rv_date">Date</label>
												<input type="date" class="validate[required] form-control" name="p_rvdate" placeholder="Enter R.V Date" autocomplete="off">
											</div>		
										</div>				
										<div class="row justify-content-md-center">
											<div class="col-md-auto">
												<button class="btn btn-primary" value="Save">Submit</button>
												<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</body>
</html>