<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Bank Accounts</h4>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Bank Account Details of <b>["{{ $banks->account_title }}"]</b></div>
								</div>
								<div class="card-body">
									<form>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Account Title">Account title</label>
												<input type="textbox" class="validate[required,custom[onlyLetterSp] form-control" value="{{ $banks->account_title }}" disabled="">
											</div>
											<div class="form-group col-md-4">
												<label for="Account">Account No</label>
												<input type="textbox" class="validate[required, custom[onlyNumberSp] form-control" value="{{ $banks->account_no }}" disabled="">
											</div>
											<div class="form-group col-md-4">
												<label for="Account">Account Starting Balance</label>
												<input type="textbox" class="validate[required, custom[number],minSize[1]] form-control" value="{{ $banks->starting_balance }}" disabled="">
											</div>
											<div class="form-group col-md-3">
												<label for="Bank Name">Bank Name</label>
												<input type="textbox" class="validate[required,custom[onlyLetterSp] form-control" value="{{ $banks->bank_name }}" disabled="">
											</div>
											<div class="form-group col-md-3">
												<label for="Branch Name">Branch Name</label>
												<input type="textbox" class="validate[required] form-control" value="{{ $banks->branch_name }}" disabled="">
											</div>
											<div class="form-group col-md-3">
												<label for="Branch Code">Branch Code</label>
												<input type="textbox" class="validate[required, custom[onlyNumberSp] form-control" value="{{ $banks->branch_code }}" disabled="">
											</div>
											<div class="form-group col-md-3">
												<label for="taxpayer status">Account Tax-Payer Status &nbsp;</label>
												<label class="form-radio-label">
													<input class="form-radio-input" type="radio"
													{{ $banks->taxpayer_status == 'Filer' ? 'checked' : '' }} disabled="">
													<span class="form-radio-sign">Filer</span>
												</label>
												<label class="form-radio-label" style="margin-top: 7px;">
													<input class="form-radio-input" type="radio" 
													{{ $banks->taxpayer_status == 'NonFiler' ? 'checked' : '' }} disabled="">
													<span class="form-radio-sign">Non-Filer</span>
												</label>
											</div>
											<div class="form-group col-md-4">
												<label for="Branch Address">Branch Address</label>
												<textarea placeholder="Enter Address" class="validate[required] form-control"  cols="10" rows="2" disabled="">{{ $banks->branch_address }}</textarea>
											</div>
										</div>
										<div class="card-action row justify-content-md-center">
											<div class="col-md-auto">
												<a href="{{ route('banks.edit',$banks->id) }}" class="btn btn-default">Edit Bank Details</a>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>