<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Bank Accounts</h4>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Add Bank Account</div>
									</div>
									<div class="card-body">
										<form id="formvalidation" method="post" action="{{ route('banks.store') }}">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="row">
												<div class="form-group col-md-4">
													<label for="Account Title">Account title</label>
													<input type="textbox" class="validate[required,custom[onlyLetterSp] form-control" name="account_title" placeholder="Enter Account Title">
												</div>
												<div class="form-group col-md-4">
													<label for="Account">Account No</label>
													<input type="textbox" class="validate[required, custom[onlyNumberSp] form-control" name="account_no" placeholder="Enter Account No">
												</div>
												<div class="form-group col-md-4">
													<label for="Account">Account Starting Balance</label>
													<input type="textbox" class="validate[required, custom[number],minSize[1]] form-control" name="starting_balance" placeholder="Starting Balance">
												</div>
												<div class="form-group col-md-3">
													<label for="Bank Name">Bank Name</label>
													<input type="textbox" class="validate[required,custom[onlyLetterSp] form-control" name="bank_name" placeholder="Enter Bank Name">
												</div>
												<div class="form-group col-md-3">
													<label for="Branch Name">Branch Name</label>
													<input type="textbox" class="validate[required] form-control" name="branch_name" placeholder="Enter Branch Name">
												</div>
												<div class="form-group col-md-3">
													<label for="Branch Code">Branch Code</label>
													<input type="textbox" class="validate[required, custom[onlyNumberSp] form-control" name="branch_code" placeholder="Enter Branch Code">
												</div>
												<div class="form-group col-md-3">
													<label for="taxpayer status">Account Tax-Payer Status</label>
													<label class="form-radio-label">
													<input class="form-radio-input" type="radio" id="filer" name="taxpayer_status" value="Filer" checked="">
													<span class="form-radio-sign">Filer</span>
													</label>
													<label class="form-radio-label" style="margin-top: 7px;">
													<input class="form-radio-input" type="radio" id="nonfiler" name="taxpayer_status" value="NonFiler">
													<span class="form-radio-sign">Non-Filer</span>
													</label>
												</div>
												<div class="form-group col-md-4">
													<label for="Branch Address">Branch Address</label>
													<textarea placeholder="Enter Address" class="validate[required] form-control" name="branch_address" cols="10" rows="2"></textarea>
												</div>
											</div>
											<div class="card-action row justify-content-md-center">
												<div class="col-md-auto">
													<input type="Submit" name="" class="btn btn-success" value="Submit">
													<input type="reset" class="btn btn-danger" value="Cancel">
												</div>
											</div>
										</form>
									</div>
									
								</div>
							</div>	

							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Bank Account List</div>
									</div>
									<div class="card-body">
										<table class="table table-striped mt-3 datatablejs">
											<thead>
												<tr>
													<th scope="col">S.No</th>
													<th scope="col">Bank & Branch Name - Code</th>
													<th scope="col">Account Title</th>
													<th scope="col">Account No</th>
													<th scope="col">Tax-Payer Status</th>
													<th scope="col">Actions</th>
												</tr>
											</thead>
											<tbody>
												@foreach($officebank as $officebanks)
												<tr>
													<td>{{$loop->iteration}}</td>
													<td>{{ $officebanks->bank_name }},{{ $officebanks->branch_name }}-{{ $officebanks->branch_code }}</td>
													<td>{{ $officebanks->account_title }}</td>
													<td>{{ $officebanks->account_no }}</td>
													<td>
														@if($officebanks->taxpayer_status == "Filer")
														<span class="badge badge-primary">{{ $officebanks->taxpayer_status }}</span>
														@else
														<span class="badge badge-warning">{{ $officebanks->taxpayer_status }}</span>
														@endif
													</td>
													<td>
														<a href="{{ route('banks.show', $officebanks->id) }}" class="btn btn-success btn-xs">View</a>
														<a href="{{ route('banks.edit',$officebanks->id) }}" class="btn btn-info btn-xs">Edit</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>

						</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>