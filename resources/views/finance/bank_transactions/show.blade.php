<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">View Bank Transactions</h4>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Transaction Details</div>
								</div>
								<div class="card-body">	
									@foreach($transaction as $trans)	
											<div class="row">
												@foreach($officebank as $bank)
													@if($trans->bank_account_id == $bank->id)
												<div class="form-group col-md-4">
													<label for="Account Select">Bank Account</label>
													<select id="bank_selection" class="form-control input-solid" disabled="">
														<option value="">{{ $bank->bank_name }}-{{ $bank->account_title }}</option>
													</select>
												</div>
													@endif
												@endforeach
												<div class="form-group col-md-4">
													<label for="Type Select">Transaction Type</label>
													<select id="type_selection" class="form-control input-solid" name="trans_type" disabled="">
														<option value="">{{ $trans->trans_type }}</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="Method Select">Payment Method</label>
													<select id="method_selection" class="form-control input-solid" name="payment_method" disabled="">
														<option value="">{{ $trans->payment_method }}</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-4">
													<label for="Account">Transaction Amount</label>
													<input type="textbox" class="form-control" name="trans_amount" value="{{ $trans->trans_amount}}"disabled="">
												</div>
												<div class="form-group col-md-3" id="cheque" >
													<label for="Account">Cheque No</label>
													@if(isset($trans->cheque_no))
													<input type="textbox" class="form-control" name="cheque_no" value="{{ $trans->cheque_no}}" disabled="">
													@else
													<input type="textbox" class="form-control" name="cheque_no" value="N/A" disabled="">
													@endif
												</div>
												<div class="form-group col-md-3" id="debit">
													<label for="Account">With Holding Tax</label>
													@if(isset($trans->w_h_tax))
														<input type="textbox" class="form-control" name="w_h_tax" value="{{ $trans->w_h_tax }}" disabled="">
													@else
														<input type="textbox" class="form-control" name="w_h_tax" value="N/A" disabled="">
													@endif
												</div>
												<div class="form-group col-md-3" id="credit" >
													<label for="Account">Bank Charges</label>
													@if(isset($trans->bank_charges))
														<input type="textbox" class="form-control" name="bank_charges" value="{{ $trans->bank_charges}}"disabled="">
													@else
														<input type="textbox" class="form-control" name="bank_charges" value="N/A" disabled="">
													@endif
												</div>
												<div class="form-group col-md-4">
													<label for="Bank Name">Transaction Description</label>
													<textarea type="textbox" class="form-control" cols="10" rows="2" name="trans_descp" disabled="">
														{{ $trans->trans_descp}}
													</textarea>
												</div>
												<div class="form-group col-md-4">
													<label for="Account">Transaction Date</label>
													<input type="date" value="{{ $trans->trans_date}}" class="form-control" name="trans_date" disabled="">
												</div>
											</div>
										<div class="card-action row justify-content-md-center"">
											<div class="col-md-auto">
												<a href="{{ route('bank_transaction.edit',$trans->id) }}" class="btn btn-default">Edit Transaction</a>
											</div>
										</div>		
									@endforeach
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>