<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<h4 class="page-title">Bank Transaction History</h4>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Select Bank Account or Bank Account and Date:</div>
								</div>
								<div class="card-body">
									<form id="formvalidation" method="GET" action="{{ route('bank_transaction.index') }}">	
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Account Select">Bank Account</label>
												<select id="bank_selection" name="id" class="form-control input-solid" required="">
													<option value="">Select Bank Account</option>
													@foreach($bank_accounts as $bank)
													<option value="{{ $bank->id }}">
														{{ $bank->bank_name }}-{{ $bank->account_title }}
													</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-md-3">
												<label for="Account Select">From Date</label>
												<input type="date" class="form-control" name="from">
											</div>
											<div class="form-group col-md-3">
												<label for="Account Select">To Date</label>
												<input type="date" class="form-control" name="to">
											</div>
										</div>
										<div class="card-action row justify-content-md-center"">
											<div class="col-md-auto">
												<input type="Submit" class="btn btn-warning" value="Submit">
												<input type="reset" class="btn btn-danger" value="Cancel">
											</div>
											<div class="col-md-1" style="margin-top: -15px;">
												<a href="{{ route('bank_transaction.create') }}" style="margin-left: 177px;" class="btn btn-primary text-left mt-3 mb-3"><i class="la la-plus"></i> Add New Transaction</a>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="row">
										<div class="col-md-auto" style="margin-top: 15px;">
											@foreach($account_details as $account)
											<button class="btn btn-info" value="" readonly="">
												Account = {{ $account->bank_name }} - {{ $account->account_title }}
											</button>
											@endforeach
										</div>
										<div class="col-md-3" style="margin-top: 15px;">
											@foreach($account_details as $account)
											@if(empty($from) && empty($to))
											@else
											<button class="btn btn-default" value="" readonly="">
												Date = {{ date('d M Y', strtotime($from)) }} to {{ date('d M Y', strtotime($to)) }}
											</button>
											@endif
											@endforeach
										</div>
										@if(!empty($w_h_tax))
										<div class="col-md-auto" style="margin-top: 15px; margin-left: 52px;">
											<button class="btn btn-danger" value="" readonly="">Total W-H-Tax = {{ $w_h_tax}}
											</button>
										</div>
										@endif
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<input type="text" class="btn btn-warning" value="Starting Bal = {{ $starting_balance }}" readonly="">
										</div>
										<div class="form-group col-md-3">
											<input type="text" class="btn btn-success" value="Total Credit = {{ $total_credit }}" readonly="">
										</div>
										<div class="form-group col-md-3">
											<input type="text" class="btn btn-danger" value="Total Debit = {{ $total_debit }}" readonly="">
										</div>
										<div class="form-group col-md-2.5">
											<input type="text" class="btn btn-primary" value="Balance = {{ $balance }}" readonly="">
										</div>
									</div>
								</div>
								<div class="card-body">
									<table class="table table-striped mt-3 datatablejs">
										<thead>
											<tr>
												<th scope="col">S.No</th>
												<th scope="col">Description</th>
												<th scope="col">Amount</th>
												<th scope="col">Type</th>
												<th scope="col">Date</th>
												<th scope="col">Actions</th>
											</tr>
										</thead>
										<tbody>
											@foreach($transactions as $transaction)
											<tr>
												<td>{{$loop->iteration}}</td>
												<td>{{$transaction->trans_descp}}</td>
												<td>{{$transaction->trans_amount}}</td>
												<td>{{$transaction->trans_type}}</td>
												<td>{{ date('d M Y', strtotime($transaction->trans_date)) }}</td>
												<td>
													<a href="{{ route('bank_transaction.show',$transaction->id) }}" class="btn btn-success btn-xs">View</a>	
													<a href="{{ route('bank_transaction.edit',$transaction->id) }}" class="btn btn-info btn-xs">Edit</a>	
													<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
													<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('bank_transaction.destroy',$transaction->id) }}" method="post">
														@method('DELETE')
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													</form>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>					
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</div>
</body>
</html>