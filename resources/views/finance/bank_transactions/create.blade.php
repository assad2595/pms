<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Bank Transactions</h4>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Add Transaction</div>
								</div>
								<div class="card-body">
									<form method="post" action="{{ route('bank_transaction.store') }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Account Select">Bank Account</label>
												<select id="bank_selection" class="form-control input-solid" required="">
													<option value="">Select Bank</option>
													@foreach($officebank as $ob)
													<option value="{{ $ob->id }}-{{$ob->taxpayer_status}}">
														{{ $ob->bank_name }}-{{ $ob->account_title }}
													</option>
													@endforeach
												</select>
												<input type="hidden" name="bank_account_id" id="bank_id">
												<input type="hidden" id="taxpayer_status">
											</div>
											<div class="form-group col-md-4">
												<label for="Type Select">Transaction Type</label>
												<select id="type_selection" class="form-control input-solid" name="trans_type" required="">
													<option value="">Select Type</option>
													<option value="Debit">Debit</option>
													<option value="Credit">Credit</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="Method Select">Payment Method</label>
												<select id="method_selection" class="form-control input-solid" name="payment_method" required="">
													<option value="">Select Method</option>
													<option value="Cash">Cash</option>
													<option value="Cheque">Cheque</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Account">Transaction Amount</label>
												<input type="textbox" class="form-control" id="amount" name="trans_amount" placeholder="Enter Amount">
											</div>
											<div class="form-group col-md-3" id="cheque" style="display: none;">
												<label for="Account">Cheque No</label>
												<input type="textbox" class="form-control" name="cheque_no" placeholder="Cheque No">
											</div>
											<div class="form-group col-md-3" id="debit" style="display: none;">
												<label for="Account">With Holding Tax</label>
												<input type="textbox" class="form-control" id="w_h_tax" name="w_h_tax" placeholder="Total Tax" readonly="">
											</div>
											<div class="form-group col-md-3" id="credit" style="display: none;">
												<label for="Account">Bank Charges</label>
												<input type="textbox" class="form-control" name="bank_charges" placeholder="Bank Charges">
											</div>
											<div class="form-group col-md-4">
												<label for="Transaction Descp">Transaction Description</label>
												<textarea placeholder="Enter Description" class="validate[required] form-control" name="trans_descp" cols="10" rows="2"></textarea>
											</div>
											<div class="form-group col-md-4">
												<label for="Account">Transaction Date</label>
												<input type="date" class="form-control" name="trans_date">
											</div>	
										</div>
										<div class="card-action row justify-content-md-center">
											<div class="col-md-auto">
												<input type="Submit" name="" class="btn btn-success" value="Submit">
												<input type="reset" class="btn btn-danger" value="Cancel">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>