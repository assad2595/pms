<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Update Bank Transactions</h4>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Update Transaction on Account
										@foreach($account_id as $bank_id)
											@foreach($banks as $bank)
												@if($bank_id->bank_account_id == $bank->id)
                                        			<b>["{{ $bank->account_title }}"]</b>
                                        		@endif
                                        	@endforeach
                                        @endforeach
									</div>	
								</div>
								<div class="card-body">
									<form method="post" action="{{ route('bank_transaction.update', $transaction->id) }}">
										@method('PUT')
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Account Select">Bank Account</label>
												<select id="bank_selection" class="form-control input-solid" required="">
													<option value="">Select Bank</option>
													@foreach($banks as $bank)
													<option value="{{ $bank->id }}-{{$bank->taxpayer_status}}">
														{{ $bank->bank_name }}-{{ $bank->account_title }}
													</option>
													@endforeach
												</select>
												<input type="hidden" name="bank_account_id" id="bank_id">
												<input type="hidden" id="taxpayer_status">
											</div>										
											<div class="form-group col-md-4">
												<label for="Type Select">Transaction Type</label>
												<select id="type_selection" class="form-control input-solid" name="trans_type" required="">
													<option value="">Select Type</option>
													<option value="Debit">Debit</option>
													<option value="Credit">Credit</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="Method Select">Payment Method</label>
												<select id="method_selection" class="form-control input-solid" name="payment_method" required="">
													<option value="">Select Method</option>
													<option value="Cash">Cash</option>
													<option value="Cheque">Cheque</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Account">Transaction Amount</label>
												<input type="textbox" class="form-control" id="amount" name="trans_amount" value="{{ $transaction->trans_amount }}" placeholder="Enter Amount">
											</div>
											<div class="form-group col-md-3" id="cheque" style="display: none;">
												<label for="Account">Cheque No</label>
												<input type="textbox" class="form-control" name="cheque_no" value="{{ $transaction->cheque_no}}" placeholder="Cheque No">
											</div>
											<div class="form-group col-md-3" id="debit">
												<label for="Account">With Holding Tax</label>
												<input type="textbox" class="form-control" id="w_h_tax" name="w_h_tax" value="{{ $transaction->w_h_tax }}"placeholder="Total Tax" readonly="">
											</div>
											<div class="form-group col-md-3" id="credit">
												<label for="Account">Bank Charges</label>
												<input type="textbox" class="form-control" name="bank_charges" value="{{ $transaction->bank_charges }}" placeholder="Bank Charges">
											</div>
											<div class="form-group col-md-4">
												<label for="Bank Name">Transaction Description</label>
												<textarea type="textbox" class="form-control" cols="10" rows="2" name="trans_descp">
													{{ $transaction->trans_descp }}
												</textarea>
											</div>	
											<div class="form-group col-md-4">
												<label for="Account">Transaction Date</label>
												<input type="date" class="form-control" value="{{ $transaction->trans_date}}" name="trans_date">
											</div>	
										</div>
										<div class="card-action row justify-content-md-center"">
											<div class="col-md-auto">
												<input type="Submit" name="" class="btn btn-success" value="Update Transaction">
											</div>
										</div>
									</form>		
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>