<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Kashaf-Enterprises</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    @include('partials.head')
</head>
<body>
    <div class="wrapper">
        @include('partials.header')
        @include('partials.sidebar-menu')
        <div class="main-panel">
            <div class="content">
                <div class="container-fluid">
                    <h4 class="page-title">Employee Advance and Salary Slip Management</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Select Employees and Salary Month</div>
                                </div>
                                <div class="card-body">
                                    <form id="formvalidation" method="GET" action="{{ route('payslips.index') }}">  
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label for="Employee Select">Select Employees</label>
                                                <select  name="id" class="form-control input-solid" required="">
                                                    <option value="">Select Employees</option>
                                                    @foreach($employees as $emp)
                                                    <option value="{{ $emp->id }}">{{ $emp->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="Account Select">Salary Month</label>
                                                <input type="month" class="form-control" name="salary_month" required="">
                                            </div>
                                        </div>
                                        <div class="card-action row justify-content-md-center"">
                                            <div class="col-md-auto">
                                                <input type="Submit" class="btn btn-warning" value="Submit">
                                                <input type="reset" class="btn btn-danger" value="Cancel">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form class="form-control" method="POST" action="{{ route('payslips.store') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped mt-3 datatablejs">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Loan Amount</th>
                                                    <th scope="col">Installment</th>
                                                    <th scope="col">Rem Loan</th>
                                                    <th scope="col">Total Salary</th>
                                                    <th scope="col">Dispatch Salary</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($employee_detail))
                                                @foreach($employee_detail as $emp)    
                                                <tr>
                                                    <input type="hidden" class="form-control" name="emp_id" value="{{ $emp->id }}">
                                                    <input type="hidden" class="form-control" name="salary_month" value="{{ $salary_month }}">
                                                    <td>
                                                        <input type="text" class="form-control" name="name" value="{{$emp->name}}" readonly="">
                                                    </td>
                                                    <td>
                                                        @if($loan_detail =="[]")
                                                        <input type="text" class="form-control" value="N/A" readonly=""> 
                                                        @else
                                                        @foreach($loan_detail as $loan)
                                                        <input type="text" class="form-control" value="{{ $loan->loan_amount }}" readonly="">
                                                        @endforeach
                                                        @endif    
                                                    </td>
                                                    <td>
                                                        @if($loan_detail =="[]")
                                                        <input type="text" class="form-control" name="monthly_installment" value="N/A" readonly="">
                                                        @else
                                                        @foreach($loan_detail as $loan)
                                                        <input type="text" class="form-control" name="monthly_installment" value="{{ $loan->monthly_installment }}" readonly="">
                                                        @endforeach
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($loan_detail =="[]")
                                                        <input type="text" class="form-control" name="rem_loan_balance" value="N/A" readonly="">
                                                        @elseif($rem_bal =="[]")
                                                            @foreach($loan_detail as $loan)
                                                            <input type="text" class="form-control" name="rem_loan_balance" value="{{ $loan->loan_amount-monthly_installment }}" readonly="">
                                                            @endforeach
                                                        @else
                                                        @foreach($rem_bal as $bal)
                                                        <input type="text" class="form-control" name="rem_loan_balance" value="{{ $bal->rem_loan_balance - $bal->installment_received }}" readonly="">
                                                        @endforeach
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="{{ $emp->total_salary }}" readonly="">
                                                    </td>
                                                    <td>
                                                        @if($loan_detail =="[]")
                                                        <input type="text" class="form-control" name="salary_dispatched" value="{{ $emp->total_salary }}" readonly="">     
                                                        @else
                                                        @foreach($loan_detail as $loan)
                                                        <input type="text" class="form-control" name="salary_dispatched" value="{{ $emp->total_salary - $loan->monthly_installment }}" readonly="">
                                                        @endforeach
                                                        @endif
                                                    </td>
                                                    <td><button type="Submit" class="btn btn-success btn-sm">Add Salary</button>
                                                    </form>
                                                </tr>    
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials.footer')
        </div>
        @include ('partials.js-libraries')
    </div>
</body>
</html>