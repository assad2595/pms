<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">nb
			<div class="content">
				<div class="container-fluid">
					<h4 class="page-title">Employees Monthly Salary Slip</h4>
						<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Select <b>Month</b> for Which You Want to Print the Salary Slip</div>
                                </div>
                                <div class="card-body">
                                    <form id="formvalidation" method="GET" action="{{ url('payslips/slips') }}">  
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="Account Select">Select Month</label>
                                                <input type="month" class="form-control" name="month">
                                            </div>
                                        </div>
                                        <div class="card-action row justify-content-md-center"">
                                            <div class="col-md-auto">
                                                <input type="Submit" class="btn btn-warning" value="Submit">
                                                <input type="reset" class="btn btn-danger" value="Cancel">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(count($slip)>0)
					<div class="card">
						<div class="card-header">
							<!-- <div class="row">
								<div class="col-md-12">
									<a href="#"  class="btn btn-success">Print Slip</a>
								</div>
							</div> -->
							</div>
							
							<div class="card-body">
								<table id="print" class="table table-bordered-bd-*states datatablesjs">
									<thead>
										<tr>
											<th scope="col">S.No</th>
											<th scope="col">Name</th>
											<th scope="col">Salary Month</th>
											<th scope="col">Installment</th>
											<th scope="col">Rem Laon</th>	
											<th scope="col">Salary</th>
											<th scope="col">Signature</th>
										</tr>
									</thead>
									<tbody>
										@foreach($slip as $pay_slip)
											<tr>
												<td>{{ $loop->iteration }}</td>
												<td>{{ $pay_slip->name }}</td>
												<td>{{ date('M Y', strtotime($pay_slip->salary_month)) }}</td>
												<td>{{ $pay_slip->installment_received }}</td>
												<td>{{ $pay_slip->rem_loan_balance }}</td>
												<td>{{ $pay_slip->salary_dispatched }}</td>
												<td></td>
											</tr>
										@endforeach
									</tbody>
								</table>					
							</div>
						</div>
					</div>
					@endif
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
			
			
			<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
			<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

			
		</div>
	</body>
	</html>