<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Add Advance Salary</div></div>
							<div class="card-body">
								<form id="formvalidation" method="post" action="{{ route('advsalarys.store') }}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" id="rem_balance" name="balance" >
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Employee Select">Select Employee</label>
											<select class="form-control input-solid" name="emp_id" >
												<option value="">Select</option>
												@foreach($emp as $e)
												<option value="{{ $e->id }}">{{ $e->name }}</option>
												@endforeach
											</select>
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Loan Amount">Loan Amount</label>
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[1]] form-control" id="advance_payment" name="loan_amount" placeholder="Amount" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="month">Month</label>			
											<input type="date" class="validate[required] form-control" name="loan_month" >
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Total Installments">Total Installments</label>	
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[1]] form-control" name="total_installments" placeholder="Installments" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="phone_no">Installment per Month</label>
											<input type="text" class="validate[required, custom[onlyNumberSp],minSize[1]] form-control" name="monthly_installment" placeholder="Installment Per Month" autocomplete="off">
										</div>	
									</div>					
									<div class="card-action row justify-content-md-center">
										<div class="col-md-auto">
										<button class="btn btn-primary" value="Save">Submit</button>
										<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
										</div>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>