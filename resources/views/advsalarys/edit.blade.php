<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Update Advance Salary Details</div></div>
							<div class="card-body">
								<form method="post" action="{{ route('advsalarys.update',$advrecords->id) }}">
									@method('PUT')
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" id="rem_balance" name="balance" >
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Loan Amount">Loan Amount</label>
<<<<<<< .merge_file_a11820
											<input type="text" class="form-control" id="advance_payment" name="loan_amount" value="{{ $advrecords->loan_amount }}" placeholder="Amount"required="" autocomplete="off">
=======
											<input type="text" class="form-control" id="advance_payment" name="loan_amount" value="{{ $advrecords->loan_amount }}" required="" autocomplete="off">
>>>>>>> .merge_file_a01472
										</div>
										<div class="form-group col-md-4">
											<label for="cnic">Month</label>								
											<input type="date" class="form-control" name="loan_month" value="{{ $advrecords->loan_month }}" required="" autocomplete="off">
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Total Installments">Total Installments</label>	
											<input type="text" class="form-control" name="total_installments" value="{{ $advrecords->total_installments }}" placeholder="Installments"required="" autocomplete="off">
										</div>
										<div class="form-group col-md-4">
											<label for="phone_no">Installment per Month</label>
											<input type="text" class="form-control" name="monthly_installment" value="{{ $advrecords->monthly_installment }}" placeholder="Installment Per Month"required="" autocomplete="off">
										</div>	
									</div>					
									<div class="card-action row justify-content-md-center">
										<div class="col-md-auto">
										<button class="btn btn-primary" value="Save">Update</button>
										</div>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>