<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<h4 class="page-title">List of Employess with Advance Salary</h4>
					<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Select <b>Employee</b> or <b>Month</b> for Which You Want to See the Advance Detail</div>
                                </div>
                                <div class="card-body">
                                    <form id="formvalidation" method="GET" action="{{ route('advsalarys.index') }}">  
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                        	<div class="form-group col-md-4">
                                                <label for="Employee Select">Select Employees</label>
                                                <select  name="id" class="form-control input-solid" >
                                                    <option value="">Select Employee</option>
                                                    @foreach($employees as $emp)
                                                    	@foreach($advrecords as $adv)
                                                    		@if($emp->id == $adv->emp_id)
                                                    		<option value="{{ $emp->id }}">{{ $emp->name }}</option>
                                                    		@endif
                                                    	@endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="Account Select">Select Month</label>
                                                <input type="month" class="form-control" name="month">
                                            </div>
                                        </div>
                                        <div class="card-action row justify-content-md-center"">
                                            <div class="col-md-auto">
                                                <input type="Submit" class="btn btn-warning" value="Submit">
                                                <input type="reset" class="btn btn-danger" value="Cancel">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="form-group col-md-3">
									<a href="{{ route('advsalarys.create') }}" class="btn btn-primary  text-left mt-3 mb-3"><i class="la la-plus"></i> Add Addvance Salary</a>
								</div>
								<div class="form-group col-md-3">
									<a href="{{ url('advsalarys/view') }}" class="btn btn-success  text-left mt-3 mb-3"><i class="la la-eye"></i> View Employees List</a>
								</div>
								@if(!empty($month))
								<div class="form-group col-md-auto" style="margin-top: 15px;">
									<input type="text" class="btn btn-info" value="Month = {{ date('M Y', strtotime($month)) }} " readonly="">
								</div>
								@endif
								@if(!empty($id))
								<div class="form-group col-md-auto" style="margin-top: 15px;">
									<input type="text" class="btn btn-info" value="Rem Installments = {{ $rem_installments }} " readonly="">
								</div>
								@endif
							</div>
						</div>
							<div class="card-body">
								<table class="table table-striped mt-3 datatablejs">
									<thead>
										<tr>
											<th scope="col">S.No</th>
											<th scope="col">Name</th>
											<th scope="col">Loan Amount</th>	
											<th scope="col">Monthly Installment</th>
											<th scope="col">Installment Month</th>
											<th scope="col">Rem Loan Balance</th>
											@if(empty($month) && $loan_balance == 0)
											<th scope="col">Alert</th>
											@endif
										</tr>
									</thead>
									<tbody>
										@if(!empty($slips))
											@foreach($slips as $slip)
												<tr>
													<td>{{$loop->iteration}}</td>
													<td>
														{{ $slip->name }}
													</td>
													<td>
														@foreach($advrecords as $adv)
														@if($slip->emp_id == $adv->emp_id)
														{{ $adv->loan_amount }}
														@endif
														@endforeach
													</td>
													<td>
														@foreach($advrecords as $adv)
														@if($slip->emp_id == $adv->emp_id)
														{{ $adv->monthly_installment }}
														@endif
														@endforeach
													</td>
													<td>{{ date('M Y', strtotime($slip->salary_month)) }}</td>
													<td>
														<button class="btn btn-danger btn-sm">Rs. {{ $slip->rem_loan_balance }}</button>
													</td>
													@if(empty($month) && $loan_balance == 0)
													<td>
														<button class="btn btn-success btn-sm">Loan Completed</button>
													</td>
													@endif
													</tr>
											@endforeach
										@endif
									</tbody>
								</table>					
							</div>
						</div>
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>