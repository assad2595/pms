<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Kashaf-Enterprises</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<h4 class="page-title">List of Employess with Advance Salary</h4>
					<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Employees</div>
                                </div>
                                <div class="card-body">
								<table class="table table-striped mt-3 datatablejs">
									<thead>
										<tr>
											<th scope="col">S.No</th>
											<th scope="col">Name</th>
											<th scope="col">Loan Month</th>
											<th scope="col">Loan Amount</th>
											<th scope="col">Total Installments</th>	
											<th scope="col">Monthly Installment</th>
											<th scope="col">Actions</th>
										</tr>
									</thead>
									<tbody>
										@foreach($advrecords as $adv)
										<tr>
											<td>{{$loop->iteration}}</td>
											<td>
												@foreach($employees as $emp)
												@if($adv->emp_id == $emp->id)
												{{ $emp->name }}
												@endif
												@endforeach
											</td>
											<td>{{ date('d M Y', strtotime($adv->loan_month)) }}</td>
											<td>{{ $adv->loan_amount }}</td>
											<td>{{ $adv->total_installments }}</td>
											<td>{{ $adv->monthly_installment }}</td>
											<td>
												<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
												<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('advsalarys.destroy',$adv->id) }}" method="post">
												@method('DELETE')
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												</form>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>					
							</div>
                            </div>
                        </div>
                    </div>
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>