<!--Sidebar-->
    <div class="sidebar">
      <div class="scrollbar-inner sidebar-wrapper">
        
        <!--Main Menu-->
        <ul class="nav">
          <!--DashBoard-->
          <li class="nav-item active">
            <a href="{{ route('home') }}">
              <i class="la la-dashboard"></i>
              <p class="user-level">Dashboard</p>
            </a>
            
            <div class="clearfix"></div>
          </li>
          <!-- Property -->
          <li class="nav-item">
            <a data-toggle="collapse" href="#PropertyMenu" aria-expanded="true">
              <i class="la la-object-ungroup"></i>
              <p>Property</p>
              <span class="caret"></span>
            </a>
            <div class="collapse in" id="PropertyMenu" aria-expanded="true">
              <ul class="nav">
                <li>
                  <a href="{{ route('property.create') }}">
                    <i class="la la-toggle-up"></i>
                    <span class="link-collapse">Add Property</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('property.index') }}">
                    <i class="la la-home"></i>
                    <span class="link-collapse">View Properties</span>
                  </a>
                </li>
                <li>
                  <a href="/propertyproject">
                    <i class="la la-delicious"></i>
                    <span class="link-collapse">Property Projects</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <!-- Files -->
          <li class="nav-item">
            <a data-toggle="collapse" href="#FilesMenu" aria-expanded="true">
              <i class="la la-file-text"></i>
              <p>Files</p>
              <span class="caret"></span></a>
            </a>
            <div class="collapse in" id="FilesMenu" aria-expanded="true">
              <ul class="nav">
                <li>
                  <a href="{{ route('file.create') }}">
                    <i class="la la-file"></i>
                    <span class="link-collapse">Add File</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('file.index') }}">
                    <i class="la la-files-o"></i>
                    <span class="link-collapse">Available Files</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('file/sale') }}">
                    <i class="la la-file-word-o"></i>
                    <span class="link-collapse">Sold Files</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('file/profit') }}">
                    <i class="la la-money"></i>
                    <span class="link-collapse">Files Profit/Loss</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <!-- Lender -->
          <li class="nav-item">
            <a href="/lenders">
              <i class="la la-user"></i>
              <p>Lenders</p>
            </a>
            
          </li>
          <!-- Accounts -->
          <li class="nav-item">
            <a data-toggle="collapse" href="#AccountsMenu" aria-expanded="true">
              <i class="la la-bank"></i>
              <p>Accounts</p>
              <span class="caret"></span>
            </a>
            <div class="collapse in" id="AccountsMenu" aria-expanded="true">
              <ul class="nav">
                <li>
                  <a href="{{ route('bank_transaction.create') }}">
                     <i class="la la-bars"></i>
                    <span class="link-collapse">Add Transaction</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('bank_transaction.index') }}">
                     <i class="la la-bar-chart"></i>
                    <span class="link-collapse">View Transactions</span>
                  </a>
                </li>
                <!-- <li>
                  <a href="#">
                     <i class="la la-tasks"></i>
                    <span class="link-collapse">W.H Tax</span>
                  </a>
                </li> -->
                <li>
                  <a href="{{ route('banks.index') }}">
                    <i class="la la-tasks"></i>
                    <span class="link-collapse">Banks</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <!-- Office -->
          <li class="nav-item">
            <a data-toggle="collapse" href="#OfficeMenu" aria-expanded="true">
              <i class="la la-clipboard"></i>
              <p>Office Expenses</p>
              <span class="caret"></span>
            </a>
            <div class="collapse in" id="OfficeMenu" aria-expanded="true">
              <ul class="nav">
                <li>
                  <a href="{{ route('officeexpenditure.create') }}">
                    <i class="la la-chain-broken"></i>
                    <span class="link-collapse">Add Expense</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('officeexpenditure.index') }}">
                    <i class="la la-crosshairs"></i>
                    <span class="link-collapse">View Expenses</span>
                  </a>
                </li>
                 <li>
                  <a href="{{ url('deposit') }}">
                    <i class="la la-file-archive-o"></i>
                    <span class="link-collapse">Add/View Deposit</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('monthlyreport') }}">
                    <i class="la la-file-archive-o"></i>
                    <span class="link-collapse">Monthly Report</span>
                  </a>
                </li>
                 
              </ul>
            </div>
          </li>
          <!-- Employees -->
          <li class="nav-item">
            <a data-toggle="collapse" href="#EmployeesMenu" aria-expanded="true">
              <i class="la la-users"></i>
              <p>Employees</p>
              <span class="caret"></span>
            </a>
            <div class="collapse in" id="EmployeesMenu" aria-expanded="true">
              <ul class="nav">
                <li>
                  <a href="{{ route('employees.create') }}">
                    <i class="la la-user-plus"></i>
                    <span class="link-collapse">Add Employee</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('employees.index') }}">
                    <i class="la la-user"></i>
                    <span class="link-collapse">View Employees</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('advsalarys.index') }}">
                    <i class="la la-venus"></i>
                    <span class="link-collapse">Employees Advance</span>
                  </a>
                </li>
                <li>
                  <a href="{{ route('payslips.index') }}">
                    <i class="la la-file-text-o"></i>
                    <span class="link-collapse">Salary Slip Management</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <!-- Reports -->
          <li class="nav-item">
            <a data-toggle="collapse" href="#ReportsMenu" aria-expanded="true">
              <i class="la la-file-pdf-o"></i>
              <p>Reports</p>
              <span class="caret"></span>
            </a>
            <div class="collapse in" id="ReportsMenu" aria-expanded="true">
              <ul class="nav">
                <li>
                  <a href="{{ url('payslips/slips') }}">
                    <i class="la la-file-text-o "></i>
                    <span class="link-collapse">Print Monthly Salary Slip</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>