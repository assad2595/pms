    
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{URL::asset('assets/css/ready.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/demo.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/style.css')}}">
    <!-- Login Page CSS -->
    <link rel="icon" type="image/png" href="assets/img/favicon.ico"/>
    <link rel="stylesheet" href="{{URL::asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/util.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/icon-font.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/validationEngine.jquery.css')}}">
    
