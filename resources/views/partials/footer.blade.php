<!-- footer -->
<footer class="footer">
  <div class="container-fluid">
    <div class="copyright ml-auto">
      Copyright &copy;<script>document.write(new Date().getFullYear());</script> <a href="http://www.notitia.tech">Notitia Tech Consultants - All Rights Reserved</a>
    </div>        
  </div>
</footer>