 <!-- JavaScript files-->

    <script src="{{URL::asset('assets/js/core/jquery.3.2.1.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/core/popper.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/core/bootstrap.min.js')}}"></script>
    <!--<script src="{{URL::asset('assets/js/plugin/chartist/chartist.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/chartist/plugin/chartist-plugin-tooltip.min.js')}}">
    </script> -->
    <script src="{{URL::asset('assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/jquery-mapael/jquery.mapael.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/jquery-mapael/maps/world_countries.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/chart-circle/circles.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/ready.min.js')}}"></script>
    <!-- Login Page Js -->
    <script src="{{URL::asset('assets/js/main.js')}}"></script>
    
    <script src="{{URL::asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::asset('assets/js/custom.js')}}"></script>
    <script src="{{URL::asset('assets/js/jquery.printPage.js')}}"></script>
    <!-- Jquery Validation Engine -->
    <script src="{{URL::asset('assets/js/jquery.validationEngine.js')}}"></script>
    <script src="{{URL::asset('assets/js/jquery.validationEngine-en.js')}}"></script>