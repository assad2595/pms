<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					@include('partials.flash-message')		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">New Property Transactions</div></div>
							<div class="card-body">
								
								<form id="formvalidation" method="post" action="{{ route('propertytransactions.store') }}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">

									<div class="row">
										<div class="form-group col-md-3">
											<label for="Property Owner Name">Owner Name</label>
											<input class="form-control" type="text" value="{{ $data[1] }}" readonly="readonly">
										</div>
										<div class="form-group col-md-3">
											<label for="Total Amount">Total Amount</label>
											<input class="form-control" type="text" id="totalpropertyamount" value="{{ $data[2]}}" readonly="readonly">
										</div>
										@if(isset($balance_amount))
											<div class="form-group col-md-3">
												<label for="Balance">Balance</label>
												<input class="form-control" id="balance_amount" type="text" value="{{ $balance_amount }}" readonly="readonly">
											</div>
										@else
											<input type="hidden" id="balance_amount" value="{{ $balance_amount }}" >
										@endif
									</div>
									<input type="hidden" name="property_id" value="{{ $data[0]}}">
									<div class="row">
										<div class="form-group col-md-3">
											<label for="Lender">Select Lender</label>
											<select class="form-control validate[groupRequired[names]" name="lender_id">
												<option value="">Select Lender</option>
												@foreach($lenders as $lender)
													<option value="{{ $lender->id }}">{{ $lender->lender_name }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group col-md-3">
											<label for="Name">Name</label>
											<input type="text" class="validate[groupRequired[names],custom[onlyLetterSp]] form-control" name="paymenter_name"> 
										</div>
										<div class="form-group col-md-6">
											<label for="Payment Description">Payment Description</label>
											<textarea class="validate[required] form-control" name="payment_description" cols="10" rows="2">
											</textarea>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-md-4">
											<label for="Amount">Amount</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="property_amount" name="payment_amount" >
										</div>
										<div class="form-group col-md-4">
											<label for="Balance">Balance</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="remaining_balance" name="balance_payment" readonly="readonly">
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-2">
											<label for="Mode of Payment">Mode of Payment</label>
											<select class="form-control" name="mode_of_payment">
												<option value="Cash">Cash</option>
												<option value="Cheque">Cheque</option>
											</select>
										</div>
										<div class="form-group col-md-2">
											<label for="Cheaque No">Cheque No</label>
											<input type="text" class="form-control" name="cheque_no" >
										</div>
										<div class="form-group col-md-2">
											<label for="bank">Bank</label>
											<input type="text" class="form-control" name="bank_name">
										</div>
										<div class="form-group col-md-2">
											<label for="pv_no">PV Number</label>
											<input type="text" class="form-control" name="pv_no" >
										</div>
										<div class="form-group col-md-3">
											<label for="Payment Date">Payment Date</label>
											<input type="date" class="validate[required] form-control" name="payment_date">
										</div>
									</div>
									<div class="card-action row justify-content-md-center">
										<div class="col-md-auto">
											<button class="btn btn-primary" value="Save">Save</button>
											<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
										</div>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>