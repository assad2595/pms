<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<h4 class="page-title">Property Projects</h4>
					@include('partials.flash-message')
						<div class="row">

							<div class="col-md-6">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Add Project</div>
									</div>
									<div class="card-body">
										<form id="formvalidation" method="post" action="{{ route('propertyproject.store') }}">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="row">
												<div class="form-group col-md-6">
													<label for="Project Name">Project Name</label>
													<input type="textbox" class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" name="project_name" placeholder="Enter Project Name">
												</div>
												<div class="form-group col-md-6">
													<label for="Select City">Select City</label>
													<select class="form-control validate[required]" name="project_city">
														<option value="">Choose City</option>
														<option value="Islamabad">Islamabad</option>
														<option value="Lahore">Lahore</option>
														<option value="Karachi">Karachi</option>
														<option value="Rawalpindi">Rawalpindi</option>
														<option value="Abbottabad">Abbottabad</option>
														<option value="Alipur">Alipur</option>
														<option value="Arifwala">Arifwala</option>
														<option value="Attock">Attock</option>
														<option value="Badin">Badin</option>
														<option value="Bahawalnagar">Bahawalnagar</option>
														<option value="Bahawalpur">Bahawalpur</option>
														<option value="Bannu">Bannu</option>
														<option value="Battagram">Battagram</option>
														<option value="Bhimber">Bhimber</option>
														<option value="Burewala">Burewala</option>
														<option value="Chakwal">Chakwal</option>
														<option value="Chaman">Chaman</option>
														<option value="Chichawatni">Chichawatni</option>
														<option value="Chiniot">Chiniot</option>
														<option value="Chishtian">Chishtian</option>
														<option value="Chitral">Chitral</option>
														<option value="Dera Ismail Khan">Dera Ismail Khan</option>
														<option value="Dera Ghazi Khan">Dera Ghazi Khan</option>
														<option value="Faisalabad">Faisalabad</option>
														<option value="Gilgit">Gilgit</option>
														<option value="Gujar Khan">Gujar Khan</option>
														<option value="Gujranwala">Gujranwala</option>
														<option value="Gujrat">Gujrat</option>
														<option value="Gwadar">Gwadar</option>
														<option value="Haripur">Haripur</option>
														<option value="Hasilpur">Hasilpur</option>
														<option value="Hunza / Nagar">Hunza / Nagar</option>
														<option value="Hyderabad">Hyderabad</option>
														<option value="Harrapa">Harrapa</option>
														<option value="Jacobabad">Jacobabad</option>
														<option value="Jahanian">Jahanian</option>
														<option value="Jaranwala">Jaranwala</option>
														<option value="Jhang">Jhang</option>
														<option value="Jhelum">Jhelum</option>
														<option value="Kasur">Kasur</option>
														<option value="Khairpur">Khairpur</option>
														<option value="Khanewal">Khanewal</option>
														<option value="Khanpur">Khanpur</option>
														<option value="Kharian">Kharian</option>
														<option value="Khushab">Khushab</option>
														<option value="Khuzdar">Khuzdar</option>
														<option value="Kohat">Kohat</option>
														<option value="Kohistan">Kohistan</option>
														<option value="Kot Addu">Kot Addu</option>
														<option value="Kotli">Kotli</option>
														<option value="Larkana">Larkana</option>
														<option value="Layyah">Layyah</option>
														<option value="Mailsi">Mailsi</option>
														<option value="Mansehra">Mansehra</option>
														<option value="Mardan">Mardan</option>
														<option value="Mianwali">Mianwali</option>
														<option value="Mirpur">Mirpur</option>
														<option value="Mohenjo-Daro">Mohenjo-Daro</option>
														<option value="Multan">Multan</option>
														<option value="Muridke">Muridke</option>
														<option value="Murree">Murree</option>
														<option value="Muzafargarh">Muzafargarh</option>
														<option value="Muzaffarabad">Muzaffarabad</option>
														<option value="Nawabshah">Nawabshah</option>
														<option value="Nowshera">Nowshera</option>
														<option value="Okara">Okara</option>
														<option value="Peshawar">Peshawar</option>
														<option value="Pakpattan">Pakpattan</option>
														<option value="Quetta">Quetta</option>
														<option value="Rahim Yar Khan">Rahim Yar Khan</option>
														<option value="Rawalakot">Rawalakot</option>
														<option value="Rajanpur">Rajanpur</option>
														<option value="Sadiqabad">Sadiqabad</option>
														<option value="Sahiwal">Sahiwal</option>
														<option value="Sargodha">Sargodha</option>
														<option value="Sawabi">Sawabi</option>
														<option value="Shakargarh">Shakargarh</option>
														<option value="Sheikhupura">Sheikhupura</option>
														<option value="Sialkot">Sialkot</option>
														<option value="Skardu">Skardu</option>
														<option value="Sukkur">Sukkur</option>
														<option value="Swat">Swat</option>
														<option value="Taxila">Taxila</option>
														<option value="Thatta">Thatta</option>
														<option value="Tibba Sultan Pur">Tibba Sultan Pur</option>
														<option value="Toba Tek Singh">Toba Tek Singh</option>
														<option value="Tunsa">Tunsa</option>
														<option value="Vehari">Vehari</option>
														<option value="Wah">Wah</option>
														<option value="Wazirabad">Wazirabad</option>
														<option value="Ziarat">Ziarat</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="card-action">
													<input type="Submit" name="" class="btn btn-success" value="Submit">
													
													<input type="reset" class="btn btn-danger" value="Reset">
												</div>
											</div>
										</form>
									</div>
									
								</div>
							</div>
						
							<div class="col-md-6">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Projects List</div>
									</div>
									<div class="card-body">
										<table class="table table-striped mt-3 datatablejs">
											<thead>
												<tr>
													<th scope="col">S.No</th>
													<th scope="col">Project Name</th>
													<th scope="col">Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($propertyProjects as $propertyProject)
												<tr>
													<td>{{$loop->iteration}}</td>
													<td>{{$propertyProject->project_name}} {{$propertyProject->project_city}}</td>
													<td>
														<a href="{{ route('propertyproject.edit',$propertyProject->id) }}" class="btn btn-info btn-xs">Edit</a>
														<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
														<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('propertyproject.destroy',$propertyProject->id) }}" method="post">
															@method('DELETE')
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
														</form>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>