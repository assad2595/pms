<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Property Expenses</div>
						</div>
						<div class="card-body">
							@include('partials.flash-message')
							<div class="row">
								<!-- used to add property expenes -->
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<div class="card-title">Add New Expenses</div>
										</div>
										<div class="card-body">
											<form method="post" action="{{ route('propertyexpenses.store') }}">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">

												<div class="row">
													
													<input class="form-control" type="hidden" value="{{ $property_id }}" name="property_id">
													
													<div class="form-group col-md-6">
														<label for="Expense Description">Expense Description</label>
														<input class="form-control" type="text" name="expenses_name">
													</div>
													<div class="form-group col-md-3">
														<label for="Balance">Amount</label>
														<input class="form-control"  type="text" name="expenses_amount">
													</div>
												</div>
												<div class="row">	
													
													<div class="form-group col-md-3">
														<label for="Mode of Payment">Mode of Payment</label>
														<select class="form-control" name="mode_of_payment">
															<option value="Cash">Cash</option>
															<option value="Cheque">Cheque</option>
														</select>
													</div>
													<div class="form-group col-md-3">
														<label for="Cheaque No">Cheque No</label>
														<input type="text" class="form-control" name="cheque_no" >
													</div>
													<div class="form-group col-md-3">
														<label for="bank">Bank</label>
														<input type="text" class="form-control" name="bank_name">
													</div>
													
												</div>

												<div class="row">
													<div class="form-group col-md-3">
														<label for="pv_no">PV Number</label>
														<input type="text" class="form-control" name="pv_no" >
													</div>
													<div class="form-group col-md-3">
														<label for="Payment Date">Payment Date</label>
														<input type="date" class="form-control" name="payment_date">
													</div>
												</div>
												
												<div class="card-action row justify-content-md-center">
													<div class="col-md-auto">
														<button class="btn btn-primary" value="Save">Save</button>
														<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
													</div>
												</div>
											</form>

										</div>
									</div>
								</div>
								<!-- div used to view property expenses -->
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<div class="card-title">Expenses List</div>
										</div>
										<div class="card-body">
											<table class="table table-striped mt-3 datatablejs">
												<thead>
													<tr>
														<th scope="col">S.No</th>
														<th scope="col">Description</th>
														<th scope="col">Amount</th>
														<th scope="col">Mode of Payment</th>
														<th scope="col">Date</th>
														<th scope="col">Actions</th>
													</tr>
												</thead>
												<tbody>
													@foreach($propertyexpenses as $propertyexpense)
													<tr>
														<td>{{$loop->iteration}}</td>
														<td>{{ $propertyexpense->expenses_name }}</td>
														<td>{{ $propertyexpense->expenses_amount }}</td>
														<td>
															{{ $propertyexpense->mode_of_payment  }}
															{{ $propertyexpense->cheque_no  }}
															{{ $propertyexpense->bank_name  }}
															{{ $propertyexpense->pv_no  }}
														</td>
														<td>{{ $propertyexpense->payment_date }}</td>
														<td>
															<a href="{{ route('propertyexpenses.edit', $propertyexpense->id) }}" class="btn btn-info btn-xs">Edit</a>
															
															<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
															<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('propertyexpenses.destroy', $propertyexpense->id) }}" method="post">
															@method('DELETE')
																<input type="hidden" name="_token" value="{{ csrf_token() }}">
															</form>
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
							 </div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>