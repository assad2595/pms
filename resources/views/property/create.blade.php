<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">New Property</div>
						</div>
							<div class="card-body">
								<form id="formvalidation" method="post" action="{{ route('property.store') }}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Property Project">Select Property Project</label>
											<select class="form-control validate[required]" name="propertyproject_id">
												<option value="">Select Property Project</option>
												@foreach($propertyprojects as $propertyproject)
												<option value="{{$propertyproject->id}}">{{$propertyproject->project_name}} {{$propertyproject->project_city}}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group col-md-4">	
											<label for="Owner name">Owner Name</label>					
											<input type="text" class="validate[required, custom[onlyLetterSp]] form-control" name="owner_name" placeholder="Enter Name" >		
										</div>
										<div class="form-group col-md-4">
											<label for="Mouza">Mouza</label>
											<input type="text" class="validate[custom[onlyLetterSp]] form-control" name="mouza" placeholder="Enter Mouza Name">
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-2">
											<label for="cnic">Acr</label>								
											<input type="text" class="validate[custom[number],minSize[1]] form-control" id="acr" name="acr">
										</div>
										<div class="form-group col-md-2">
											<label for="kanal">kanal</label>
											<input type="text" class="validate[custom[number],minSize[1]] form-control" id="kanal" name="kanal">
										</div>
										<div class="form-group col-md-2">
											<label for="marla">Marla</label>
											<input type="text" class="validate[custom[number],minSize[1]] form-control" id="marla" name="marla">
										</div>
										<div class="form-group col-md-2">
											<label for="yard">Yard</label>
											<input type="text" class="validate[custom[number],minSize[1]] form-control" id="yard" name="yard">
										</div>
										<div class="form-group col-md-2">
											<label for="Total Marlas">Total Marlas</label>
											<input type="text" class="form-control" id="total_marlas" name="total_marlas" readonly="">		

										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="Rate Per Acr">Rate Per Acr</label>
											<input type="text" class="validate[required, custom[number] form-control" id="rate_per_acr" name="rate_per_acr">
										</div>
										<div class="form-group col-md-3">
											<label for="Rate Per Marlas">Rate Per Marla</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="rate_per_marla" name="rate_per_marla">
										</div>
										<div class="form-check col-md-3">
											<label>If Commission Applicable</label><br>
											<label class="form-radio-label">
												<input class="form-radio-input" type="radio" name="commission_option" value="yes">
												<span class="form-radio-sign">Yes</span>
											</label>
											<label class="form-radio-label ml-3">
												<input class="form-radio-input" checked="checked" type="radio" name="commission_option" value="no">
												<span class="form-radio-sign">No</span>
											</label>
										</div>
										<div id="commission_div" class="form-group col-md-3" style="display: none;">
											<label for="Property Commission">Commisssion (1%)</label>
											<input type="text" class="form-control" id="proprety_commission" name="proprety_commission" disabled="disabled">
										</div>
										
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="entry_point">Point Entry</label>				

											<input type="text" class="validate[required] form-control" id="point_entry" name="point_entry" readonly="readonly">		

										</div>
										<div class="form-group col-md-4">	
											<label for="Exemption Rate">Exemption Rate</label>	
											<input type="text" class="validate[required, custom[number],minSize[1]] form-control" id="exemption_rate" name="exemption_rate">		
										</div>	
										<div class="form-group col-md-4">
											<label for="Number of Files">Number of Files</label>
											<input type="text" class="validate[required] form-control" id="no_of_files" name="no_of_files" readonly="readonly">
										</div>					
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Total Amount">Total Amount</label>

											<input type="text" class="validate[required, custom[number]] form-control" id="total_amount" name="total_amount" readonly="">
										</div>
										<div class="form-group col-md-4">
											<label for="Property Date">Purchasing Date</label>
											<input type="date" class="validate[required] form-control" name="purchasing_date">
										</div>							
									</div>
															
									<div class="card-action row justify-content-md-center">
										<div class="col-md-auto">
										<button class="btn btn-primary" value="Save">Save & Proceed for Payment Record</button>
										<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
										</div>
									</div>
								</form>
							</div>
					</div>		
				</div>
			</div>
		@include('partials.footer')
	</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>