<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<!-- <h4 class="page-title">Property Transaction Records</h4> -->
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Property File Sale Record</div>
								</div>
								<div class="card-body">
									<form id="formvalidation" method="post" action="{{ route('propertyfiles.update',$propertyfile->id) }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<!-- Files Details -->
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Property Project">Property Project</label> 
											</div>
											
										
										</div>
										<!-- File Information -->
									
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Sale Rate">Sale Rate</label>
												<input class="form-control" id="sale_price" type="text" name="sale_price" value="{{ $propertyfile->sale_price}}">
											</div>
											<div class="form-group col-md-4">
												<label for="Purchaser Name">Purchaser Name</label>
												<input class="form-control" type="text" name="purchaser_name" value="{{ $propertyfile->purchaser_name}}">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Balance">Total Sale Price</label>
												<input class="form-control" id="total_sale_price" name="total_sale_price" type="text" value="{{ $propertyfile->total_sale_price}}">
											</div>
											<div class="form-group col-md-4">
												<label for="Purchaser Name">Profit</label>
												<input class="form-control" id="file_profit" type="text" name="file_profit" value="{{ $propertyfile->file_profit}}">
											</div>
										</div>				
										<div class="row justify-content-md-center">
											<div class="col-md-auto">
												<button class="btn btn-primary" value="Save">Submit</button>
												<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</body>
</html>