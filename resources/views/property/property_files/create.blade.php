<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<!-- <h4 class="page-title">Property Transaction Records</h4> -->
					<div class=row>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Property File Sale Record</div>
								</div>
								<div class="card-body">
									<form id="formvalidation" method="post" action="{{ route('propertyfiles.store') }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<input type="hidden" name="property_id" value="{{$project_id}}">
										<!-- Files Details -->
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Property Project">Property Project</label> 
												@foreach($propertyprojects as $projects)
													@if($projects->id == $propertyproject_id)
														<input type="text" class="form-control" name="propertyproject" value="{{$projects->project_name }} {{$projects->project_city}} " disabled="disabled">
													@endif
												@endforeach
											</div>
											<div class="form-group col-md-4">	
												<label for="Owner name">Owner Name</label>		
												<input type="text" class="form-control" name="owner_name" value="{{$owner_name}}" disabled="disabled">
											</div>
											<div class="form-group col-md-4">
												<label for="No of Files">Total No of Files</label>
												@if(isset($remaining_files))
													<input class="form-control" type="text" value="{{$remaining_files}}" id="total_no_of_files" disabled="disabled">
												@else
													<input class="form-control" type="text" value="{{$no_of_files}}" id="total_no_of_files" disabled="disabled">
												@endif
												
											</div>
										</div>
										<!-- File Information -->
										<div class="row">
											
											<div class="form-group col-md-4">
												<label for="Total Amount">Unit File Price</label>
												<input class="form-control" id="file_price" type="text" value="{{$file_price}}" name="file_price" readonly="readonly">
											</div>
											<div class="form-group col-md-4">
												<label for="Sold Files">Files to be Sold</label>
												<input class="form-control" name="no_of_files" type="text" id="no_of_files" value="{{$sold_files}}" readonly="readonly">
											</div>
											<div class="form-group col-md-4">
												<label for="No of Files">Remaining Files</label>
												<input class="form-control" type="text"" id="remaining_files" name="remaining_files" readonly="readonly">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Sale Rate">Sale Rate</label>
												<input class="form-control" id="sale_price" type="text" name="sale_price">
											</div>
											<div class="form-group col-md-4">
												<label for="Purchaser Name">Purchaser Name</label>
												<input class="form-control" type="text" name="purchaser_name">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
												<label for="Balance">Total Sale Price</label>
												<input class="form-control" id="total_sale_price" name="total_sale_price" type="text" readonly="readonly">
											</div>
											<div class="form-group col-md-4">
												<label for="Purchaser Name">Profit</label>
												<input class="form-control" id="file_profit" type="text" name="file_profit" readonly="readonly">
											</div>
										</div>				
										<div class="row justify-content-md-center">
											<div class="col-md-auto">
												<button class="btn btn-primary" value="Save">Submit</button>
												<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('partials.footer')
	</div>
	@include ('partials.js-libraries')
</body>
</html>