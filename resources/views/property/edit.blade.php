<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">		   						
					<div class="card">
						<div class="card-header">
							<div class="card-title">Updated Property information</div></div>
							<div class="card-body">
								<form id="formvalidation" method="post" action="{{ route('property.update', $property->id) }}">
									@method('PUT')
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Property Project">Select Property Project</label>
											<select class="form-control" name="propertyproject_id">
												
												@foreach($propertyprojects as $propertyproject)

								                  <option value="{{$propertyproject->id}}" {{$property->propertyproject_id==$propertyproject->id ? 'selected="selected"' : ''}}>
								                  	{{$propertyproject->project_name}} {{$propertyproject->project_city}}</option>

								                @endforeach
												<!-- <option value="2">DHA karachi</option>
												<option value="3">Askari 11, Multan</option> -->
											</select>
										</div>
										<div class="form-group col-md-4">	
											<label for="Owner name">Owner Name</label>					
											<input type="text" class="validate[required,custom[onlyLetterSp]] form-control" name="owner_name" placeholder="Enter Name" value="{{ $property->owner_name }}">		
										</div>
										<div class="form-group col-md-4">
											<label for="Mouza">Mouza</label>
											<input type="text" class="validate[custom[onlyLetterSp]] form-control" name="mouza" placeholder="Enter Mouza Name" value="{{ $property->mouza }}" >
										</div>	
									</div>
									<div class="row">
										<div class="form-group col-md-2">
											<label for="cnic">Acr</label>								
											<input type="text" class="validate[custom[number]] form-control" id="acr" name="acr" value="{{ $property->acr }}">
										</div>
										<div class="form-group col-md-2">
											<label for="kanal">kanal</label>
											<input type="text" class="validate[custom[number]] form-control" id="kanal" name="kanal" value="{{ $property->kanal }}">
										</div>
										<div class="form-group col-md-2">
											<label for="marla">Marla</label>
											<input type="text" class="validate[custom[number]] form-control" id="marla" name="marla"  value="{{ $property->marla }}">
										</div>
										<div class="form-group col-md-2">
											<label for="yard">Yard</label>
											<input type="text" class="validate[custom[number]] form-control" name="yard" value="{{ $property->yard }}">
										</div>
										<div class="form-group col-md-2">
											<label for="Total Marlas">Total Marlas</label>				
											<input type="text" class="form-control" id="total_marlas" name="total_marlas" value="{{ $property->total_marlas }}" readonly="">		
										</div>
										
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="Rate Per Acr">Rate Per Acr</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="rate_per_acr" name="rate_per_acr" value="{{ $property->rate_per_acr }}">
										</div>
										<div class="form-group col-md-3">
											<label for="Rate Per Marlas">Rate Per Marla</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="rate_per_marla" name="rate_per_marla" value="{{ $property->rate_per_marla }}">
										</div>
										
										<div class="form-check col-md-3">
											<label>If Commission Applicable</label><br>
											<label class="form-radio-label">
												@if(isset($property->proprety_commission))
													<input class="form-radio-input" checked="checked" type="radio" name="commission_option" value="yes">
													<span class="form-radio-sign">Yes</span>
												@else
													<input class="form-radio-input" type="radio" name="commission_option" value="yes">
													<span class="form-radio-sign">Yes</span>
												@endif

											</label>
											<label class="form-radio-label ml-3">
												@if(isset($property->proprety_commission))
													<input class="form-radio-input" type="radio" name="commission_option" value="no">
													<span class="form-radio-sign">No</span>
												@else
													<input class="form-radio-input" checked="checked" type="radio" name="commission_option" value="no">
													<span class="form-radio-sign">No</span>
												@endif

											</label>
										</div>
										@if(isset($property->proprety_commission))
											<div id="commission_div" class="form-group col-md-3">
												<label for="Property Commission">Commisssion (1%)</label>
												<input type="text" class="form-control" id="proprety_commission" value="{{ $property->proprety_commission }}" name="proprety_commission" readonly="readonly">
											</div>
										@else
											<div id="commission_div" class="form-group col-md-3"  style="display: none;">
												<label for="Property Commission">Commisssion (1%)</label>
												<input type="text" class="form-control" id="proprety_commission" value="{{ $property->proprety_commission }}" name="proprety_commission" readonly="readonly">
											</div>
										@endif
										
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="entry_point">Point Entry</label>				
											<input type="text" class="validate[required, custom[number],minSize[1]] form-control" id="point_entry" name="point_entry" value="{{ $property->point_entry }}" readonly="readonly">		
										</div>
										<div class="form-group col-md-4">	
											<label for="Exemption Rate">Exemption Rate</label>		
											<input type="text" class="validate[required, custom[number]] form-control" id="exemption_rate" name="exemption_rate" value="{{ $property->exemption_rate }}" >		
										</div>	
										<div class="form-group col-md-4">
											<label for="Number of Files">Number of Files</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="no_of_files" name="no_of_files" value="{{ $property->no_of_files }}" readonly="readonly">
										</div>					
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label for="Total Amount">Total Amount</label>
											<input type="text" class="validate[required, custom[number]] form-control" id="total_amount" name="total_amount" value="{{ $property->total_amount }}" readonly="readonly">
										</div>
										<div class="form-group col-md-4">
											<label for="Property Date">Purchasing Date</label>
											<input type="date" class="validate[required] form-control" name="purchasing_date" value="{{ $property->purchasing_date }}">
										</div>							
									</div>
															
									<div class="card-action row justify-content-md-center">
										<div class="col-md-auto">
											<button class="btn btn-primary" value="Save">Update</button>
										</div>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
				@include('partials.footer')
			</div>
			@include ('partials.js-libraries')
		</div>
	</body>
	</html>