<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					
					<h4 class="page-title">Properties List</h4>
					
					@include('partials.flash-message')

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-12">
									<a href="{{ route('property.create') }}" class="btn btn-primary text-left float-right"><i class="la la-plus"></i> Add Property</a>
								</div>
							</div>
						</div>
						<div class="card-body">
							<table class="table table-striped mt-3 datatablejs">
								<thead>
									<tr>
										<th scope="col">S.No</th>
										<th scope="col">Owner Name</th>
										<th scope="col">Mouza</th> 
										<!-- <th scope="col">Acr-Kanal-Marla-Yard</th> -->
										<th scope="col">Total Land (Marlas)</th>
										<th scope="col">Rate Per Acr</th> 
										<th scope="col">Total Amount</th>
										<th scope="col">File Points</th>							
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($propertys as $land)
									<tr>
										<td>{{$loop->iteration}}</td>
										<td>{{ $land->owner_name }}</td>
										<td>{{ $land->mouza }}</td>  
										<td>{{ $land->total_marlas }}</td>
										<td>{{ $land->rate_per_acr }}</td>
										<td>{{ $land->total_amount }}</td>
										<td>{{ $land->no_of_files }}</td>
										<td>
											<a href="{{ route('property.show',$land->id) }}" class="btn btn-success btn-xs">View</a>	
											<a href="{{ route('property.edit',$land->id) }}" class="btn btn-info btn-xs">Edit</a>	
											<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
											<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('property.destroy',$land->id) }}" method="post">
												@method('DELETE')
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>					
						</div>
					</div>
				</div>
			</div>
			@include('partials.footer')
			@include ('partials.js-libraries')
		</div>
	</div>
</body>
</html>