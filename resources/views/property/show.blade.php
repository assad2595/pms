<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	@include('partials.head')
</head>
<body>
	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar-menu')
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">

					@include('partials.flash-message')

					<div class="card">
						<!-- Property Details -->
						<div class="card-header">
							<div class="row">
								<div class="col-md-5">
									<div class="card-title">Property Details</div>
								</div>
								<div class="col-md-4">
									<a href="{{ route('propertyexpenses.create', $property->id) }}" class="btn btn-primary">Add/View Property Registry Expenses</a>
								</div>
								<div class="col-md-2 float-right">
									@if(count($propertytransactions) > 0)
										@foreach($propertytransactions as $pt)
											@if($loop->last)
												@if($pt->balance_payment == 0)
													<button class="btn btn-success float-right">Payment Completed</button>
												@else
													<a href="{{ route('propertytransactions.create', $property->id) }}" class="btn btn-primary">Create New Transaction</a>
												@endif
											@endif
										@endforeach
									@else
										<a href="{{ route('propertytransactions.create', $property->id) }}" class="btn btn-primary">Create New Transaction</a>
									@endif
								</div>
								<!-- <div class="col-md-12">
									
								</div> -->
							</div>
						<div class="card-body">
							<form method="post" action="{{ route('property.show', $property->id) }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="Property Project">Select Property Project</label>
										<select class="form-control" name="propertyproject_id" disabled="disabled"> 
											@foreach($propertyprojects as $propertyproject)

							                  <option value="{{$propertyproject->id}}" {{$property->propertyproject_id==$propertyproject->id ? 'selected="selected"' : ''}}>
							                  	{{$propertyproject->project_name}} {{$propertyproject->project_city}}</option>

							                @endforeach
										</select>
									</div>
									<div class="form-group col-md-4">	
										<label for="Owner name">Owner Name</label>					
										<input type="text" class="form-control" name="owner_name" placeholder="Enter Name" value="{{ $property->owner_name }}" disabled="disabled">		
									</div>
									<div class="form-group col-md-4">
										<label for="Mouza">Mouza</label>
										<input type="text" class="form-control" name="mouza" placeholder="Enter Mouza Name" value="{{ $property->mouza }}" disabled="disabled">
									</div>	
								</div>
								<div class="row">
									<div class="form-group col-md-2">
										<label for="cnic">Acr</label>								
										<input type="text" class="form-control" id="acr" name="acr" value="{{ $property->acr }}" disabled="disabled"">
									</div>
									<div class="form-group col-md-2">
										<label for="kanal">kanal</label>
										<input type="text" class="form-control" id="kanal" name="kanal" value="{{ $property->kanal }}" disabled="disabled">
									</div>
									<div class="form-group col-md-2">
										<label for="marla">Marla</label>
										<input type="text" class="form-control" id="marla" name="marla"  value="{{ $property->marla }}" disabled="disabled">
									</div>
									<div class="form-group col-md-2">
										<label for="yard">Yard</label>
										<input type="text" class="form-control" name="yard" value="{{ $property->yard }}" disabled="disabled">
									</div>
									<div class="form-group col-md-2">
										<label for="Total Marlas">Total Marlas</label>				
										<input type="text" class="form-control" id="total_marlas" name="total_marlas" value="{{ $property->total_marlas }}" disabled="disabled">		
									</div>
									
								</div>
								<div class="row">
									<div class="form-group col-md-4">
										<label for="Rate Per Acr">Rate Per Acr</label>
										<input type="text" class="form-control" id="rate_per_acr" name="rate_per_acr" value="{{ $property->rate_per_acr }}" disabled="disabled">
									</div>
									<div class="form-group col-md-4">
										<label for="Rate Per Marlas">Rate Per Marla</label>
										<input type="text" class="form-control" id="rate_per_marla" name="rate_per_marla" value="{{ $property->rate_per_marla }}" disabled="disabled">
									</div>
									@if(isset($property->proprety_commission))
									<div id="commission_div" class="form-group col-md-3">
										<label for="Property Commission">Commisssion (1%)</label>
										<input type="text" class="form-control" id="proprety_commission" name="proprety_commission" disabled="disabled" value="{{ $property->proprety_commission }}">
									</div>
									@endif
								</div>
								<div class="row">
									<div class="form-group col-md-4">
										<label for="entry_point">Point Entry</label>				
										<input type="text" class="form-control" id="point_entry" name="point_entry" value="{{ $property->point_entry }}" disabled="disabled">		
									</div>
									<div class="form-group col-md-4">	
										<label for="Exemption Rate">Exemption Rate</label>		
										<input type="text" class="form-control" id="exemption_rate" name="exemption_rate" value="{{ $property->exemption_rate }}" disabled="disabled">		
									</div>	
									<div class="form-group col-md-4">
										<label for="Number of Files">Number of Files</label>
										<input type="text" class="form-control" id="no_of_files" name="no_of_files" value="{{ $property->no_of_files }}" disabled="disabled">
									</div>					
								</div>
								<div class="row">
									<div class="form-group col-md-4">
										<label for="Total Amount">Total Amount</label>
										<input type="text" class="form-control" id="total_amount" name="total_amount" value="{{ $property->total_amount }}" disabled="disabled">
									</div>
									<div class="form-group col-md-4">
										<label for="Property Date">Purchasing Date</label>
										<input type="date" value="{{ $property->purchasing_date }}" class="form-control" name="purchasing_date" disabled="disabled" >
									</div>								
								</div>
								<div class="card-action row justify-content-md-center">
									<div class="col-md-auto">
										
										<a href="{{ route('property.edit',$property->id) }}" class="btn btn-default">Edit Details</a>
									
										<a href="{{ route('propertyexpenses.create', $property->id) }}" class="ml-2 btn btn-primary float-right">Add/View Property Registry Expenses</a>
										
									</div>
								</div>
							</form>
						</div>
						<!--Property Transactions-->
						<div class="card-header">
							<div class="row">
								<div class="col-md-6">
									<div class="card-title">Property Transaction Details</div>
								</div>
								<div class="col-md-6">
									
									@if(count($propertytransactions) > 0)
										@foreach($propertytransactions as $pt)
											@if($loop->last)
												@if($pt->balance_payment == 0)
													<button class="btn btn-success float-right">Payment Completed</button>
												@else
													<a href="{{ route('propertytransactions.create', $property->id) }}" class="btn btn-primary float-right">Create New Transaction</a>
												@endif
											@endif
										@endforeach
									@else
										<a href="{{ route('propertytransactions.create', $property->id) }}" class="btn btn-primary float-right">Create New Transaction</a>
									@endif
								</div>
							</div>	
						</div>
						<div class="card-body">
							<table class="table table-striped mt-3 datatablejs">
								<thead>
									<tr>
										<th scope="col">S.No</th>
										<th scope="col">Name</th>
										<th scope="col">Amount</th>
										<th scope="col">Balance</th>
										<th scope="col">Description</th>
										<th scope="col">Mode of Payment</th>
										<th scope="col">Date</th>
									</tr>
								</thead>
								<tbody>
									@foreach($propertytransactions as $transactions)
										<tr>
											<td>{{$loop->iteration}}</td>
											<td>
												@if($transactions->paymenter_name != "")
													{{$transactions->paymenter_name}}
												@else
													@foreach($lenders as $lender)
														@if($transactions->lender_id == $lender->id)
															{{$lender->lender_name}}
														@endif
													@endforeach

												@endif
											</td>
											<td>{{$transactions->payment_amount }}</td>
											<td>{{$transactions->balance_payment }}</td>
											<td>{{$transactions->payment_description }}</td>
											<td>
												{{$transactions->mode_of_payment}}
												@if($transactions->bank_name != "")
													- {{$transactions->bank_name}}
												@endif

												@if($transactions->cheque_no != "")
													- ChequeNo: {{$transactions->cheque_no}}
												@endif

												@if($transactions->pv_no != "")
													- PV No: {{$transactions->pv_no}}
												@endif

											</td>
											<td>{{$transactions->payment_date}}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<!-- Property Profit/Loss & overall cost -->
						<div class="card-header">
							<div class="row">
								<div class="col-md-6">
									<div class="card-title">Property Overview</div>
								</div>
							</div>	
						</div>
						<div class="card-body">
							<!-- Property profit n loss -->
							<div class="row justify-content-md-center">
								<div class="form-group col-md-2.5">
									<label class="btn btn-warning">Purchasing Price = {{$property->total_amount}}</label>
								</div>
								<div class="form-group col-md-3.5">
									<label class="btn btn-success" value="">Registry Expenditure = {{$propertyexpenses}} </label>
								</div>
								@if(isset($property->proprety_commission))
									<div class="form-group col-md-3">
										<label class="btn btn-warning">Commission (1%)= {{$property->proprety_commission}}</label>
									</div>
								@endif

								<div class="form-group col-md-3">
									<label class="btn btn-warning">Total Land Price = {{$total_property_price}}</label>
								</div>

							</div>

							
							<div class="row justify-content-md-center">
								<!-- <div class="form-group col-md-0.5"></div> -->

								<div class="form-group col-md-2.5">
									<label class="btn btn-success">No of Files = {{ $property->no_of_files }}</label>
								</div>
								@if($remaining_files != 0)
									<div class="form-group col-md-2.5">
										<label class="btn btn-warning">Available Files = {{ $remaining_files }}</label>
									</div>
								@else
									<div class="form-group col-md-2.5">
										<label class="btn btn-success">Files Sold!</label>
									</div>
								@endif
								<div class="form-group col-md-4.5">
									<label class="btn btn-warning">Average File Price = {{$average_file_price}}</label>
								</div>
							
							
								<div class="form-group col-md-2.5">
									<label class="btn btn-warning">Total File Profit = {{$total_profit}}</label>
								</div>
							</div>
							
							@if($remaining_files != '0.00')
								<form id="formvalidation" method="post" action="{{ route('propertyfiles.sale', $property->id)}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								
								<div class="card-header">
									<div class="row">
										<div class="col-md-6">
											<div class="card-title">Sold Files</div>
										</div>
									</div>	
								</div>
								<div class="card-body">
									<div class="row">
										<input type="hidden" id="no_of_files" name="no_of_files" value="{{$property->no_of_files }}">
										<input type="hidden" id="remaining_files" name="remaining_files" value="{{$remaining_files}}">

										<input type="hidden" name="file_price" value="{{$average_file_price}}">
										
										<div class="form-group col-md-4">	
											<input type="text" class="form-control" name="sold_file" id="sold_file" placeholder="Enter the files No">
										</div>
											
										<div class="form-group col-md-auto">
											<input class="btn btn-primary" type="submit" value="Sold File">
											<button type="Reset" class="btn btn-danger" value="Reset">Reset</button>
										</div>
									</div>
								</div>
							</form>
							@endif
							<div class="card-header">
								<div class="row">
									<div class="col-md-6">
										<div class="card-title">List of Sold Files</div>
									</div>
								</div>	
							</div>
							<!-- Details to show sold files -->

							<div class="card-body">
								<!-- <div class="row"> -->
									<table class="table table-striped mt-3 datatablejs">

										<thead>
											<tr>
												<th scope="col">S.No</th>
												<th scope="col">Purchaser Name</th>
												<th scope="col">Nos File</th>
												<th scope="col">File Price</th>
												<th scope="col">Sale Price</th>
												<th scope="col">Total Sale Price</th>
												<th scope="col">Profit</th>
												<th scope="col">Date</th>
												<th scope="col">Actions</th>
											</tr>
										</thead>
										<tbody>

											@foreach($propertyfiles as $propertyfile)
												<tr>
													<td>{{$loop->iteration}}</td>
													<td>{{$propertyfile->purchaser_name}}</td>
													<td>{{$propertyfile->no_of_files}}</td>
													<td>{{$propertyfile->file_price}}</td>
													<td>{{$propertyfile->sale_price}}</td>
													<td>{{$propertyfile->total_sale_price}}</td>
													<td>{{$propertyfile->file_profit}}</td>
													<td>{{$propertyfile->created_at}}</td>
													<td>
															
														<a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger btn-xs">Delete</a>
														<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('propertyfiles.destroy',$propertyfile->id) }}" method="post">
														@method('DELETE')
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
														</form>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								<!-- </div> -->
							</div>
						</div>
					</div>		
				</div>
			</div>
			@include('partials.footer')
		</div>
		@include ('partials.js-libraries')
	</div>
</body>
</html>